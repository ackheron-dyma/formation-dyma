import {
    Component,
    ElementRef,
    QueryList,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import { FruitComponent } from './fruit/fruit.component.js';

// Le sélecteur @Component avec des métadonnées
@Component({
    // Le sélecteur utilisé -> voir dans index.html
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
})

// La classe portant le nom du composant
export class AppComponent {
    // Tableau de fruits où chaque fruit ajouté sera stocké
    // Cette propriété est utilisée avec *ngFor dans le template pour l'affichage dynamique
    fruitsTab: string[] = ['pomme', 'banane'];

    // Déclare une propriété avec @ViewChild pour accéder à l'élément d'entrée 'myinput'
    // { static: false } indique que la référence sera définie après le rendu initial de la vue
    @ViewChild('myinput', { static: false })
    myInputElement!: ElementRef<HTMLInputElement>;

    // Utilisation de @ViewChildren pour obtenir une liste de tous les composants 'FruitComponent' enfants
    // 'liste' est de type QueryList<FruitComponent>, ce qui permet d'itérer sur tous les éléments
    @ViewChildren(FruitComponent) liste!: QueryList<FruitComponent>;

    // Constructeur du composant
    // Il est vide car aucune logique d'initialisation n'est requise pour le moment
    constructor() {}

    // Méthode appelée lorsqu'on souhaite ajouter un fruit au tableau 'fruitsTab'
    addFruit() {
        // Log de la liste actuelle des composants enfants FruitComponent
        console.log(this.liste);

        // Ajout de la valeur de l'input 'myinput' au tableau 'fruitsTab'
        this.fruitsTab.push(this.myInputElement.nativeElement.value);

        // Réinitialise le champ d'entrée pour le préparer à une nouvelle saisie
        this.myInputElement.nativeElement.value = '';

        // Log du tableau 'fruitsTab' pour vérifier son contenu après l'ajout
        console.log(this.fruitsTab);
    }
    // Méthode pour supprimer un fruit du tableau
    // Cette méthode utilise la fonction 'filter' pour créer un nouveau tableau
    // qui exclut le fruit donné en paramètre
    deleteFruit(fruit: string) {
        // Le tableau est filtré pour exclure le fruit à supprimer
        // Tous les fruits différents du paramètre 'fruit' seront gardés
        this.fruitsTab = this.fruitsTab.filter((f) => f != fruit);

        // Log du tableau mis à jour des fruits après suppression
        console.log(this.fruitsTab);
    }
}
