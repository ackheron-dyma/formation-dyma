import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 *
 * Explication des fonctionnalités :

    @Input() :
        Le décorateur @Input() permet de recevoir une valeur du composant parent (dans ce cas, un fruit). Le composant enfant ne fait qu'afficher cette valeur.
        La propriété fruitDuComposantEnfant est de type string?, ce qui signifie que c'est une chaîne de caractères, mais elle peut être optionnelle.

    @Output() :
        Le décorateur @Output() permet de définir un événement que le composant enfant émettra vers le parent.
        EventEmitter<string> : C'est l'objet qui permet d'envoyer un événement avec une valeur (ici, le fruit à supprimer). Le parent écoute cet événement et réagit en conséquence.
        La propriété deleteFruitComposantEnfant est initialisée avec un nouveau EventEmitter, prêt à être utilisé pour envoyer des événements.

    sendToParentComp() :
        Cette méthode est déclenchée lors du clic sur le fruit dans la liste (<li>).
        Elle utilise emit() pour envoyer la valeur actuelle de fruitDuComposantEnfant (le fruit cliqué) au composant parent.
 *
 */

@Component({
    selector: 'app-fruit',
    templateUrl: './fruit.component.html',
    styleUrl: './fruit.component.scss',
})
export class FruitComponent {
    // La propriété 'fruitDuComposantEnfant' sera une donnée d'entrée passée par le composant parent
    // Le décorateur @Input() permet de recevoir des données depuis le parent
    @Input() fruitDuComposantEnfant?: string;
    // L'EventEmitter 'deleteFruitComposantEnfant' permettra d'émettre un événement vers le parent
    // Le décorateur @Output() permet au composant d'envoyer un événement au composant parent
    @Output() deleteFruitComposantEnfant: EventEmitter<string> =
        new EventEmitter();

    // Cette méthode est appelée lorsqu'un utilisateur clique sur l'élément de la liste
    // Elle émet un événement avec la valeur du fruit pour demander au parent de le supprimer
    sendToParentComp() {
        // Emit de l'événement en passant la valeur du fruit à supprimer
        this.deleteFruitComposantEnfant.emit(this.fruitDuComposantEnfant);
    }
}
