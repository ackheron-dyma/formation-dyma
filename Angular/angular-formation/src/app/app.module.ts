// Importation des modules et composants nécessaires
import { NgOptimizedImage } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FruitComponent } from './fruit/fruit.component';

// Décorateur @NgModule pour définir un module Angular
@NgModule({
    // Déclaration des composants, directives et pipes appartenant à ce module
    declarations: [AppComponent, FruitComponent],
    // Importation d'autres modules dont les exportations sont nécessaires à ce module
    imports: [
        BrowserModule, // Fournit les services essentiels pour exécuter l'application dans un navigateur
        FormsModule,
        NgOptimizedImage,
    ],
    // Déclaration des services à l'échelle de l'application (vide ici)
    providers: [],
    // Spécifie le composant racine à démarrer lors du lancement de l'application
    bootstrap: [AppComponent],
})
// Définition de la classe du module principal de l'application
export class AppModule {}
