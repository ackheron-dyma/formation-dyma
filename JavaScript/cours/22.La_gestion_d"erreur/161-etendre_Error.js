/*=============================================
=            Créer des gestionnaire d'erreurs            =
=============================================*/
/* Grâce à une bonne gestion des erreurs vous pouvez ensuite créer des gestionnaires génériques pour vos erreurs dans votre application. */

console.log(Error);
class ValidationError extends Error {
    constructor(message) {
        super(message); // Appel du constructeur de la classe Error avec le message d'erreur
        this.name = "ValidationError"; // Définition du nom de l'erreur
    }
}

class IntervalError extends ValidationError {
    constructor(min, max) {
        super(`Vous devez entrer une valeur entre ${min} et ${max}`); // Appel du constructeur de la classe ValidationError avec le message spécifique
        this.name = "IntervalError"; // Définition du nom de l'erreur
    }
}

try {
    const data = {
        age: -42,
    };
    // Vérification si la valeur de data.age est un entier
    if (!Number.isInteger(data.age)) {
        // Lancer une instance de ValidationError avec le message d'erreur approprié
        throw new ValidationError("Age n'est pas un nombre");
        // Vérification si la valeur de data.age est en dehors de l'intervalle valide
    } else if (data.age < 0 || data.age > 110) {
        // Lancer une instance de IntervalError avec les valeurs d'intervalle appropriées
        throw new IntervalError(0, 110);
    }
} catch (error) {
    // Vérifier si l'erreur est une instance de ValidationError
    if (error instanceof ValidationError) {
        console.log(error);
        // Vérifier si l'erreur est une instance de IntervalError
    } else if (error instanceof IntervalError) {
        console.log(error);
    } else {
        throw error; // Lancer l'erreur à nouveau si elle n'est pas de type ValidationError ou IntervalError
    }
}

/*=============================================
=            Le raccourcis `this.constructor.name`            =
=============================================*/
/* L'utilisation de this.constructor.name dans le constructeur de la classe Erreur permet d'obtenir dynamiquement le nom de la classe actuelle, sans avoir à le spécifier explicitement. Cela rend le code plus flexible et évite la répétition du nom de la classe dans chaque constructeur. De cette façon, si vous changez le nom de la classe, le code continuera à fonctionner correctement sans avoir besoin de modifier manuellement chaque occurrence du nom de la classe. */

class Erreur extends Error {
    // Appel du constructeur de la classe Error avec le message d'erreur
    constructor(message) {
        super(message);
        this.name = this.constructor.name; // Obtention du nom de la classe et assignation à la propriété 'name'
    }
}

class ValidationErreur extends Erreur {
    // Appel du constructeur de la classe Erreur avec le message d'erreur
    constructor(message) {
        super(message);
    }
}

class IntervaleErreur extends Erreur {
    // Appel du constructeur de la classe Erreur avec le message d'erreur spécifique
    constructor(min, max) {
        super(`Vous deve entrer une valeur entre ${min} et ${max}.`);
    }
}
