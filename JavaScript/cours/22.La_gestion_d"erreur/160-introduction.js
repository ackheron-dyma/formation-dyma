/*=============================================
=            Erreurs asynchrones            =
=============================================*/

// Il vaut mieux utiliser async / await. Si pas le choix, mettre les blocs try / catch dans la fonction asynchrone :

setTimeout(() => {
    // Dans le bloc try, nous lançons une erreur en utilisant l'instruction throw avec la valeur "erreur". Cela simule la génération d'une erreur asynchrone.
    try {
        throw "erreur";
    } catch (error) {
        console.log("L'erreur est attrapée !");
    }
}, 1000);

/* L'utilisation de try/catch permet de capturer et de gérer les erreurs asynchrones de manière synchrone, ce qui facilite le débogage et le traitement des erreurs dans le code. Cependant, il est recommandé d'utiliser des approches plus modernes telles que les fonctions asynchrones avec async/await pour une gestion plus élégante des erreurs asynchrones. */

/*=============================================
=            Bonnes pratiques générales            =
=============================================*/
// Cela permet de gérer les erreurs spécifiques, telles que les erreurs de syntaxe, de manière appropriée et d'afficher des messages d'erreur pertinents pour faciliter le débogage.

data = '{ "prenom": "jean" }'; // Déclaration d'une chaîne de caractères représentant des données JSON

try {
    const utilisateur = JSON.parse(data); // Conversion de la chaîne JSON en objet JavaScript en utilisant JSON.parse()

    if (!utilisateur.age) {
        throw new SyntaxError("Il manque le paramètre âge !"); // Lancer une erreur de type SyntaxError si le paramètre 'age' est manquant dans l'objet utilisateur
    }
} catch (error) {
    // Vérifier si l'erreur est de type SyntaxError
    if (error instanceof SyntaxError) {
        // Afficher le message d'erreur spécifique de SyntaxError
        console.log(error.message);
    } else {
        // Sinon, cela signifie que l'erreur est d'un autre type et nous la lançons à nouveau en utilisant l'instruction throw error.
        throw error;
    }
}
