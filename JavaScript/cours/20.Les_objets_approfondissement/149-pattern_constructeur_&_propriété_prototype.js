/*=============================================
=            Le pattern constructeur            =
=============================================*/
/* Le pattern constructeur est une façon courante de créer des objets avec des propriétés spécifiées. Il vous permet de créer plusieurs instances d'objets avec des valeurs différentes pour les propriétés, tout en partageant la même structure et les mêmes fonctionnalités. */

// La fonction constructeur "Voiture" prend un paramètre "marque"
function Voiture(marque) {
    this.marque = marque;
    this.moteur = true;
}

// Une instance d'objet de voiture nommée "tesla", le mot-clé "new" suivi du constructeur et de l'argument nécessaire,
const tesla = new Voiture("Tesla");

/*=============================================
=            La propriété prototype            =
=============================================*/
/* La propriété prototype est une propriété spéciale dans les fonctions constructeurs en JavaScript. Elle est utilisée pour ajouter des propriétés et des méthodes aux objets créés à l'aide de la fonction constructeur. */

function Car(marque) {
    this.marque = marque;
}

// la propriété moteur est ajoutée à Car.prototype en utilisant Car.prototype.moteur = true;. Cela signifie que toutes les instances de voitures créées à partir de Car auront accès à la propriété moteur via leur prototype.
Car.prototype.moteur = true;

const renault = new Car("Renault");

// En résumé, le code utilise la propriété prototype pour ajouter une propriété moteur à la fonction constructeur Car et vérifie ensuite si une instance de voiture spécifique, renault, a accès à cette propriété via son prototype.
const test = renault.__proto__ === Car.prototype;
console.log("🚀 ~ file: 149-pattern_constructeur_&_propriété_prototype.js:25 ~ test:", test);

/*=============================================
=            La propriété constructor de la propriété prototype            =
=============================================*/
/* Dans JavaScript, chaque objet possède une propriété interne appelée "constructor" qui fait référence à la fonction constructeur utilisée pour créer cet objet. Cette propriété est définie par défaut sur le prototype de la fonction constructeur. */

function Test() {}

const instance = new Test();
console.log("🚀 ~ file: 149-pattern_constructeur_&_propriété_prototype.js:40 ~ instance:", instance);

// Lorsque vous créez une instance de "Test" en utilisant "new Test()", la propriété "constructor" de cette instance est automatiquement définie sur la fonction constructeur "Test". C'est pourquoi la condition instance.constructor === Test renvoie true, car la propriété "constructor" de "instance" fait référence à la fonction constructeur "Test".
const verif = instance.constructor === Test;
console.log("🚀 ~ file: 149-pattern_constructeur_&_propriété_prototype.js:42 ~ verif:", verif);

// EN RÉSUMÉ, LA PROPRIÉTÉ "CONSTRUCTOR" FAIT RÉFÉRENCE À LA FONCTION CONSTRUCTEUR UTILISÉE POUR CRÉER UN OBJET, ET ELLE EST AUTOMATIQUEMENT DÉFINIE SUR LE PROTOTYPE DE CETTE FONCTION CONSTRUCTEUR.
