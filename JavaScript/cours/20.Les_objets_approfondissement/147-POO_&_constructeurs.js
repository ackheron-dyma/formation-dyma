// La fonction constructeur "Utilisateurs" prend trois paramètres : "nom", "prenom" et "age". Lorsque vous créez une nouvelle instance d'utilisateur en utilisant le mot-clé "new" suivi de la fonction constructeur et des arguments nécessaires, les propriétés de l'utilisateur sont définies en utilisant les valeurs fournies.
function Utilisateurs(nom, prenom, age) {
    this.nom = nom;
    this.prenom = prenom;
    this.age = age;
    this.dateCreation = new Date();
    this.admin = false;
}

/**
 *
 L'opérateur new
L'utilisation de l'opérateur new sur une fonction entraîne quatre effets effectués par le moteur JavaScript :
1 - Un objet vide est créé auquel this est assigné.
2 - Les instructions de la fonction sont exécutées : nous ajoutons des propriétés et des méthodes à l'objet vide.
3 - this, donc le nouvel objet avec les nouvelles propriétés, est retourné.
4 - Le prototype de l'objet instancié est le prototype du constructeur (nous y reviendrons en détails dans les leçons suivantes).

function unConstructeur(item, item2) {
    this = {}; // implicitement effectué par le moteur JS

    this.item = item;
    this.item2 = item2,

    return this; // implicitement effectué par le moteur JS
}

 */
const jean = new Utilisateurs("Dupont", "Jean", 45);

const paul = new Utilisateurs("Vidal", "Paul", 23);
