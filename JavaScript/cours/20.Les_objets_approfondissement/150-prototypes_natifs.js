/*=============================================
=            Le prototype de Object            =
=============================================*/
/* Lorsque nous créons un objet en JavaScript, nous créons une instance de la fonction constructeur native Object.
Le [[ Prototype ]] d'un objet est donc bien la propriété prototype de la fonction constructeur Object, et sa fonction constructeur est Object. */

const type = typeof Object; // function
console.log("🚀 ~ file: 150-prototypes_natifs.js:2 ~ type:", type);

const test = new Object(); // ou le raccourci {}

// Cette condition est vraie, car constructor fait référence à la fonction constructeur utilisée pour créer l'objet, qui est Object.
test.constructor === Object; // true
// Cette condition est également vraie, car __proto__ fait référence au prototype de l'objet, et dans ce cas, il est lié à Object.prototype.
test.__proto__ = Object.prototype; // true

/*=============================================
=            Le prototype de Function            =
=============================================*/
/*  la fonction constructeur native Function est utilisée pour créer des objets de type fonction en JavaScript. Lorsqu'une fonction est créée en utilisant Function, son prototype est lié à la propriété prototype de la fonction constructeur Function, et sa fonction constructeur est Function. Cela permet à la fonction d'accéder aux propriétés et méthodes définies sur Function.prototype */
const typeFunction = typeof Function; // function
console.log("🚀 ~ file: 150-prototypes_natifs.js:22 ~ typefunction:", typeFunction);

const myFonction = new Function();
console.log("🚀 ~ file: 150-prototypes_natifs.js:25 ~ myFonction:", myFonction);

//  Cette condition est vraie, car constructor fait référence à la fonction constructeur utilisée pour créer l'objet, qui est Function.
myFonction.constructor === Function; // true;
// Cette condition est également vraie, car __proto__ fait référence au prototype de l'objet, et dans ce cas, il est lié à Function.prototype.
myFonction._proto__ === Function.prototype; // true

/*=============================================
=            Tout est objet en Javascript            =
=============================================*/
/* En JavaScript, tout est un objet ou peut être traité comme un objet. Même les types de données primitifs ont des prototypes qui sont liés à Object.prototype. Cela permet d'utiliser des fonctionnalités et des méthodes supplémentaires en accédant à leurs prototypes, et cela contribue à la flexibilité et à la puissance du langage JavaScript. */

const tableau = new Array();
const date = new Date();
console.log("🚀 ~ file: 150-prototypes_natifs.js:38 ~ date:", date);
const fonction = new Function();
const primitive = new Number();

// Dans chaque cas, vous obtenez true en vérifiant que __proto__.__proto__ === Object.prototype. Cela signifie que le prototype du prototype de chaque objet est lié à Object.prototype, ce qui est conforme à la nature orientée objet de JavaScript.
const log = tableau.__proto__.__proto__ === Object.prototype;
console.log("🚀 ~ file: 150-prototypes_natifs.js:42 ~ log:", log); // true

const log2 = date.__proto__.__proto__ === Object.prototype;
console.log("🚀 ~ file: 150-prototypes_natifs.js:44 ~ log2:", log2); // true

const log3 = fonction.__proto__.__proto__ === Object.prototype;
console.log("🚀 ~ file: 150-prototypes_natifs.js:46 ~ log3:", log3); // true

const log4 = primitive.__proto__.__proto__ === Object.prototype;
console.log("🚀 ~ file: 150-prototypes_natifs.js:48 ~ log4:", log4); // true
