/*=============================================
=            Les accesseurs (getters)            =
Les accesseurs permettent de renvoyer des valeurs dynamiquement calculées en utilisant les autres propriétés d'un objet.
Un accesseur se définit avec le mot clé get.
=============================================*/

//Accesseurs (Getter) définit dans un objet littéral

const user = {
    prénom: "Jean",
    nom: "Dupont",
    get nomComplet() {
        return `${this.prénom} ${this.nom}`;
    },
};

console.log("🚀 ~ file: 146-accesseurs_&_mutateurs.js:5 ~ getnomComplet ~ nomComplet:", user.nomComplet);

//Accesseurs (Getter) définit par la méthode defineProperty() du constructeur Object.
const newUser = {
    prénom: "Bill",
    nom: "Duke",
};
Object.defineProperty(newUser, "nomComplet", {
    get() {
        return `${this.prénom} ${this.nom}`;
    },
    enumerable: false,
    configurable: false,
});

console.log(newUser.nomComplet);

// Exemple d'accesseurs qui retourne la moyenne d'un table de notes[] de l'objet notesObject{}
const notesObject = {
    notes: [12, 16, 2, 18, 15, 6],
    get moyenneNotes() {
        if (this.notes.length > 0) {
            const total = this.notes.reduce((previousValue, currentValue) => currentValue + previousValue, 0);
            return total / this.notes.length;
        } else {
            return null;
        }
    },
};
console.log(notesObject.moyenneNotes);

/*=============================================
=            Les mutateurs (setters)            =
Un mutateur permet de lier une propriété d'un objet à une fonction qui sera exécutée pour calculer la valeur finale de la propriété dynamiquement à chaque affectation.
Un mutateur se définit avec le mot clé set.
=============================================*/

/*----------  Premier exemple  ----------*/

//Lorsque la chaîne de caractères "Jean Maurice" est affectée à la propriété nomComplet de l'objet utilisateur, l'accesseur se déclenche et utilise la méthode split() pour diviser la chaîne en deux parties séparées par un espace et retourner un tableau affecter par décomposition à gauche de l'opérateur d'affectation. Ces deux parties sont ensuite affectées aux propriétés prénom et nom de l'objet utilisateur respectivement
const utilisateur = {
    prénom: "",
    nom: "",
    set nomComplet(value) {
        [this.prénom, this.nom] = value.split(" ");
    },
};

utilisateur.nomComplet = "Jean Maurice";

console.log("🚀 ~ file: 146-accesseurs_&_mutateurs.js:61 ~ utilisateur:", utilisateur.prénom);
console.log("🚀 ~ file: 146-accesseurs_&_mutateurs.js:61 ~ utilisateur:", utilisateur.nom);

/*----------  Second exemple  ----------*/

// Lorsque la chaîne de caractères "fr-FR" est affectée à la propriété locale de l'objet preference, l'accesseur se déclenche et ajoute cette valeur à la fin du tableau historiqueLocale de l'objet preference. Ensuite, la propriété localeActuelle de l'objet preference est définie sur "fr-FR"
const preference = {
    historiqueLocale: [],
    set locale(value) {
        this.historiqueLocale.push(value);
        this.localeActuelle = value;
    },
};

preference.locale = "fr-FR";

console.log("🚀 ~ file: 146-accesseurs_&_mutateurs.js:74 ~ preference:", preference.historiqueLocale);
console.log("🚀 ~ file: 146-accesseurs_&_mutateurs.js:74 ~ preference:", preference.localeActuelle);

/*----------  Troisième exemple  ----------*/

// Le code suivant définit un objet notesObjectSetter qui a deux propriétés : notes et moyenneNotes. L'objet a également une propriété addNote définie comme un mutateur avec le mot-clé set. Cette propriété est utilisée pour ajouter une note à la fin du tableau notes de l'objet notesObjectSetter. Lorsque la propriété addNote est modifiée, le mutateur se déclenche et ajoute la note à la fin du tableau notes. Ensuite, si la longueur du tableau notes est supérieure à 0, l'objet calcule la moyenne des notes et définit la propriété moyenneNotes de l'objet notesObjectSetter sur cette valeur.
const notesObjectSetter = {
    notes: [12, 16, 13, 6, 10],
    set addNote(note) {
        this.notes.push(note);
        if (this.notes.length > 0) {
            const total = this.notes.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
            this.moyenneNotes = total / this.notes.length;
        } else {
        }
    },
};

notesObjectSetter.addNote = 20;
console.log("🚀 ~ file: 146-accesseurs_&_mutateurs.js:99 ~ notesObjectSetter:", notesObjectSetter.moyenneNotes);
