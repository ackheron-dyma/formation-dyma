/*=============================================
=            Accéder au [[ Prototype ]] d'un objet            =
=============================================*/
/* En effet, il est totalement déconseillé d'utiliser __proto__ et d'utiliser le pattern des fonctions constructeurs. Nous allons étudier les approches modernes pour l'héritage en JavaScript. */

const vehicule = {
    hasEngine: true,
    marque: "bmw",
};

// Pour accéder au [[ Prototype ]] de cet objet, utilisation de la méthode Object.getPrototypeOf(objet). Cette méthode renvoie le [[ Prototype ]] de l'objet spécifié.
const testPrototype = Object.getPrototypeOf(vehicule);
// Ici le [[Prototype]] de `vehicule` et tout simplement le l'objet global `Object`
console.log("🚀 ~ file: 151-utilisation_prototypes.js:12 ~ testPrototype:", testPrototype);

/*=============================================
=            Créer un objet en spécifiant son [[ Prototype ]]            =
=============================================*/
/* Object.create() permet de créer un nouvel objet en passant en argument le [[ Prototype ]] de l'objet créé. */

const animal = {
    vivant: true,
};

// Création d'un nouvel objet "tigre" en utilisant "animal" comme prototype
const tigre = Object.create(animal);
tigre.pattes = 4;
console.log("🚀 ~ file: 151-utilisation_prototypes.js:26 ~ tigre:", tigre);

const testCreateProto = Object.getPrototypeOf(tigre);
console.log("🚀 ~ file: 151-utilisation_prototypes.js:29 ~ testCreateProto:", testCreateProto);

/*=============================================
=            Définir le [[ Prototype ]] d'un objet            =
=============================================*/

/* Plutôt que d'utiliser __proto__, il faut utiliser la méthode setPrototypeOf() pour définir le [[ Prototype ]] d'un objet déjà créé. */

const gpu = {
    dx12: true,
};

const amd = {
    brand: "AMD",
    country: "USA",
};

Object.setPrototypeOf(amd, gpu);

console.log("🚀 ~ file: 151-utilisation_prototypes.js:47 ~ amd:", amd);
