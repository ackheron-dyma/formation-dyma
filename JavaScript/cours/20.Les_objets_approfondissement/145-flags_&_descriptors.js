/* le descripteur d'une propriété
Par défaut, lors d'une affectation, une propriété a des attributs, appelés property flags qui sont automatiquement définis : */

const monObjet = {
    a: 1,
};

/* Object.getOwnPropertyDescriptor() permet d'accéder aux attributs d'une propriété d'un objet.
- value:  est la valeur de la propriété que nous avons affectée.
- writable: est un booléen qui indique si la valeur de la propriété peut être modifiée.
- enumerable: est un booléen qui indique si la propriété sera accessible lors d'itérations par des boucles par exemple.
- configurable:  est un booléen qui indique si la propriété peut être supprimée avec delete et si les attributs (autre que la valeur) peuvent être modifiés.
 */
const descriptor = Object.getOwnPropertyDescriptor(monObjet, "a");
console.log("🚀 ~ file: 145-flags_&_descriptors.js:6 ~ descriptor:", descriptor);

/*----------  *********************************  ----------*/

// Il est possible de modifier un ou plusieurs attributs d'un objet en utilisant Object.defineProperty() :
// En créant un nouvel objet vide les attributs `writable`, `configurable` et `iterable` sont définis à false par défaut :
const nouvelObjet = {};

// On crée une propriété à l'objet `nouvelObjet` qui a pour clé "key et on lui édite des attributs"
Object.defineProperty(nouvelObjet, "key", {
    // Nous passons à l'attribut `value` la valeur de 42
    value: 42,
    // Nous passons l'attribut `configurable` à true
    configurable: true,
});

const descriptor02 = Object.getOwnPropertyDescriptor(nouvelObjet, "key");
console.log("🚀 ~ file: 145-flags_&_descriptors.js:17 ~ descriptor02:", descriptor02);

// On édite un nouvel attribut de la propriété `key` de l'objet `nouvelObjet`
Object.defineProperty(nouvelObjet, "key", {
    writable: true,
});

const descriptor03 = Object.getOwnPropertyDescriptor(nouvelObjet, "key");
console.log("🚀 ~ file: 145-flags_&_descriptors.js:26 ~ descriptor03:", descriptor03);

/*----------  *********************************  ----------*/

// MODIFIER LES ATTRIBUTS DE PLUSIEURS PROPRIÉTÉS D'UN OBJET AVEC Object.defineProperties()

const encoreObject = {};

Object.defineProperties(encoreObject, {
    key: { value: 256, writable: true },
    key2: { value: "Trésor", configurable: true },
});

// ACCÉDER À TOUS LES DESCRIPTEURS DES PROPRIÉTÉS D'UN OBJET AVEC getOwnPropertyDescriptors()
const descriptor04 = Object.getOwnPropertyDescriptors(encoreObject);
console.log("🚀 ~ file: 145-flags_&_descriptors.js:36 ~ descriptor04:", descriptor04);
