"use strict";
/*=============================================
=            Object.freeze(objet) et Object.isFrozen(objet)            =
=============================================*/

const obj = {
    solution: 42,
};

// Object.freeze(obj) est une méthode qui fige un objet, empêchant toute modification de ses propriétés. Une fois qu'un objet est figé, vous ne pouvez pas ajouter de nouvelles propriétés, supprimer des propriétés existantes ni modifier les valeurs de ses propriétés
Object.freeze(obj);

// Object.isFrozen(objet) retourne un booléen permettant de savoir si l'objet est gelé
console.log(Object.isFrozen(obj));

// Après avoir figé l'objet, toute tentative de modification de ses propriétés générera une erreur en mode strict. En mode non strict les tentatives de modification échoueront silencieusement, c'est-à-dire qu'elles ne produiront aucune erreur, mais les modifications ne seront pas prises en compte.
obj.solution = 43; // "solution" is read only
obj.test = 1; // can't define property "test": Object is not extensible
Object.defineProperty(obj, "solution", { value: 0 }); // can't redefine no-configurable property "solution"
delete obj.solution; // property "solution" is non configurable and can't be deleted
Object.setPrototypeOf(obj, null); // can't prototype of this object

// il est possible de geler un tableau également

const tableau = [1, 3, 43, "test"];

Object.freeze(tableau);

tableau.push(69); // can't define array index property past the end of an array with non-writable length
tableau.pop(); // property 3 is non-configurable and can't be deleted

/*=============================================
=            Object.seal(objet) et Object.isSealed(objet)            = 
=============================================*/

// Object.Seal permet de sceller un objet pour empêcher l'ajout de nouvelles propriétés ou la suppression de propriétés

const obj2 = {
    solution: 28,
};

Object.seal(obj2);

// Ces action ne fonctionneront pas
delete obj2.solution;
obj2.test = 1;
Object.defineProperty(obj2, "test2", {
    value: 13,
});

// Par contre les propriétés existantes peuvent être modifiés
obj2.solution = 12;
Object.defineProperty(obj2, "solution", {
    value: 79,
});

// Object.isSealed(objet) retourne un booléen permettant de savoir si l'objet est scellé
Object.isSealed(obj2);

/*=============================================
=            Object.preventExtension(objet) et Object.isExtensible(objet)            =
=============================================*/

// Object.preventExtension permet d'empêcher l'ajout de nouvelles propriété sur objet. C'est donc le version la moins stricte de trois (les autres étant Object.freeze et Object.seal)

const obj3 = {
    solution: 89,
};

Object.preventExtensions(obj3);

// Ne fonctionnera pas
obj3.test = 2;
Object.defineProperty(obj3, "test3", {
    value: 78,
});

// Fonctionnera
obj3.solution = 12;
delete obj3.solution;

// Object.isExtensible retourne un booléen permettant de savoir si objet est extensible

/*=============================================
=            Object.getOwnPropertyNames(objet)            =
=============================================*/
// Retourne un tableau contenant uniquement les clés de toutes les propriétés propres d'un objet
const obj4 = {
    solution: 42,
    unTableau: [1, 2, 3],
    uneFonction() {
        console.log(42);
    },
};

Object.getOwnPropertyNames(obj); // ["solution", "unTableau", "uneFonction"]

// Si nous souhaitons avoir que les propriétés énumérables, nous utiliserons `Object.keys(objet)`

/*=============================================
=            Object.is(valeur1, valeur2)            =
=============================================*/
// Permet de déterminer si deux valeurs passées en arguments sont identiques.
// Il existe des différences entre Object.is() et le comparateur d'égalité stricte ===
/* La méthode statique Object.is() détermine si deux valeurs sont la même valeur1. Deux valeurs sont les mêmes si l’une des conditions suivantes est vraie:

    les deux sont undefined
    les deux sont null
    les deux sont true ou les deux sont false
    les deux sont des chaînes de la même longueur avec les mêmes caractères dans le même ordre
    les deux sont le même objet (ce qui signifie que les deux valeurs référencent le même objet en mémoire)
    les deux sont des BigInts avec la même valeur numérique
    les deux sont des symbols qui référencent la même valeur symbolique
    les deux sont des nombres et les deux +0, les deux -0, les deux NaN ou les deux non-zéro, non-NaN, et ont la même valeur */

Object.is(+0, -0); // false
+0 === -0; // true

Object.is(0, -0); // false
0 === -0; // true

Object.is(NaN, NaN); //true
NaN === NaN; // false
