/*=============================================
=            Premier exemple de lien prototypal            =
=============================================*/
/*En JavaScript, tous les objets ont une propriété spéciale interne le [[Prototype]].
 __proto__ utilise un getter pour accéder au [[Prototype]] de l'objet et un setter pour le modifier. Nous verrons qu'il n'est pas recommandé de l'utiliser, car il est uniquement défini dans les spécifications des navigateurs et non du langage ECMAScript, et qu'il est moins performant que d'autres fonctions plus optimisées.
 */

// L'objet "voiture" est défini avec des propriétés telles que "roues" (4) et "moteur" (true), ainsi qu'une méthode "demarrer()" qui affiche "Vrouuum !" lorsque appelée.
const voiture = {
    roues: 4,
    moteur: true,
    demarrer() {
        console.log("Vrouuum !");
    },
};

// Ensuite, l'objet "tesla" est créé avec une propriété spécifique "marque" définie sur "Tesla".
const tesla = {
    marque: "Tesla",
};

// Pour établir un lien de prototype entre les deux objets, la ligne "tesla.__proto__ = voiture;" est utilisée. Cela signifie que "tesla" hérite du prototype de l'objet "voiture". Ainsi, "tesla" peut accéder aux propriétés et méthodes de "voiture".
tesla.__proto__ = voiture;
console.log("🚀 ~ file: 148-introduction_prototypes.js:16 ~ tesla:", tesla);

// La propriété "roues" est héritée de l'objet "voiture", donc "tesla" peut accéder à cette propriété et affiche la valeur "4".
console.log(tesla.roues);
// La méthode "demarrer()" est également héritée de l'objet "voiture". L'appel de cette méthode sur "tesla" affiche "Vrouuum !" dans la console
tesla.demarrer();
// La propriété "marque" est spécifique à l'objet "tesla". Elle n'est pas héritée de l'objet "voiture". Dans ce cas, "tesla" conserve sa propre valeur pour "marque", qui est "Tesla".
console.log(tesla.marque);

/*En utilisant le lien prototypal "__proto__", vous pouvez établir des relations d'héritage entre des objets JavaScript et accéder aux propriétés et méthodes de l'objet prototype. */

/*=============================================
=            La chaîne des prototypes            =
=============================================*/
/* Lorsque nous tentons d'utiliser une propriété ou une méthode sur un objet, le moteur va d'abord regarder sur l'objet.
Si il ne la trouve pas sur l'objet, il va regarder si elle existe sur le [[Prototype]] de l'objet.
S'il ne la trouve toujours pas sur le [[Prototype]] il va regarder sur le [[Prototype]] du [[Prototype]] et ainsi de suite jusqu'à que le [[Prototype]] soit null.
C'est ce qu'on appelle la chaîne des prototypes.
Nous pouvons avoir donc autant de niveaux d'héritage que nous voulons en JavaScript, cependant un objet ne peut avoir qu'un seul [[Prototype]] : */

// Ici, le [[Prototype]] de tigre est mammifere, et le [[Prototype]] de mammifere est animal. Nous avons la chaîne prototypale suivante : tigre -> mammifere -> animal.

const animal = {
    vivant: true,
};

const mammifere = {
    allaite: true,
};
const tigre = {
    rugir() {
        console.log("Arggggghhh !");
    },
};
tigre.__proto__ = mammifere;
mammifere.__proto__ = animal;

console.log(tigre.vivant);

/*=============================================
=            Modification dynamique du [[Prototype]]            =
=============================================*/

// Dans cet exemple, nous avons un objet "car" qui possède des propriétés et des méthodes liées à une voiture, telles que "roues", "moteur" et "demarrer()".
const car = {
    roues: 4,
    moteur: true,
    demarrer() {
        console.log("VroumVroum !");
    },
};

// Ensuite, nous créons un objet "mazda" avec une propriété spécifique "marque" définie sur "Mazda".
const mazda = {
    marque: "Mazda",
};

// Pour établir un lien de prototype entre les deux objets, nous utilisons la ligne "mazda.__proto__ = car;", ce qui signifie que "mazda" hérite du prototype de l'objet "car".
mazda.__proto__ = car;

// Ensuite, nous modifions dynamiquement l'objet "car" en ajoutant une nouvelle méthode appelée "freiner" en utilisant la syntaxe "car.freiner = function () {...};". Cette méthode est maintenant disponible sur tous les objets qui héritent de l'objet "car" dans la chaîne des prototypes.
car.freiner = function () {
    console.log(".....hiiiiiii");
};

/* L'appel de la méthode "freiner()" sur l'objet "mazda" exécute la fonction définie dans l'objet "car", car "mazda" hérite de ce prototype. Ainsi, la méthode "freiner()" est disponible et exécutée avec succès sur l'objet "mazda". */

mazda.freiner();

// Cette flexibilité permet de modifier dynamiquement les objets et d'ajouter de nouvelles fonctionnalités à la volée. Cependant, il est important de noter que cette modification affecte tous les objets qui partagent le même prototype, et non pas uniquement l'objet sur lequel la modification est effectuée. */

/*=============================================
=            Utilisation de `for ... in` avec les prototypes            =
=============================================*/

const moto = {
    roues: 2,
    fonctionne: true,
};

const moteur = {
    electric: false,
};

const ducati = {
    start() {
        console.log("BzzzzzRhh !");
    },
};

// Nous établissons un lien de prototype entre ces objets de la manière suivante :
// Cela signifie que l'objet "ducati" hérite du prototype de l'objet "moteur", qui lui-même hérite du prototype de l'objet "moto".
ducati.__proto__ = moteur;
moteur.__proto__ = moto;

/* Ensuite, nous utilisons une boucle for...in pour itérer sur les propriétés de l'objet "ducati" :
 Cela affichera toutes les propriétés de l'objet "ducati" et de ses prototypes. Dans cet exemple, cela affichera :
start
electric
roues
fonctionne
 */
for (const prop in ducati) {
    console.log(prop);
}

/* Ensuite, nous utilisons une autre boucle for...in en vérifiant si chaque propriété est propre à l'objet "ducati" ou héritée :
 Cela permet de distinguer les propriétés propres à l'objet "ducati" de celles héritées de ses prototypes. Dans cet exemple, cela affichera :
Sur l'objet: start
Héritée: electric
Héritée: roues
Héritée: fonctionne
 */
for (const key in ducati) {
    if (ducati.hasOwnProperty(key)) {
        console.log(`Sur l'objet: ${key}`);
    } else {
        console.log(`Héritée: ${key}`);
    }
}

/* AINSI, LA BOUCLE FOR...IN NOUS PERMET D'ITÉRER SUR TOUTES LES PROPRIÉTÉS D'UN OBJET, Y COMPRIS CELLES HÉRITÉES DE SES PROTOTYPES. EN UTILISANT HASOWNPROPERTY(), NOUS POUVONS DISTINGUER LES PROPRIÉTÉS PROPRES DE L'OBJET DES PROPRIÉTÉS HÉRITÉES. */
