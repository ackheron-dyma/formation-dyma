/*=============================================
=            Ce code JavaScript démontre différentes façons de créer des objets Date en utilisant le constructeur new Date() avec différents types d'arguments,            =
=============================================*/

// Le premier objet Date, today, est créé en utilisant le constructeur new Date(), qui initialise l'objet Date avec la date et l'heure actuelles de l'ordinateur.
const today = new Date();
console.log("🚀 ~ file: 128-déclarer_date.js:2 ~ today:", today);

// Le deuxième objet Date, date, est créé en utilisant le constructeur new Date() avec un seul argument, qui est le nombre de millisecondes depuis l'époque Unix (1er janvier 1970 00:00:00 UTC) jusqu'à la date souhaitée. Dans ce cas, le nombre de millisecondes correspond à un an (1000 millisecondes x 60 secondes x 60 minutes x 24 heures x 365 jours).
const date = new Date(1000 * 3600 * 24 * 365);
console.log("🚀 ~ file: 128-déclarer_date.js:5 ~ date:", date);

// Le troisième objet Date, date2, est créé en utilisant le constructeur new Date() avec cinq arguments, qui représentent respectivement l'année (2020), le mois (0 pour janvier), le jour du mois (2), l'heure (10) et les minutes (50) de la date souhaitée.
const date2 = new Date(2020, 0, 2, 10, 50);
console.log("🚀 ~ file: 128-déclarer_date.js:8 ~ date2:", date2);
