/*=============================================
=            Ce code JavaScript démontre différentes façons de formater et d'afficher des chaînes de caractères représentant la date et l'heure à l'aide de différentes méthodes de l'objet Date. Chaque méthode de formatage de chaîne de caractères renvoie une chaîne de caractères dans un format spécifique.            =
=============================================*/

const date3 = new Date();
//  la méthode toString() est appelée sur cet objet pour retourner une chaîne de caractères représentant la date et l'heure au format local en précisant le fuseau horaire. C'est le format par défaut.
console.log("🚀 ~ file: 129-date_strings.js:5 ~ date3:", date3.toString());

const date4 = new Date();
/*   la méthode toDateString() est appelée sur cet objet pour retourner une chaîne de caractères représentant la date au format local (jour de la semaine, mois, jour du mois, année).
Ce format est standardisé mais est recommandé uniquement pour l'affichage en développement.
En effet, il ne contient pas le fuseau horaire de référence et ne peut donc être utilisé pour le stockage.
Et il ne convient pas à l'affichage aux utilisateurs car il contient des abréviations et affiche uniquement une chaîne de caractères en anglais. */
console.log("🚀 ~ file: 129-date_strings.js:9 ~ date4:", date4.toDateString());

const date5 = new Date();
/* la méthode toUTCString() est appelée sur cet objet pour retourner une chaîne de caractères représentant la date et l'heure au format UTC. Ce format est également standardisé. Il permet d'inclure également le fuseau horaire ce qui en fait une date fiable. Cependant, elle ne convient pas particulièrement au stockage. Elle n'est pas assez précise (pas de millisecondes).   */
console.log("🚀 ~ file: 129-date_strings.js:16 ~ date5:", date5.toUTCString());

const date6 = new Date();
/*  la méthode toISOString() est appelée sur cet objet pour retourner une chaîne de caractères représentant la date et l'heure au format ISO 8601 (année-mois-jourTheure:minute:seconde.millisecondesZ) .
------
Les parties de la date sont séparées par des tirets -
------
'T' pour time, le séparateur avant l'heure, les parties de l'heure sont séparées par des :
------
Les millisecondes sont séparées des secondes par un point .
------
'Z' indique que la date et l'heure sont exprimées en UTC, ce qui signifie qu'il n'y a aucun décalage horaire par rapport à l'heure UTC.
------
la méthode toISOString() est souvent utilisée pour stocker la date et l'heure dans les bases de données et sur les serveurs,
  */
console.log("🚀 ~ file: 129-date_strings.js:20 ~ date6:", date6.toISOString());
