/*=============================================
=            L'API des objets Date            =
=============================================*/

// O1- LIRES DES DATES

const apiDate = new Date();
// getDate permet d'obtenir le jour du mois (1 à 31).
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getDate());
// getFullYear() permet d'obtenir les quatre chiffres de l'année.
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getFullYear());
// getMonth() permet d'obtenir l'index du mois (entre 0 et 11).
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getMonth());
// getDay() permet d'obtenir le jour de la semaine (0 à 6). Mais attention ! Cela utilise le système américain où le premier jour de la semaine est le dimanche et non le lundi comme en Europe, donc 0 est dimanche
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getDay());
// getHours() permet d'obtenir l'heure (entre 0 et 23).
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getHours());
// getMinutes() permet d'obtenir les minutes (entre 0 et 59).
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getMinutes());
// getSeconds() permet d'obtenir les secondes (entre 0 et 59).
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getSeconds());
// getMilliseconds() permet d'obtenir les millisecondes (entre 0 et 999).
console.log("🚀 ~ file: 131-api_objets_date.js:2 ~ apiDate:", apiDate.getMilliseconds());

/* Les méthodes utilisant UTC
Toutes les méthodes précédentes ont un équivalent pour obtenir des informations mais plus à partir de l'heure locale mais de UTC.
Il suffit d'ajouter UTC : getUTCDate(), getUTCDay(), getUTCFullYear(), getUTCMonth(), getUTCHours(), getUTCMinutes(), getUTCSeconds() et getUTCMilliseconds().  */

/* ---------------------------------------------------------------------------------------------------------------------- */

// 02- MODIFIER DES DATES

const apiDate2 = new Date();
// setDate() : permet de définir le jour du mois (1 à 31) pour la date spécifiée.
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setDate(11));
// setFullYear() : permet de définir l'année en utilisant 4 chiffres. Vous pouvez également optionnellement passer un deuxième argument pour définir l'index du mois (0 à 11) et un troisième argument pour définir le jour (1 à 31).
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setFullYear(1998));
// setMonth() : permet de définir l'index du mois (0 à 11), et optionnellement le jour en deuxième argument, en passant un nombre entre 1 et 31.
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setMonth(9));
// setHours() : permet de définir l'heure (0 à 23). Vous pouvez également optionnellement passer un deuxième argument pour les minutes, un troisième pour les secondes et un quatrième pour les millisecondes.
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setHours(16));
// setMinutes() : permet de définir les minutes (0 à 59). Vous pouvez également optionnellement passer un deuxième argument pour les secondes et un troisième pour les millisecondes.
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setMinutes(45));
// setSeconds() : permet de définir les secondes (0 à 59). Vous pouvez également optionnellement passer un deuxième argument pour les millisecondes.
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setSeconds(56));
// setMilliseconds() : permet de définir le nombre de millisecondes (0 à 999).
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setMilliseconds(555));

// Affichage de la date complète qui a été modifié avec les méthodes au dessus
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2);

// setTime() permet de définir une date en utilisant un timestamp :
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2.setTime(15628836));

// Affichage de la date complète qui a été modifié avec la méthode setTime() et le timestamp
console.log("🚀 ~ file: 131-api_objets_date.js:26 ~ apiDate2:", apiDate2);

/*Les méthodes utilisant UTC
Toutes les méthodes précédentes ont un équivalent pour définir des dates mais en utilisant UTC.
Il suffit d'ajouter UTC : setUTCDate(), setUTCFullYear(), setUTCMonth(), setUTCHours(), setUTCMinutes(), setUTCSeconds() et setUTCMilliseconds(). */
