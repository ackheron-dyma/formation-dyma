Il ya différences dans la manière dont Chrome et Firefox implémentent la méthode toString() pour les objets Date.

Dans Chrome, lorsque vous appelez console.log sur un objet Date, le navigateur appelle implicitement la méthode toString() pour afficher une représentation sous forme de chaîne de caractères de l'objet Date.

En revanche, dans Firefox, console.log n'appelle pas implicitement la méthode toString() pour afficher la représentation sous forme de chaîne de caractères de l'objet Date. Au lieu de cela, il affiche directement l'objet Date dans la console.

Cela peut être utile si vous voulez inspecter les propriétés de l'objet Date (comme les heures, les minutes et les secondes) directement depuis la console. Toutefois, si vous souhaitez afficher une représentation sous forme de chaîne de caractères de l'objet Date dans Firefox, vous pouvez appeler explicitement la méthode toString() sur l'objet Date, comme ceci :

```javascript
const myDate = new Date();
console.log(myDate.toString()); // affiche la représentation sous forme de chaîne de caractères de l'objet Date
console.log(myDate); // affiche directement l'objet Date
```

En général, il est préférable de s'en tenir à la méthode console.log() pour afficher les objets Date dans la console, car cela fonctionne de manière cohérente sur tous les navigateurs.
