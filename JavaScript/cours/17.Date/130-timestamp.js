/*=============================================
=            Le code JavaScript suivant utilise différentes méthodes pour manipuler et convertir des timestamps en JavaScript... le timestamp est le nombre en millisecondes depuis epoch:
            =
=============================================*/

/*La première méthode utilise le constructeur new Date() pour créer un nouvel objet Date qui représente le moment présent. Ensuite, la méthode getTime() est appelée sur l'objet Date pour récupérer le timestamp correspondant à ce moment en millisecondes depuis le 1er janvier 1970.  */
const time = new Date();
console.log("🚀 ~ file: 130-timestamp.js:2 ~ time:", time.getTime());

/*La deuxième méthode utilise la méthode statique Date.now() pour récupérer le timestamp actuel en millisecondes depuis le 1er janvier 1970. */
const now = Date.now();
console.log("🚀 ~ file: 130-timestamp.js:5 ~ now:", now);

/*La troisième méthode utilise la méthode statique Date.parse() pour convertir une chaîne de caractères représentant une date en un timestamp. Deux exemples sont donnés: "2019-02-10" et "2019-02-10T15:20:00". Dans le premier cas, la chaîne de caractères représente le 10 février 2019 à minuit (UTC), donc le timestamp correspondant est affiché dans la console. Dans le deuxième cas, la chaîne de caractères représente le 10 février 2019 à 15h20m00s (heure locale). Dans le troisième on ajoute le fuseau horaire (UTC + 1).

Attention ! Si vous ne précisez pas de fuseau horaire, les chaînes de caractères contenant uniquement des dates seront considérées comme en UTC et celles contenant des dates et des heures seront considérées comme au fuseau horaire de votre système. 
 */
const parse = Date.parse("2019-02-10");
const parse2 = Date.parse("2019-02-10T15:20:00");
const parse3 = Date.parse("2019-02-10T15:20:00.000+01:00");
console.log("🚀 ~ file: 130-timestamp.js:7 ~ parse:", parse);
console.log("🚀 ~ file: 130-timestamp.js:9 ~ parse2:", parse2);
console.log("🚀 ~ file: 130-timestamp.js:19 ~ parse3:", parse3);

/*La dernière méthode utilise le constructeur new Date() pour créer un nouvel objet Date à partir d'une chaîne de caractères représentant une date ("2023-01-11"). Ensuite, la méthode Date.now() est utilisée pour récupérer le timestamp actuel, et une soustraction est effectuée entre les deux timestamps pour calculer la différence en millisecondes entre la date donnée et le moment présent.  */
const date7 = new Date("2023-01-11");
const diff = Date.now() - date7;
console.log("🚀 ~ file: 130-timestamp.js:11 ~ diff:", diff);

// BONUS => nous avons divisé la valeur de diff par (1000 * 60 * 60 * 24), qui correspond au nombre de millisecondes dans une journée (1000 millisecondes * 60 secondes * 60 minutes * 24 heures = 86400000 millisecondes). Nous avons ensuite utilisé Math.floor() pour arrondir le résultat à l'entier inférieur. Cela nous donne le nombre de jours écoulés entre la date actuelle et la date du 11 janvier 2023.
const daysDiff = Math.floor(diff / (1000 * 60 * 60 * 24));
console.log("🚀 ~ file: 130-timestamp.js:30 ~ daysDiff:", daysDiff);
