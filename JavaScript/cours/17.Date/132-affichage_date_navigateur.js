/*=============================================
=            Méthodes utiles pour l'affichage en date et heure locale dans le navigateur            =
=============================================*/

// 01- LA MÉTHODE TO LOCATE STRING
/*  La méthode toLocateString() permet de renvoyer une chaîne de caractères correspondant à une date et une heure locale. Elle est extrêmement paramétrable.
 C'est-à-dire que nous seulement la méthode va utiliser le fuseau horaire du système local pour l'affichage, mais également la langue et les formats d'affichage des dates dans la langue.
  Cette méthode est extrêmement puissante car elle permet de prendre de très nombreuses options. 
*/
const screenDate = new Date();
// Les lignes suivantes affichent la date et l'heure de "screeDate" en français, anglais américain et allemand autrichien
console.log("🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:", screenDate.toLocaleString("fr-FR"));
console.log("🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:", screenDate.toLocaleString("en-US"));
console.log("🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:", screenDate.toLocaleString("de-AT"));

// Le deuxième argument de cette méthode est un objet d'options qui permet de personnaliser l'affichage en fonction des préférences locales. Voici une liste des options disponibles

//weekday: Cet option permet de spécifier le format du jour de la semaine. Les valeurs possibles sont "narrow" (un seul caractère), "short" (abréviation) et "long" (nom complet).
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        weekday: "narrow",
    })
);
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        weekday: "long",
    })
);
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        weekday: "short",
    })
);

// month: Cette option permet de spécifier le format du mois. Les valeurs possibles sont "numeric" (par exemple, 12), "2-digit" (par exemple, 12), "narrow" (un seul caractère), "short" (abréviation) et "long" (nom complet).
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        month: "short",
    })
);
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        month: "long",
    })
);
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        month: "narrow",
    })
);
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        month: "numeric",
    })
);
console.log(
    "🚀 ~ file: 132-affiche_date_navigateur.js:6 ~ screenDate:",
    screenDate.toLocaleString("fr-FR", {
        month: "2-digit",
    })
);
/* LES AUTRES OPTIONS SONT
year: Cette option permet de spécifier le format de l'année. Les valeurs possibles sont "numeric" (par exemple, 2022), "2-digit" (par exemple, 22) ou bien undefined pour afficher l'année par défaut.

day: Cette option permet de spécifier le format du jour. Les valeurs possibles sont "numeric" (par exemple, 31) et "2-digit" (par exemple, 31).

hour: Cette option permet de spécifier le format de l'heure. Les valeurs possibles sont "numeric" (par exemple, 23) et "2-digit" (par exemple, 23).

minute: Cette option permet de spécifier le format des minutes. Les valeurs possibles sont "numeric" (par exemple, 59) et "2-digit" (par exemple, 59).

second: Cette option permet de spécifier le format des secondes. Les valeurs possibles sont "numeric" (par exemple, 59) et "2-digit" (par exemple, 59).

timeZoneName: Cette option permet de spécifier si l'affichage du nom de la zone horaire est requis. Les valeurs possibles sont "short" (abréviation) et "long" (nom complet).
*/

/*------------------------------------------------------------------------------------------------------------------------------------- */

// 02- LA MÉTHODE toLocateDateString

// la méthode toLocateDateString() permet de formater une date selon les paramètres régionaux du navigateur (langue, fuseau horaire, etc.). Elle renvoie une chaîne de caractères représentant seulement la date dans le format local.
const screenDate2 = new Date();
console.log("🚀 ~ file: 132-affiche_date_navigateur.js:88 ~ screenDate2:", screenDate2.toLocaleDateString("fr-FR"));
//  Elle peut prendre les mêmes arguments que toLocateString()

/*------------------------------------------------------------------------------------------------------------------------------------- */

// 03- LA MÉTHODE toLocateTimeString

// la méthode toLocateTimeString() permet de formater une date selon les paramètres régionaux du navigateur (langue, fuseau horaire, etc.). Elle renvoie une chaîne de caractères représentant seulement l'heure dans le format local.
const screenDate3 = new Date();
console.log("🚀 ~ file: 132-affiche_date_navigateur.js:97 ~ screenDate3:", screenDate3.toLocaleTimeString("fr-FR"));
//  Elle peut prendre les mêmes arguments que toLocateString()
