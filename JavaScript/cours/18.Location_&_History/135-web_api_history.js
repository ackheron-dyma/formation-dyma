/*=============================================
=            La Web API history            =

L'objet history contient l'historique de navigation de la session en cours. Il permet de naviguer vers les pages précédentes ou suivantes dans l'historique du navigateur.
=============================================*/

//  back(): Cette méthode permet de naviguer vers la page précédente dans l'historique du navigateur. Elle est équivalente à cliquer sur le bouton "Retour" du navigateur.
history.back();

// forward(): Cette méthode permet de naviguer vers la page suivante dans l'historique du navigateur. Elle est équivalente à cliquer sur le bouton "Suivant" du navigateur.
history.forward();

// go(n): Cette méthode permet de naviguer vers une page spécifique dans l'historique du navigateur. Elle prend un paramètre entier n qui spécifie le nombre de pages à avancer (si n est positif) ou à reculer (si n est négatif) dans l'historique. Si n est égal à zéro, la méthode recharge simplement la page courante depuis le cache.
history.go(4);

history.go(-5);

console.log(history);
