/*=============================================
=            L'objet state de history            =

le code ci-dessus surveille la valeur de l'élément input de la variable myInput.
Si la valeur change, il la met à jour dans l'historique du navigateur en utilisant la méthode history.replaceState(). 
Cela peut être utile pour garder une trace de la progression de la saisie utilisateur dans un formulaire ou pour faciliter la navigation à travers l'historique du navigateur.
=============================================*/

const myInput = document.querySelector("input");

myInput.addEventListener("input", (event) => {
    // On enregistre la valeur de l'input dans la variable `myValue`
    const myValue = event.target.value;
    // La méthode history.replaceState() remplace l'état courant de l'historique du navigateur avec l'état fourni. Dans ce cas, il ne fournit qu'un seul paramètre qui est un objet d'état contenant la nouvelle valeur de l'élément input.
    history.replaceState({ valueOfMyInput: myValue }, "");
});

console.log(history);
console.log(history.state);
