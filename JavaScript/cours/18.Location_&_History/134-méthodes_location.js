/*=============================================
=            Les méthodes de l'objet location            =
=============================================*/

// assign() : cette méthode permet de charger une nouvelle ressource en remplaçant la ressource actuelle dans l'historique de navigation. Dans l'exemple ci-dessus, location.assign("https://www.startpage.com/fr/") charge la page d'accueil de StartPage en français. Lorsque cette méthode est utilisée, la nouvelle URL est ajoutée à l'historique de navigation, de sorte que l'utilisateur peut utiliser la fonction "Retour" du navigateur pour revenir à la page précédente.
const assignExample = location.assign("https://www.startpage.com/fr/");

// replace() : cette méthode remplace l'URL actuelle par une nouvelle URL, sans ajouter la nouvelle URL à l'historique de navigation. Dans l'exemple ci-dessus, location.replace("https://www.google.com") remplace l'URL actuelle par la page d'accueil de Google. Si l'utilisateur utilise la fonction "Retour" du navigateur après avoir utilisé cette méthode, il ne retournera pas à la page précédente, mais restera sur la même page.
const replaceExample = location.replace("https://www.google.com");

// reload() : cette méthode recharge la page actuelle. Dans l'exemple ci-dessus, location.reload() recharge simplement la page actuelle.
const reloadExample = location.reload();

// toString() : cette méthode renvoie l'URL actuelle sous forme de chaîne de caractères. Dans l'exemple ci-dessus, location.toString() renvoie simplement l'URL actuelle sous forme de chaîne de caractères.
const toStringExample = location.toString();
