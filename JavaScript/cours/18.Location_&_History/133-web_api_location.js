/*=============================================
=            La Web API location

location est un objet global qui fait partie des Web API du navigateur.
Il contient des informations relatives à l'URL du document de la page et permet l'utilisation de méthodes afin d'interagir avec cette URL. 
            =
=============================================*/

const loc = location;
console.log("🚀 ~ file: 133-web_api_location.js:6 ~ loc:", loc);

const hash = location.hash;
console.log("🚀 ~ file: 133-web_api_location.js:6 ~ hash:", hash);

const hostName = location.hostname;
console.log("🚀 ~ file: 133-web_api_location.js:9 ~ hostname:", hostName);

const host = location.host;
console.log("🚀 ~ file: 133-web_api_location.js:12 ~ host:", host);

const href = location.href;
console.log("🚀 ~ file: 133-web_api_location.js:15 ~ href:", href);

const origin = location.origin;
console.log("🚀 ~ file: 133-web_api_location.js:18 ~ origin:", origin);

const pathName = location.pathname;
console.log("🚀 ~ file: 133-web_api_location.js:21 ~ pathName:", pathName);

const port = location.port;
console.log("🚀 ~ file: 133-web_api_location.js:23 ~ port:", port);

const protocol = location.protocol;
console.log("🚀 ~ file: 133-web_api_location.js:27 ~ protocol:", protocol);

const search = location.search;
console.log("🚀 ~ file: 133-web_api_location.js:30 ~ search:", search);

/* ----------------------------------------------------------------------------------------------------------------------------- */

// On peut facilement accéder aux paramètres de la propriété 'search' avec l'objet URL qui donne accès au paramètre 'searchParams', par exemple cette adresse startpage ci dessous, ou encore avec l'adresse courant en passant à new URL ("location.href")

// en deux fois
const url = new URL("https://www.startpage.com/do/dsearch?query=intel");
console.log("🚀 ~ file: 133-web_api_location.js:36 ~ url:", url);

const searchParams = url.searchParams.get("query");
console.log("🚀 ~ file: 133-web_api_location.js:43 ~ searchParams:", searchParams);

// en une fois
const url2 = new URL("https://www.startpage.com/do/dsearch?query=metallica").searchParams.get("query");
console.log("🚀 ~ file: 133-web_api_location.js:46 ~ url2:", url2);
