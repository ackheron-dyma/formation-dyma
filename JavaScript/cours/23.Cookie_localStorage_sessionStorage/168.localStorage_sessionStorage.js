/*=============================================
=            L'API DU LOCALSTORAGE            =
=============================================*/

/* Pour stocker une paire de clé il suffit de faire */

localStorage.setItem("uneClé", "coucou");
localStorage.setItem("secondeClé", "bonjour");
localStorage.setItem("troisièmeClé", 45);

/* Pour récupérer une clé */

let récup = localStorage.getItem("uneClé");
console.log("🚀 ~ file: 168.localStorage_sessionStorage.js:10 ~ récup:", récup);

// Pour supprimer un clé

localStorage.removeItem("uneCléASupprimer");

// Pour supprimer totalement le localStorage

// localStorage.clear()

/* Pour connaître le nombre de paires clé/valeur sauvegardées */

let longueur = localStorage.length;
console.log("🚀 ~ file: 168.localStorage_sessionStorage.js:24 ~ longueur:", longueur);

/*=============================================
=            PARCOURIR LES ÉLÉMENTS DU LOCALSTORAGE            =
=============================================*/

// Première méthode : on utilise Object.keys(localStorage) pour obtenir un tableau des clés du localStorage, puis on utilise une boucle for...of pour itérer sur ce tableau et accéder aux valeurs correspondantes avec getItem(clé)
for (const clé of Object.keys(localStorage)) {
    console.log(`${clé}: ${localStorage.getItem(clé)}`);
}

// Deuxième méthode : on utilise Object.entries(localStorage) pour obtenir un tableau des paires clé/valeur du localStorage, puis on utilise une boucle for...of pour itérer sur ce tableau et décomposer les sous-tableaux avec [clé, valeur]
for (const [clé, valeur] of Object.entries(localStorage)) {
    console.log(`${clé}: ${valeur}`);
}

// Troisième méthode : on utilise Object.entries(localStorage) pour obtenir un tableau des paires clé/valeur du localStorage, puis on utilise une boucle for...of pour itérer sur ce tableau et afficher chaque sous-tableau comme un élément
for (const tab of Object.entries(localStorage)) {
    console.log(tab);
}

/*=============================================
=            Stocker des objets JavaScript            =
=============================================*/

const monObjet = {
    lang: "fr",
    country: "France",
    city: "Narbonne",
    age: 34,
};

// On convertit l'objet en chaîne de caractères au format JSON avec JSON.stringify(objet) et on stocke cette chaîne dans le localStorage sous la clé identity avec la notation pointée
localStorage.identity = JSON.stringify(monObjet);

// On récupère la chaîne de caractères au format JSON associée à la clé identity dans le localStorage et on la convertit en objet JavaScript avec JSON.parse(chaîne) et on l'affiche dans la console
console.log(JSON.parse(localStorage.identity));
