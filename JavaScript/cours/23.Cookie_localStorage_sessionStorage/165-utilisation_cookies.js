/*=============================================
=            Accéder aux cookies depuis le navigateur            =
=============================================*/

console.log(document.cookie);

/*=============================================
=            Créer un cookie            =
=============================================*/

// Pour créer un cookie, il suffit d'assigner une valeur à document.cookie sous la forme "nom=valeur"
// On peut aussi spécifier des attributs optionnels comme l'expiration, le domaine, le chemin, le secure ou le sameSite
// Par exemple, l'attribut sameSite permet de contrôler comment les cookies sont envoyés avec les requêtes cross-site
document.cookie = "name=Jean; sameSite=True";

/* -------------------------------------------------------------------------- */

// On peut aussi stocker des objets dans les cookies en les convertissant en chaînes JSON avec la méthode JSON.stringify
let testCookie = {
    pays: "France",
    ville: "Paris",
};

document.cookie = `location=${JSON.stringify(testCookie)}`;

/*=============================================
=            Parser les cookies            =
=============================================*/
// Pour extraire les cookies et les stocker dans un objet, on peut utiliser une fonction qui utilise les méthodes split et map
// La méthode split permet de diviser une chaîne en un tableau de sous-chaînes selon un séparateur
// La méthode map permet d'appliquer une fonction à chaque élément du tableau pour produire un nouveau tableau
// Pour plus d'informations, voir https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/split et https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/map

const extractCookies = () => {
    // On divise la chaîne document.cookie par le séparateur "; "
    // On obtient un tableau de chaînes de la forme "nom=valeur"
    return Object.fromEntries(
        document.cookie
            .split("; ")
            // On applique la fonction map sur chaque élément du tableau
            .map((currentValue) => {
                // On sépare le nom et la valeur du cookie par le signe égal, split() retourne un tableau
                const pair = currentValue.split("=");
                // On retourne un tableau [nom, valeur] pour chaque élément
                // Si la valeur commence par une accolade, on la parse en objet JSON avec la méthode JSON.parse
                // Sinon, on la conserve telle quelle
                // Pour plus d'informations, voir https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/JSON
                return [pair[0], pair[1][0] === "{" ? JSON.parse(pair[1]) : pair[1]];
                /* 
    [pair[0], pair[1][0] === "{" ? JSON.parse(pair[1]) : pair[1]] : c’est un tableau littéral, c’est-à-dire une façon de créer un tableau en utilisant des crochets et en séparant les éléments par des virgules. Ici, le tableau contient deux éléments :

    pair[0] : c’est le premier élément du tableau pair, qui contient le nom du cookie. On utilise l’opérateur d’accès aux propriétés [ ] pour accéder à un élément d’un tableau par son indice. L’indice commence à 0, donc pair[0] correspond au premier élément de pair.
    pair[1][0] === "{" ? JSON.parse(pair[1]) : pair[1] : c’est le deuxième élément du tableau, qui contient la valeur du cookie. On utilise l’opérateur conditionnel ? : pour renvoyer une valeur différente selon une condition. La syntaxe est condition ? valeurSiVrai : valeurSiFaux. Ici, la condition est pair[1][0] === "{", qui vérifie si le premier caractère de la valeur du cookie est une accolade ouvrante {. On utilise l’opérateur de comparaison strict === pour comparer les valeurs sans faire de conversion de type. Si la condition est vraie, on renvoie JSON.parse(pair[1]), qui est une méthode qui permet de convertir une chaîne JSON en un objet JavaScript. Si la condition est fausse, on renvoie simplement pair[1], qui est la valeur du cookie sans modification.

 */
            })
    ); // On utilise la méthode Object.fromEntries pour convertir le tableau de paires [nom, valeur] en un objet
};
console.log("🚀 ~ file: utilisation_cookies.js:25 ~ extractCookies ~ extractCookies:", extractCookies());
