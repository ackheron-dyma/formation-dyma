/*=============================================
=            Copier un objet            =
=============================================*/

const obj = { a: 1 };
const obj2 = { ...obj };

// 01 SHALLOW COPY, il est important de noter que les copies superficielles ne copient que les propriétés de premier niveau de l'objet, tandis que les copies en profondeur copient toutes les propriétés et sous-propriétés de l'objet.
const obj3 = { a: {} };
const shallowCopyObj3 = { ...obj3 };
shallowCopyObj3.a.test = 42;
console.log("🚀 ~ file: 45-copier_un_objet.js:7 ~ shallowCopyObj3:", shallowCopyObj3); // { a: { test: 42 }} On affecte la propriété test: 42 dans l'objet imbriqué de la propriété a.
console.log("🚀 ~ file: 45-copier_un_objet.js:6 ~ obj3:", obj3); // Copy superficielle car l'objet imbriqué de notre objet d'origine est aussi modifié

//02 DEEP COPY avec JSON.parse() sans aucune référence en commun avec l'objet source, la méthode JSON.parse() et JSON.stringify() peut être plus lente que les autres méthodes de copie, en particulier pour les objets complexes avec de nombreuses propriétés imbriquées. Cependant, elle est considérée comme la méthode la plus sûre pour effectuer une copie en profondeur.
const obj4 = { a: {} };
const deepCopyObj4 = JSON.parse(JSON.stringify(obj4));
deepCopyObj4.a.test = 56;
console.log("🚀 ~ file: 45-copier_un_objet.js:15 ~ deepCopyObj4:", deepCopyObj4); // { a: { test: 56 } } On affecte la propriété test: 56 à l'objet deepCopy
console.log("🚀 ~ file: 45-copier_un_objet.js:14 ~ obj4:", obj4); // { a: {} } Nous obtenons une copie profonde sans référence à l'objet imbriqué car l'objet d'origine n'est pas modifié

//03 DEEP COPY avec structuredClone(). En termes de performances, la méthode structuredClone() est souvent la plus rapide pour les objets complexes avec de nombreuses propriétés imbriquées, car elle utilise une approche similaire à JSON.parse() et JSON.stringify(), mais en interne elle utilise une méthode plus efficace pour effectuer la copie.
const obj5 = { a: "titi" };
const deepCopyObj5 = structuredClone(obj5);
deepCopyObj5.a = "toto";
console.log("🚀 ~ file: 45-copier_un_objet.js:26 ~ deepCopyObj5:", deepCopyObj5); // {a: toto} on change la valeur de la propriété 'a' de notre deepCopy
console.log("🚀 ~ file: 45-copier_un_objet.js:26 ~ obj5:", obj5); // {a: titi} la valeur de la propriété 'a' de l'objet d'origine n'est pas modifiée
