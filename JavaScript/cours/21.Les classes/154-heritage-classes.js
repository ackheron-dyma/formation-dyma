/*=============================================
=            L'héritage entre les classes            =
=============================================*/
/* Il est possible de faire hériter une classe d'une autre classe.
Dans ce cas on appelle la classe qui a hérité la classe fille et l'autre classe, la classe parente. */

class Voiture {
    rouler() {
        console.log("Je roule");
    }
}

// On déclare une classe Sportive qui hérite de la classe Voiture avec le mot-clé extends
// Le mot-clé extends établit une relation d'héritage entre deux classes, en définissant le [[ Prototype ]] du prototype de la classe fille comme étant le prototype de la classe parente.
class Sportive extends Voiture {
    faireLaCourse() {
        console.log("Je fais la course");
    }
}

// On crée une instance de la classe Sportive avec le mot-clé new
const monBolide = new Sportive();

// On appelle la méthode faireLaCourse() sur l'instance monBolide
console.log(monBolide.faireLaCourse()); // affiche "Je fais la course"

// On appelle la méthode rouler() héritée de la classe Voiture sur l'instance monBolide
console.log(monBolide.rouler()); // affiche "Je roule"

/* Si une propriété ou une méthode n’est pas trouvée dans le prototype de Sportive, le moteur JavaScript va chercher dans le prototype de Voiture, puis dans le prototype de Object, puis dans null (qui termine la chaîne). */

/*=============================================
=            Utiliser des propriétés de la classe parente            =
=============================================*/
/* Il existe un mot clé spécial permettant d'appeler des méthodes et le constructeur de la classe parente depuis la classe fille : super.
super.methodeDeLaClasseParente() permet d'invoquer une méthode parente.
super(param1, param2…) permet d'appeler le constructeur de la classe parente avec les arguments spécifiés. */

class NouvelleVoiture {
    constructor(sieges) {
        this.sieges = sieges;
    }

    rouler() {
        return "Je roule";
    }
}

class NouvelleSportive extends NouvelleVoiture {
    constructor(sieges, chevaux) {
        super(sieges);
        this.chevaux = chevaux;
    }

    faireLaCourse() {
        console.log(`${super.rouler()} Et je fais la course`);
    }
}

const theBolide = new NouvelleSportive(4, 960);
console.log("🚀 ~ file: 154-heritage-classes.js:57 ~ theBolide:", theBolide);
