// Définition des classes A et B
class A {}
class B {}

// Définition d'un mixin contenant deux méthodes
const mixin = {
    methode1() {
        console.log("Je suis la méthode numéro 1");
    },
    methode2() {
        console.log("Je suis la méthode numéro 2");
    },
};

// Utilisation de la méthode Object.assign pour assigner les méthodes du mixin aux prototypes des classes A et B
Object.assign(A.prototype, mixin);
Object.assign(B.prototype, mixin);

// Création d'une instance de la classe A
const instanceA = new A();
console.log("🚀 ~ file: 159-mixins.js:17 ~ instanceA:", instanceA);

// Création d'une instance de la classe B
const instanceB = new B();
console.log("🚀 ~ file: 159-mixins.js:19 ~ instanceB:", instanceB);
