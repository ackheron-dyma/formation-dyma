/*----------  instanceof permet de savoir un objet est une instance d'un constructeur donné  ----------*/

class UneClasse {}

const test = new UneClasse();
console.log(test instanceof UneClasse); //true

/*----------  Fonctionne aussi avec les instances des fonctions constructeur natives  ----------*/

const tableau = [1, 2];

console.log(tableau instanceof Array); //true

/*----------  L'opérateur regarde sur tout la chaîne des prototypes, donc nous avons aussi  ----------*/

console.log(test instanceof Object); //true
console.log(tableau instanceof Object); //true
