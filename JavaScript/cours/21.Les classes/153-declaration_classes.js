/*=============================================
=            Les classes            =
=============================================*/
/*  Les classes sont une syntaxe introduite avec ECMAScript 2015 pour créer des objets et manipuler l'héritage prototypal de façon plus simple. Elles sont des fonctions spéciales qui peuvent être définies par déclaration ou par expression. */

class UneClasse {
    // La méthode constructor est une méthode spéciale qui permet de créer et d'initialiser les objets créés avec une classe.
    constructor(prenom) {
        this.prenom = prenom;
    }

    // Ajout de méthodes dans le prototype de la classe qui servirons de [[ Prototype ]] aux objets qui instancieront cette classe
    direBonjour() {
        console.log("Bonjour");
    }
}
console.log("🚀 ~ file: 153-declaration_classes.js:6 ~ UneClasse:", UneClasse);

// On peut aussi définir des fonctions constructrices qui ont le même rôle que les classes, mais avec une syntaxe différente. Ici, on définit une fonction UnConstructeur qui prend en paramètre le prénom de l'objet et lui assigne comme propriété.
function UnConstructeur(prenom) {
    this.prenom = prenom;
}

// On peut ajouter des méthodes au prototype de la fonction constructrice avec la syntaxe UnConstructeur.prototype.nomDeLaMethode. Ici, on ajoute la même méthode direBonjour que précédemment.
UnConstructeur.prototype.direBonjour = function () {
    console.log("Bonjour");
};

const paul = new UneClasse("Paul");
const jean = new UnConstructeur("Jean");

console.log("🚀 ~ file: 153-declaration_classes.js:24 ~ paul:", paul);
console.log("🚀 ~ file: 153-declaration_classes.js:25 ~ jean:", jean);

/* La différence entre une classe et une fonction constructrice en JavaScript est principalement une question de syntaxe et de style. Les deux permettent de créer des objets et de gérer l’héritage prototypal, mais la syntaxe des classes est plus récente (introduite avec ECMAScript 2015) et plus proche des langages orientés objet comme C#. Les classes ont aussi quelques avantages par rapport aux fonctions constructrices, comme :

    Elles vérifient que le constructeur est appelé avec le mot-clé new, ce qui évite des erreurs potentielles.
    Elles permettent d’utiliser le mot-clé super pour appeler le constructeur ou les méthodes de la classe parente.
    Elles sont plus simples, surtout si on fait des sous-classes.
    Elles sont plus adaptées pour faire des sous-classes de types intégrés comme Error ou Array.

Cependant, les classes sont en fait des fonctions spéciales sous le capot, et elles utilisent le même mécanisme d’héritage prototypal que les fonctions constructrices. Donc, il n’y a pas de différence fondamentale entre les deux, mais plutôt une préférence personnelle ou une question de compatibilité avec les navigateurs. Si vous utilisez des outils comme Babel ou Webpack, vous pouvez transpiler le code des classes en code des fonctions constructrices pour assurer la compatibilité. Sinon, vous pouvez consulter le site caniuse.com pour vérifier le support des classes par les navigateurs. */

/*=============================================
=            Fonctionnement d'une classe            =
=============================================*/
// LA CLASSE CRÉÉE SOUS LE CAPOT UN FONCTION CONSTRUCTEUR

// l'équivalent de la classe Voiture ci après en fonction constructeur serait :
/* 
    function Voiture(marque, prix, portes) {
        this.marque = marque;
        this.prix = prix;
        this.portes = portes;
} 
    Voiture.prototype.toString = function() { return `${this.marque}, ${this.prix}, ${this.portes}` };
*/
class Voiture {
    // le corp de cette fonction constructeur est situé dans le le corp `constructor` de notre classe
    // Les paramètres de cette fonction constructeur sont les paramètres déclarés comme paramètres du `constructor` de notre classe (marque, prix, portes)
    constructor(marque, prix, portes) {
        this.marque = marque;
        this.prix = prix;
        this.portes = portes;
    }

    // Lest méthodes de notre classe sont placées  dans la propriété `prototype` de cette fonction
    toString() {
        return `${this.marque}, ${this.prix}, ${this.portes}`;
    }
}

// L'instanciation d'un nouvel objet avec new permet de définir son [[ Prototype ]] comme étant le prototype de Voiture, d'exécuter le corps de la fonction avec les paramètres et de définir this comme le nouvel objet instancié. Ici this de Voiture = audi3
const audi3 = new Voiture("Audi", 24000, "5");

/*=============================================
=            Différences avec un fonction constructeur: l'énumérabilité des méthodes            =
=============================================*/

// On déclare une classe UneAutreClasse avec le mot-clé class suivi du nom de la classe
class UneAutreClasse {
    constructor(nom) {
        this.nom = nom;
    }
    methode1() {
        console.log(1);
    }
    methode2() {
        console.log(2);
    }
}

// On déclare une fonction constructrice UneFonctionConstructeur avec le mot-clé function suivi du nom de la fonction. Elle prend en paramètre le nom de l'objet.
function UneFonctionConstructeur(nom) {
    this.nom = nom;
}
UneFonctionConstructeur.prototype.direBonjour = function () {
    console.log("bonjour !");
};

const classe = new UneAutreClasse("classe");
const fonction = new UneFonctionConstructeur("fonction");

// On utilise une boucle for...in pour parcourir les propriétés énumérables de l'objet classe. Les propriétés énumérables sont celles qui peuvent être listées par une boucle for...in ou par Object.keys().
for (const key in classe) {
    console.log(key); // Affiche la propriété `nom`, mais les méthodes `methode1()` et methode2()` ne sont pas parcourues et ne s'affiche pas
}

// // On utilise une boucle for...in pour parcourir les propriétés énumérables de l'objet fonction.
for (const key in fonction) {
    console.log(key); // affiche la propriété `nom` et la méthode direBonjour()
}

/* // La différence entre les deux objets est que les méthodes définies dans la classe UneAutreClasse ne sont pas énumérables, alors que celles définies dans le prototype de la fonction constructrice UneFonctionConstructeur le sont. Cela signifie que les méthodes de la classe sont ignorées par la boucle for...in ou par Object.keys(), alors que celles de la fonction constructrice sont listées. C'est un choix de conception fait par les concepteurs d'ECMAScript 2015 pour éviter d'exposer les méthodes internes des classes. Si on veut rendre les méthodes d'une classe énumérables, on peut utiliser Object.defineProperty() ou Object.defineProperties() pour changer leur attribut enumerable à true. */

/*=============================================
=            Plus de facilité d’utiliser des accesseurs            =
=============================================*/

// Avec les fonctions constructeurs, pour utiliser les accesseurs, il faut faire par exemple

function MaFonctionConstructeur(param) {
    this.param = param;
}
Object.defineProperties(MaFonctionConstructeur.prototype, {
    name: {
        get() {
            return this._param;
        },
        set(value) {
            this._param = value;
        },
    },
});

// Alors qu'en utilisant une classe on peut facilement faire :
class MaClasse {
    constructor(param) {
        this.param = param;
    }

    get param() {
        return this._param;
    }

    set param(value) {
        if (value.length < 3) {
            console.log("Trop court.");
            return;
        }
        this._param = value;
    }
}
