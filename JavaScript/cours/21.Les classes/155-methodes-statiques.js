/*=============================================
=            Les méthodes statiques            =
=============================================*/

class Personne {
    constructor(prenom, nom) {
        this.prenom = prenom;
        this.nom = nom;
    }
    //  Les méthodes statiques sont des méthodes qui appartiennent à la classe elle-même plutôt qu'à une instance spécifique de la classe. Dans ce cas, la méthode decrire() affiche simplement un message dans la console pour décrire la classe Personne.
    static decrire() {
        console.log("Personne est une classe pour créer des personnes");
    }
}

const dylan = new Personne("Bob", "Dylan");

console.log(Personne.decrire()); //  Personne est une classe pour créer des personnes.
// console.log(dylan.decrire()); // Error : dylan.decrire is not a function

/*=============================================
=            Héritage et méthodes statiques            =
=============================================*/
/* Les méthodes statiques sont également héritées sur la classe fille.
En fait, c'est parce que extends définit également le [[ Prototype ]] de la classe fille comme étant la classe parente. */

class Humain {
    constructor(prenom, nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    static decrire() {
        console.log("Humain est une classe pour créer des humains");
    }
}

class Homme extends Humain {
    constructor(prenom, nom) {
        super(prenom, nom);
        this.genre = "masculin";
    }
}
console.log("🚀 ~ file: 155-methodes-statiques.js:37 ~ Homme:", Homme);

const brian = new Homme("Brian", "May");
console.log("🚀 ~ file: 155-methodes-statiques.js:45 ~ brian:", brian);

console.log(Homme.decrire()); // Humain est une classe pour créer des humains
console.log(brian.decrire()); // Error : brian.decrire is not a function
