// L'exemple montre comment créer une instance de la classe Personne, accéder à la propriété privée prenom via le getter et utiliser le setter pour modifier sa valeur. */

class Personne {
    //Dans cet exemple, la classe Personne a une propriété privée #prenom, indiquée par le préfixe #. Cette notation récente (introduite dans ECMAScript 2022) permet de définir des propriétés privées accessibles uniquement à l'intérieur de la classe.
    #prenom; // Propriété privée

    constructor(prenom) {
        this.#prenom = prenom;
    }

    // Getter pour la propriété privée
    // Pour accéder à cette propriété privée, la classe définit un getter prenom(). Ce getter permet d'obtenir la valeur de la propriété #prenom.
    get prenom() {
        return this.#prenom;
    }

    // Setter pour la propriété privée
    // La classe utilise également un setter prenom(nouveauPrenom) pour modifier la valeur de #prenom. Le setter vérifie d'abord si la valeur fournie est une chaîne de caractères avant de mettre à jour la propriété. Si la valeur fournie n'est pas une chaîne de caractères, une erreur est affichée dans la console.
    set prenom(nouveauPrenom) {
        if (typeof nouveauPrenom === "string" && nouveauPrenom.length > 0) {
            this.#prenom = nouveauPrenom;
        } else {
            console.error("Le prénom doit être une chaîne de caractères.");
        }
    }

    // Méthode privée
    #methodePrivee() {
        console.log("Ceci est une méthode privée.");
    }

    // Méthode publique
    methodePublique() {
        console.log("Ceci est une méthode publique.");
        // Appel de la méthode privée à l'intérieur de la classe
        this.#methodePrivee();
    }
}

// Utilisation de la classe
const personne = new Personne("John");
console.log(personne.prenom); // Output: "John"

personne.prenom = "Jane";
console.log(personne.prenom); // Output: "Jane"

personne.prenom = 123; // Error: Le prénom doit être une chaîne de caractères.

console.log("🚀 ~ file: 156-propriétés-privées.js:50 ~ personne:", personne.methodePublique()); // Output: "Ceci est une méthode publique." "Ceci est une méthode privée."

// L'appel de personne.#methodePrivee() générera une erreur car les méthodes privées ne sont pas accessibles en dehors de la classe.
// console.log("🚀 ~ file: 156-propriétés-privées.js:53 ~ personne:", personne.#methodePrivee()); // Error: #methodePrivee is private
