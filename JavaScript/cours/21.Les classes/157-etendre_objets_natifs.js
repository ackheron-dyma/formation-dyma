// Définition d'une classe UniqueArray qui hérite de la classe native Array
class UniqueArray extends Array {
    aDesDoublons() {
        return this.some((el, index, tableau) => tableau.indexOf(el) !== index);
    }

    toUniqueArray() {
        return this.filter((el, index, tableau) => tableau.indexOf(el) === index);
    }
}

const test = new UniqueArray(1, 2, 2, 4);

console.log(test.aDesDoublons());
console.log(test.toUniqueArray());
