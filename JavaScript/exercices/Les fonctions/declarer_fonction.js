/* DÉCLARER UNE FONCTION

ÉNONCÉ
Déclarez une fonction pairOuImpair() qui retourne si le nombre reçu en argument est pair ou impair.

LISTE DES OBJECTIFS À REMPLIR
Déclarez une fonction pairOuImpair() qui reçoit un seul argument
La fonction doit retourner 'pair' ou 'impair' suivant le nombre reçu

EXEMPLE D'ENTRÉE
(2)
(54)
(-3)

EXEMPLE DE SORTIE
pair
pair
impair */

/* -------------------------------------------------------------------------- */

function pairOuImpair(param) {
    const resultat = param % 2 === 0 ? "pair" : "impair";

    return resultat;
}

console.log(pairOuImpair(2));
console.log(pairOuImpair(54));
console.log(pairOuImpair(-3));
