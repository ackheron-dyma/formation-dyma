/* FONCTIONS ET CLOSURES

ÉNONCÉ
Utilisez une fonction creerMultiplicateur() qui prend en argument un multiplicateur et qui va retourner une nouvelle fonction fléchée qui prend un nombre en argument et le multiplie par ce multiplicateur. C'est un exemple d'utilisation de closures JavaScript. Cette fonction permet ensuite de créer facilement d'autres fonctions : const doubler = creerMultiplicateur(2); const tripler = creerMultiplicateur(3); const quadrupler = creerMultiplicateur(4);

LISTE DES OBJECTIFS À REMPLIR
Exporter une fonction creerMultiplicateur() qui prend en argument un multiplicateur
La fonction creerMultiplicateur() doit retourner une nouvelle fonction qui prend en argument un nombre et le multiplie par le multiplicateur */

/* -------------------------------------------------------------------------- */

export function creerMultiplicateur(multiplicateur) {
    return (nombre) => nombre * multiplicateur;
}
