/* FONCTION FLÉCHÉE

Énoncé
Déclarez une constante carre contenant une fonction fléchée qui met au carré le nombre reçu.

LISTE DES OBJECTIFS À REMPLIR
Exportez une constante carre contenant une fonction fléchée
La fonction fléchée prend un argument et le retourne au carré

EXEMPLE D'ENTRÉE
(4)
(-2)

EXEMPLE DE SORTIE
16
4 */

/* -------------------------------------------------------------------------- */

const carre = (nombre) => {
    return nombre ** 2;
};

console.log(carre(4));
console.log(carre(-2));
