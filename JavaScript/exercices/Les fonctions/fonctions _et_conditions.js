/* FONCTIONS ET CONDITIONS

ÉNONCÉ
Déclarez une fonction lePlusGrand() qui prend trois arguments et retourne le plus grand.

LISTE DES OBJECTIFS À REMPLIR
Exportez une fonction lePlusGrand qui prend 3 arguments
Retournez le nombre le plus grand
N'utilisez pas Math.max(), l'objectif étant d'utiliser des conditions

EXEMPLE D'ENTRÉE
(1, 2, 3)
(42, 1, -10)

EXEMPLE DE SORTIE
3
42 */

/* -------------------------------------------------------------------------- */

function lePlusGrand(nombre1, nombre2, nombre3) {
    let plusGrand = nombre1; // Supposons que le nombre1 est le plus grand

    // Vérifions si le nombre2 est plus grand que le plusGrand actuel
    if (nombre2 > plusGrand) {
        plusGrand = nombre2;
    }

    // Vérifions si le nombre3 est plus grand que le plusGrand actuel
    if (nombre3 > plusGrand) {
        plusGrand = nombre3;
    }

    return plusGrand;
    // Une méthode plus simple et d'utiliser l'Objet Math avec sa méthode .max qui retour la valeur la plus grande passé en argument
    // return Math.max(nombre1, nombre2, nombre3)
}

console.log(lePlusGrand(1, 2, 3));
console.log(lePlusGrand(42, 1, -10));
