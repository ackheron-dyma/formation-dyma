function testFn(note) {
    let remarque;
    switch (note) {
        case 5:
            remarque = "Excellent";
            break;
        case 4:
            remarque = "Très bien";
            break;
        case 3:
            remarque = "Bien";
            break;
        case 2:
            remarque = "Moyen";
            break;
        // Regroupe les cas 1 et 0 dans le même bloc
        case 1:
        case 0:
            remarque = "Médiocre";
            break;
        // Si 'note' ne correspond à aucun des cas précédents, assigne la valeur "La note doit être un nombre compris entre 0 et 5" à 'remarque'
        default:
            remarque = "La note doit être un nombre compris entre 0 et 5";
            break;
    }
    return remarque;
}

console.log(testFn(4));
console.log(testFn(0));
console.log(testFn(-99));
console.log(testFn(200));
console.log(testFn(undefined));
