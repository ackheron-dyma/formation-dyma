/* ÉNONCÉ

Utilisez une boucle for et l'instruction continue pour concaténer des nombres dans la variable texte. Indices pour le dernier objectif : pensez à utiliser if / else et typeof pour savoir si le type de la variable nombre vaut 'number'.

Code de départ

export function testFn(nombre) {
  // Déclarez en dessous :
  let texte = '';

  return texte;
}

Liste des objectifs à remplir
Pour tout nombre reçu, la variable texte doit contenir la concaténation du nombre et des 4 suivants inclus sauf le 2ème. (Par exemple si nombre vaut 1, texte doit valoir : '1245'.)
Il faut également que si nombre n'est pas un nombre alors texte doit valoir 'Pas un nombre'

Exemple d'entrée
(50)
(-5)
(undefined)
('Dupont')

Exemple de sortie
'50515354'
'-5-4-2-1'
'Pas un nombre'
'Pas un nombre' */

function testFn(nombre) {
    let texte = "";

    // On vérifie si le type de nombre n'est pas égal à 'number' en utilisant typeof. Si ce n'est pas le cas, la valeur de texte est mise à jour avec la chaîne 'Pas un nombre'.
    if (typeof nombre !== "number") {
        texte = "Pas un nombre";
    } else {
        // Sinon, une boucle for est utilisée pour itérer à travers les nombres de nombre à nombre + 4, inclusivement.
        for (let i = nombre; i <= nombre + 4; i++) {
            // À chaque itération, on vérifie si i est égal à nombre + 2. Si c'est le cas, on utilise l'instruction continue pour passer à l'itération suivante sans exécuter le reste du code dans la boucle.
            if (i === nombre + 2) {
                continue;
            }
            // Sinon, on concatène la valeur de i à la variable texte en utilisant l'opérateur d'addition et d'affectation  (+=).
            texte += i;
        }
    }

    return texte;
}

console.log(testFn(-10));
