function testFn(condition, age) {
    // Déclare une constante 'a' avec la valeur 42
    const a = 42;
    // Déclare une constante 'b' avec la valeur de 'a' si 'condition' est vraie, sinon avec la valeur 0
    const b = condition ? a : 0;
    // Déclare une constante 'statut' avec la valeur "Majeur au USA" si 'age' est supérieur ou égal à 21,
    // sinon avec la valeur "Majeur en France" si 'age' est supérieur ou égal à 18,
    // sinon avec la valeur "Mineur"
    const statut = age >= 21 ? "Majeur aux USA" : age >= 18 ? "Majeur en France" : "Mineur";
    return [b, statut];
}

const test = testFn(0, 19);
console.log("🚀 ~ file: operateur_conditionnel.js:21 ~ test:", test);
