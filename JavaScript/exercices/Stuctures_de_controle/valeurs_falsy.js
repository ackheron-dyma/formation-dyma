// falsy1 est défini avec la valeur false, qui est une valeur booléenne falsy. Elle représente la fausseté ou la non-véracité.
export const falsy1 = false;
// falsy2 est défini avec la valeur undefined, qui est une valeur falsy utilisée pour représenter une variable non définie.
export const falsy2 = undefined;
// falsy3 est défini avec la valeur null, qui est une valeur falsy utilisée pour représenter l'absence intentionnelle de toute valeur d'objet.
export const falsy3 = null;
// falsy4 est défini avec la valeur 0, qui est une valeur numérique falsy lorsque utilisée dans un contexte booléen.
export const falsy4 = 0;
// falsy5 est défini avec la valeur "", qui est une chaîne de caractères vide. Les chaînes de caractères vides sont considérées comme des valeurs falsy.
export const falsy5 = "";
// falsy6 est défini avec la valeur NaN, qui signifie "Not a Number" (non un nombre). NaN est une valeur falsy qui indique un résultat invalide lors de calculs numériques.
export const falsy6 = NaN;
