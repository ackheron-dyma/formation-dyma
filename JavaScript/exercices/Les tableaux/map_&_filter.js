/* MAP ET FILTER

ÉNONCÉ
Créez une fonction filtreFemmesEtAjouteNomComplet() qui reçoit un tableau de personnes célèbres en argument. Les objets sont de la forme : { prenom: 'Albert', nom: 'Einstein', genre: 'masculin' } La fonction doit d'abord filtrer les personnes célèbres de genre féminin. Elle doit ensuite ajouter à chaque objet du tableau le nom complet à partir du prénom et du nom.

LISTE DES OBJECTIFS À REMPLIR
Créez et exportez une fonction filtreFemmesEtAjouteNomComplet() qui reçoit un tableau en argument
Filtrez les objets du tableau pour que seuls les éléments avec une clé genre ayant pour valeur "féminin" soient inclus
Ajoutez sur tous les objets du tableau une propriété nomComplet qui comporte le prénom suivi d'un espace puis le nom

EXEMPLE D'ENTRÉE
[
  { prenom: 'Albert', nom: 'Einstein', genre: 'masculin' },
  { prenom: 'Isaac', nom: 'Newton', genre: 'masculin' },
  { prenom: 'Marie', nom: 'Curie', genre: 'féminin' },
  { prenom: 'Max', nom: 'Planck', genre: 'masculin' },
  { prenom: 'Simone', nom: 'de Beauvoir', genre: 'féminin' },
  { prenom: 'Rosalind', nom: 'Franklin', genre: 'féminin' },
];

EXEMPLE DE SORTIE
[
  {
    prenom: 'Simone',
    nom: 'de Beauvoir',
    nomComplet: 'Simone de Beauvoir',
    genre: 'féminin',
  },
  {
    prenom: 'Rosalind',
    nom: 'Franklin',
    nomComplet: 'Rosalind Franklin',
    genre: 'féminin',
  },
  {
    prenom: 'Marie',
    nom: 'Curie',
    nomComplet: 'Marie Curie',
    genre: 'féminin',
  },
]; */

/* -------------------------------------------------------------------------- */

/**
 *
 * @param {array} personnes
 * @returns
 */
function filtreFemmesEtAjouteNomComplet(personnes) {
    // La méthode filter() est utilisée pour filtrer les femmes célèbres en vérifiant que la propriété genre a pour valeur 'féminin'. Le résultat est stocké dans la variable femmes.
    const femmes = personnes.filter((personne) => personne.genre === "féminin");

    /* La méthode map() est utilisée pour créer un nouveau tableau femmesAvecNomComplet en parcourant le tableau femmes et en ajoutant la propriété nomComplet à chaque objet en utilisant la syntaxe de décomposition (...femme) pour copier toutes les propriétés existantes et en ajoutant la nouvelle propriété nomComplet avec la valeur ${femme.prenom} ${femme.nom}. */
    const femmesAvecNomComplet = femmes.map((femme) => ({
        ...femme,
        nomComplet: `${femme.prenom} ${femme.nom}`,
    }));

    return femmesAvecNomComplet;
}

console.log(
    filtreFemmesEtAjouteNomComplet([
        { prenom: "Albert", nom: "Einstein", genre: "masculin" },
        { prenom: "Isaac", nom: "Newton", genre: "masculin" },
        { prenom: "Marie", nom: "Curie", genre: "féminin" },
        { prenom: "Max", nom: "Planck", genre: "masculin" },
        { prenom: "Simone", nom: "de Beauvoir", genre: "féminin" },
        { prenom: "Rosalind", nom: "Franklin", genre: "féminin" },
    ])
);
