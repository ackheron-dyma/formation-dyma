/* REDUCE

ÉNONCÉ
Créez une fonction compterElements() qui reçoit un tableau en argument. La fonction doit retourner un objet qui comporte en clés les chaînes de caractères du tableau et en valeur le nombre d’occurrences dans le tableau.

LISTE DES OBJECTIFS À REMPLIR
Créez et exportez une fonction compterElements() qui reçoit un tableau en argument
La fonction doit retourner un objet qui comporte une clé unique pour chaque élément du tableau et en valeur le nombre de fois que cet élément est contenu dans le tableau

EXEMPLE D'ENTRÉE
[
  'veste',
  'clé',
  'verre',
  'chien',
  'biberon',
  'chien',
  'veste',
  'clé',
  'chien',
]

EXEMPLE DE SORTIE
{
  veste: 2,
  clé: 2,
  verre: 1,
  chien: 3,
  biberon: 1,
} */

/* -------------------------------------------------------------------------- */

/**
 *
 * @param {array} tableau
 * @returns
 */
function compterElements(tableau) {
    // Utiliser reduce pour parcourir le tableau et créer un objet qui compte les occurrences
    return tableau.reduce((acc, valeurCourante) => {
        // Si l'élément courant existe déjà comme clé dans l'accumulateur, incrémenter sa valeur
        if (acc[valeurCourante]) {
            acc[valeurCourante]++;
        } else {
            // Sinon, initialiser sa valeur à 1
            acc[valeurCourante] = 1;
        }
        // Retourner l'accumulateur mis à jour
        return acc;
    }, {}); // Spécifier un objet vide comme valeur initiale de l'accumulateur
}

/* NOTES SUR LA NOTATION acc[valeurCourante]

Vous ne pouvez pas utiliser la notation acc.valeurCourante au lieu de acc[valeurCourante] parce que valeurCourante est une variable qui contient le nom de la clé de l’objet acc. La notation acc.valeurCourante signifie que vous voulez accéder à la propriété nommée “element” de l’objet acc, qui n’existe pas. La notation acc[valeurCourante] signifie que vous voulez accéder à la propriété dont le nom est la valeur de la variable valeurCourante de l’objet acc, ce qui correspond à ce que vous voulez faire.

Par exemple, si valeurCourante vaut “veste”, alors acc[valeurCourante] est équivalent à acc[“veste”], qui est la valeur associée à la clé “veste” dans l’objet acc. En revanche, acc.valeurCourante est équivalent à acc[“valeurCourante”], qui est undefined car il n’y a pas de clé “element” dans l’objet acc.

La notation entre crochets vous permet de dynamiser le nom de la propriété que vous voulez accéder ou modifier dans un objet. La notation avec le point vous oblige à utiliser un nom fixe de propriété.

Exemple de la notation:
/ -------------------------------------------------------------------------- /
const user = {
  age: 25,
  name: 'Jack',
}

let key = 'age';

console.log(user[key]);  // 25

key = 'name';

console.log(user[key]);  // Jack

/ -------------------------------------------------------------------------- /

  */

const entries = ["veste", "clé", "verre", "chien", "biberon", "chien", "veste", "clé", "chien"];
console.log("🚀 ~ file: reduce.js:37 ~ entries:", compterElements(entries));
