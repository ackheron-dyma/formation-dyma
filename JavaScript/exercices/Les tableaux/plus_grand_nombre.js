/* LE PLUS GRAND NOMBRE

ÉNONCÉ

Créez une fonction qui prend un nombre indéfini d'arguments et retourne le plus grand nombre. Utilisez l'opérateur rest (...) et une boucle pour itérer sur les arguments. N'utilisez pas Math.max().

LISTE DES OBJECTIFS À REMPLIR
Créez et exportez une fonction plusGrandNombre() qui prend un nombre indéfini d'arguments
Utilisez une structure de contrôle et une boucle pour retourner le plus grand nombre

EXEMPLE D'ENTRÉE
(1, 2, 3, 4, 5, 7)
(120, 1, 140)

EXEMPLE DE SORTIE
7
140 */

/* -------------------------------------------------------------------------- */

/* l’opérateur rest retourne un tableau qui contient tous les arguments qui ne sont pas assignés à un paramètre nommé. Par exemple, dans la fonction plusGrandNombre(…nombres), le paramètre rest …nombres est un tableau qui contient tous les arguments passés à la fonction */
function plusGrandNombre(...nombres) {
    // initialisez le max avec le premier élément du tableau
    let max = nombres[0];

    // parcourez le reste du tableau
    for (let i = 0; i < nombres.length; i++) {
        // comparez chaque élément avec le max actuel
        if (nombres[i] > max) {
            // mettez à jour le max si nécessaire
            max = nombres[i];
        }
    }
    return max;
}

console.log(plusGrandNombre(1, 2, 3, 4, 5, 6, 7));
console.log(plusGrandNombre(120, 1, 140));
