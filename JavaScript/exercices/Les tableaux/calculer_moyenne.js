/* CALCULER LA MOYENNE

ÉNONCÉ
Créez une fonction obtenirMoyenne() qui reçoit un tableau de tableaux en argument. Dans chaque tableau se trouve un certain nombre de notes sur 20. La fonction doit retourner la moyenne des notes arrondie à une décimale après la virgule.

LISTE DES OBJECTIFS À REMPLIR
Créez et exportez une fonction obtenirMoyenne qui reçoit un tableau de tableaux en argument
Utilisez les méthodes map() et reduce() pour calculer la moyenne de chaque tableau
Utilisez la méthode Math.round() pour arrondir la moyenne à une décimale après la virgule

EXEMPLE D'ENTRÉE
([
  [12, 14, 16, 10, 5],
  [7, 2, 4],
]),

EXEMPLE DE SORTIE
[11.4, 4.3] */

/* -------------------------------------------------------------------------- */

/**
 *
 * @param {array} tableaux
 */
function obtenirMoyenne(tableaux) {
    // On utilise la méthode map() pour appliquer une fonction à chaque élément du tableau tableaux
    // La fonction prend en argument un tableau de notes et retourne la moyenne de ces notes
    const moyennes = tableaux.map((tableau) => {
        // On utilise la méthode reduce() pour calculer la somme des notes du tableau
        // La fonction de réduction prend en argument l'accumulateur (acc) et la valeur courante (currentValue)
        // Elle retourne la nouvelle valeur de l'accumulateur après avoir ajouté la valeur courante
        // On initialise l'accumulateur à 0

        let somme = tableau.reduce((acc, currentValue) => {
            return acc + currentValue;
        }, 0);

        // On divise la somme par le nombre de notes pour obtenir la moyenne
        let moyenne = somme / tableau.length;

        // On arrondit la moyenne à une décimale après la virgule en utilisant la méthode Math.round()
        // On multiplie la moyenne par 10, on arrondit le résultat, puis on divise par 10
        return Math.round(moyenne * 10) / 10;
    });

    // On retourne le tableau de moyennes
    return moyennes;
}

const monTab = [
    [12, 14, 16, 10, 5],
    [7, 2, 4],
];
console.log("🚀 ~ file: calculer_moyenne.js:28 ~ monTab:", obtenirMoyenne(monTab));
