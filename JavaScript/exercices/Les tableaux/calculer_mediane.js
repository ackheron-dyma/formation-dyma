/* CALCULER LA MÉDIANE

ÉNONCÉ
Ecrivez une fonction obtenirMediane() qui reçoit un tableau de tableaux de notes. La fonction doit, pour chaque tableau, retourner la médiane du tableau de notes. La médiane est la valeur qui sépare la moitié inférieure de la moitié supérieure d'un ensemble. Pour calculer la médiane, il faut d’abord trier les données (les trier dans l’ordre ascendant). Si le nombre d'éléments est impair, la médiane est le nombre qui se situe au milieu. Si le nombre d'éléments est pair, la médiane est la moyenne des deux valeurs du milieu.

LISTE DES OBJECTIFS À REMPLIR
Créez et exportez une fonction obtenirMediane() qui reçoit un tableau de tableaux en argument
Itérez sur chaque tableau en utilisant la méthode map()
Trouvez l'index de l'élément au milieu du tableau. Si le nombre d'élément est pair trouvez la position de la première des deux valeurs du milieu
Copiez et triez le tableau des notes avec les méthodes slice() et sort()
Utilisez l'opérateur % pour déterminer si le nombre d'éléments dans le tableau est pair ou impair
Si le nombre d'élément est impair, retournez la valeur du milieu du tableau
Si le nombre d'élément est pair, retournez la moyenne des deux valeurs du milieu du tableau

EXEMPLE D'ENTRÉE
([[2, 14, 16, 15, 5], [7, 2, 4]])
([12, 14, 12, 13], [2, 14, 1, 5]])

EXEMPLE DE SORTIE
[14, 4]
[12.5, 3.5] */

/* -------------------------------------------------------------------------- */
function obtenirMediane(tableauxNotes) {
    // La méthode map() est utilisée pour itérer sur chaque tableau de tableauxNotes.
    return tableauxNotes.map((tableau) => {
        // Pour chaque tableau, la méthode sort() est utilisée pour trier les éléments par ordre croissant en utilisant la fonction de comparaison (firstEl, secondEl) => firstEl - secondEl.
        const triage = tableau.sort((firstEl, secondEl) => firstEl - secondEl);

        /* Ensuite, la variable milieu est définie en calculant l'indice médian du tableau trié en utilisant Math.floor(triage.length / 2)
         On utilise Math.floor() pour arrondir un nombre à l’entier inférieur le plus proche. Par exemple, Math.floor(3.7) renvoie 3 et Math.floor(-2.4) renvoie -3. Cela peut être utile pour calculer l’indice du milieu d’un tableau, car il faut un nombre entier pour accéder aux éléments d’un tableau. Par exemple, si le tableau a une longueur de 5, l’indice du milieu sera Math.floor(5 / 2) qui vaut 2.
         */
        const milieu = Math.floor(triage.length / 2);

        // Si la longueur du tableau est paire
        if (triage.length % 2 === 0) {
            /* la méthode slice() est utilisée pour extraire les deux valeurs du milieu en utilisant milieu - 1 comme indice de début et milieu + 1 comme indice de fin. Ensuite, la méthode reduce() est utilisée pour calculer la moyenne des deux valeurs. */
            return triage.slice(milieu - 1, milieu + 1).reduce((acc, currentValue) => acc + currentValue / 2, 0);
        } else {
            // Si la longueur du tableau trié est impaire, alors la valeur du milieu (triage[milieu]) est directement retournée.
            return triage[milieu];
        }
    });
}

const entree = [
    [2, 14, 16, 15, 5],
    [7, 2, 4],
];
const entree2 = [
    [12, 14, 12, 13],
    [2, 14, 1, 5],
];
console.log("🚀 ~ file: calculer_mediane.js:33 ~ entree:", obtenirMediane(entree));
console.log("🚀 ~ file: calculer_mediane.js:33 ~ entree:", obtenirMediane(entree2));
