/* TRIER UN TABLEAU D'OBJETS

ÉNONCÉ

Créez une fonction trierTableau() qui reçoit en argument un tableau d'objets et le retourne trié selon deux clés. Le tableau doit être trié d'abord par prix et ensuite par quantité. Cela signifie que si deux objets ont la même valeur pour la propriété prix, ils sont ensuite triés par quantité.

LISTE DES OBJECTIFS À REMPLIR
Créez une fonction trierTableaux() qui reçoit un tableau d'objets en argument
La fonction doit retourner le tableau trié par prix de manière décroissante
La fonction doit trier les objets par quantité de manière décroissante si plusieurs objets ont le même prix

EXEMPLE D'ENTRÉE
({ prix: 20, quantite: 10 }, { prix: 20, quantite: 2 }, { prix: 50, quantite: 1 })

EXEMPLE DE SORTIE
({ prix: 50 quantite: 1 }, { prix: 20, quantite: 10 }, { prix: 20, quantite: 2 }) */

/* -------------------------------------------------------------------------- */

function trierTableau(...tableau) {
    return tableau.sort((firstEl, secondEl) => {
        /*        // Compare le prix des deux éléments
        // Si le PRIX du premier élément est supérieur au prix du deuxième élément, retourne un nombre négatif
        // Si le PRIX du premier élément est inférieur au prix du deuxième élément, retourne un nombre positif
        // Si le PRIX des deux éléments est égal, compare la quantité des deux éléments

        // Si la QUANTITÉ du premier élément est inférieure à la quantité du deuxième élément, retourne un nombre négatif
        // Si la QUANTITÉ du premier élément est supérieure à la quantité du deuxième élément, retourne un nombre positif
        // Si la QUANTITÉ des deux éléments est égale, retourne zéro */

        // retourne les tableau trier dans un ordre décroissant car (secondEl - firstEl) si on veut trier dans un ordre croisant il faut faire l'inverse (firstEl - secondEl)
        return secondEl.prix - firstEl.prix ? secondEl.prix - firstEl.prix : secondEl.quantite - firstEl.quantite;
    });
}

console.log(trierTableau({ prix: 20, quantite: 10 }, { prix: 20, quantite: 2 }, { prix: 50, quantite: 1 }));
