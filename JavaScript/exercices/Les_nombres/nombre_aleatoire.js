function testFn() {
    // Math.floor() arrondit un nombre à l'entier inférieur. En multipliant Math.random() par 11, nous obtenons un nombre aléatoire entre 0 et 10.9999. Ensuite, Math.floor() arrondit ce nombre à l'entier inférieur, ce qui donne un nombre aléatoire entre 0 et 10.
    let aleatoire = Math.floor(Math.random() * 11);
    return aleatoire;
}

const a = testFn();
console.log("🚀 ~ file: nombre_aleatoire.js:7 ~ a:", a);
