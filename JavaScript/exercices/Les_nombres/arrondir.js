/* ÉNONCÉ

Vous devez utiliser plusieurs fonctions et méthodes relatives aux nombres pour arrondir un nombre à virgule flottante. Vous allez devoir arrondir le nombre reçu, uniquement s'il est à virgule flottante et retourner un nombre. Indice pour le dernier objectif, +var ou Number(var) permettent de convertir la variable en nombre

Code de départ

export function testFn(nombre) {
  // Déclarez en dessous :

  // Ne touchez pas au return :
  return nombre;
}

Liste des objectifs à remplir
Dans une structure de contrôle (if), vérifiez avec typeof et isNan() que nombre est bien un nombre
Si nombre n'est pas un nombre, assignez lui 'Pas un nombre'
Si nombre est un entier, ne modifiez pas le nombre
Si nombre est un nombre à virgule flottante arrondissez le à 3 chiffres après la virgule et convertissez le en nombre

Exemple d'entrée
(5)
(5.555)
(3.333333333333)
(9.9999999999)
(undefined)
(null)
('Bob')

Exemple de sortie
5
5.555
3.333
10
'Pas un nombre'
'Pas un nombre'
'Pas un nombre' */

function testFn(nombre) {
    // Vérifie si 'nombre' n'est pas de type nombre ou s'il est NaN
    if (typeof nombre !== "number" || isNaN(nombre)) {
        // Assigne la valeur 'Pas un nombre' à la variable 'nombre'
        nombre = "Pas un nombre";
        // Vérifie si 'nombre' n'est pas un entier
    } else if (!Number.isInteger(nombre)) {
        // Convertit 'nombre' en nombre et arrondit à 3 chiffres après la virgule
        nombre = Number(nombre.toFixed(3));
    }

    return nombre;
}

let a = testFn(5);
console.log("🚀 ~ file: arrondir.js:15 ~ a:", a);
let b = testFn(5.555);
console.log("🚀 ~ file: arrondir.js:17 ~ b:", b);
let c = testFn(3.3333333);
console.log("🚀 ~ file: arrondir.js:19 ~ c:", c);
let d = testFn(9.9999999);
console.log("🚀 ~ file: arrondir.js:21 ~ d:", d);
let e = testFn(undefined);
console.log("🚀 ~ file: arrondir.js:23 ~ e:", e);
let f = testFn(null);
console.log("🚀 ~ file: arrondir.js:25 ~ f:", f);
let g = testFn("bob");
console.log("🚀 ~ file: arrondir.js:27 ~ g:", g);
