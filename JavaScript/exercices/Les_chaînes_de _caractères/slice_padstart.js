/* Méthodes slice() et padStart()

ÉNONCÉ
Quelle que soit la longueur de la carte bancaire reçue, remplacez tous les caractères par des astérisques sauf les 4 derniers.

CODE DE DÉPART

export function testFn(cb) {
  // Déclarez en dessous :
  let reponse = ...

  // Ne touchez pas au return :
  return reponse;
}

LISTE DES OBJECTIFS À REMPLIR
Utilisez la méthode slice() pour ne conserver que les 4 derniers caractères de la carte
Utilisez la méthode padStart() pour masquer tous les autres caractères de la carte, quelle que soit la longueur de la carte

EXEMPLE D'ENTRÉE
('30116613634425')
('379517272367653')
('5223112447929699')

EXEMPLE DE SORTIE
'**********4425'
'***********7653'
'************9699' */

/* -------------------------------------------------------------------------- */

/**
 *
 * @param {string} cb
 * @returns
 */
function testFn(cb) {
    // La méthode slice(-4) renvoie une sous-chaîne de cb qui contient les quatre derniers caractères. La méthode padStart(cb.length, “*”) ajoute des astérisques au début de la sous-chaîne jusqu’à ce que sa longueur soit égale à celle de cb.
    let reponse = cb.slice(-4).padStart(cb.length, "*");

    return reponse;
}

console.log(testFn("379517272367653"));
