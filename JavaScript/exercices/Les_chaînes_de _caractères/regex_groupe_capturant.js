/* EXPRESSION RÉGULIÈRE ET GROUPE CAPTURANT
ÉNONCÉ
L'objectif est d'extraire l'extension d'un fichier

CODE DE DÉPART

export function testFn(fichier) {
  // Déclarez en dessous :
  let reponse = ...

  // Ne touchez pas au return :
  return reponse;
}

LISTE DES OBJECTIFS À REMPLIR

En utilisant une expression régulière avec un groupe capturant, récupérez uniquement l'extension du fichier
L'extension peut fait un nombre de caractères de 1 à n (pas de maximum)

EXEMPLE D'ENTRÉE
('MON-fichier123.png')
('MON-fichier123.doc')
('fichier.jpeg')

EXEMPLE DE SORTIE
png
doc
jpeg */

/* -------------------------------------------------------------------------- */

/**
 *
 * @param {string} fichier
 * @returns
 */
function testFn(fichier) {
    /* En utilisant l'expression régulière /\.(\w+)$/, on recherche le motif correspondant à un point suivi d'un ou plusieurs caractères alphanumériques à la fin de la chaîne, qui représente généralement l'extension du fichier. Le groupe capturant (\w+) permet de capturer les caractères de l'extension. */

    /* La méthode match est utilisée pour appliquer l'expression régulière à la chaîne fichier et renvoyer un tableau contenant les correspondances. L'élément [1] du tableau est utilisé pour accéder au groupe capturé, c'est-à-dire à l'extension du fichier. */
    let reponse = fichier.match(/\.(\w+)$/)[1];

    return reponse;
}

console.log(testFn("MON-fichier123.png"));
console.log(testFn("MON-fichier123.doc"));
console.log(testFn("fichier.jpegsuperplus"));
