/* Méthodes toUpperCase() et toLowerCase()
Énoncé
Votre objectif est de mettre le mot reçu en Title Case. A savoir, que seule la première lettre d'un mot doit être en majuscule.

Code de départ

export function testFn(chaine) {
  // Déclarez en dessous :

  // Ne touchez pas au return :
  return chaine;
}

Liste des objectifs à remplir
Modifiez le mot reçu dans chaine pour mettre la première lettre en majuscule
Modifiez le mot reçu dans chaine pour mettre toutes les autres lettres en minuscule

Exemple d'entrée
('chat')
('TEST')
('HeLLo')

Exemple de sortie
Chat
Test
Hello */

/*----------  ********************************************************************************  ----------*/

export function testFn(chaine) {
    if (typeof chaine === "string") {
        /*  On utilise la méthode charAt(0) pour extraire le premier caractère de la chaîne.
            On utilise ensuite la méthode toUpperCase() pour le convertir en majuscule.
            On utilise la méthode slice(1) pour récupérer le reste de la chaîne à partir de l'indice 1 (en excluant le premier caractère).
            On utilise la méthode toLowerCase() pour convertir toutes les autres lettres en minuscule.
            Enfin, les deux parties sont concaténées pour obtenir le résultat final. */
        const resultat = chaine.charAt(0).toUpperCase() + chaine.slice(1).toLowerCase();
        //  Ensuite, nous mettons à jour la valeur de chaine avec le résultat en utilisant l'assignation chaine = resultat.
        chaine = resultat;
    } else {
        return console.log("Pas une chaîne de caractère");
    }
    return chaine;
}
