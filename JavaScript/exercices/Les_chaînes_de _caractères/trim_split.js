/* Méthodes trim() et split()
Énoncé
Comptez le nombre de mots dans la phrase reçue.

Code de départ

export function testFn(phrase) {
  // Déclarez en dessous :
  let reponse = ...

  // Ne touchez pas au return :
  return reponse;
}

Liste des objectifs à remplir
Utilisez la méthode split() pour compter le nombre de mots dans la phrase
N'oubliez pas d'enlever les espaces au début ou à la fin de la phrase

Exemple d'entrée
('Un petit chat')
('  Une phrase avec des espaces au début')

Exemple de sortie
3
7 */

/*----------  **************************************************************************  ----------*/

/**
 *
 * @param {string} phrase phrase pass" en paramètre
 * @returns
 */
function testFn(phrase) {
    // La phrase est d'abord traitée avec la méthode trim() pour supprimer les espaces en début et fin de chaîne.
    // Ensuite, la méthode split(" ") est utilisée pour diviser la phrase en un tableau de mots en utilisant l'espace comme séparateur.
    // La propriété length est utilisée pour obtenir le nombre d'éléments dans le tableau, c'est-à-dire le nombre de mots dans la phrase.

    let reponse = phrase.trim().split(" ").length;
    return reponse;
}

console.log(testFn("  Une phrase avec des espaces au début      "));
