export function testFn(prop, valeur) {
    // Déclarez en dessous :
    const objet = {
        [prop]: valeur,
    };
    console.log("🚀 ~ file: objets_propriété_calculée.js:6 ~ testFn ~ objet:", objet);

    // Ne touchez pas au return :
    return objet;
}
