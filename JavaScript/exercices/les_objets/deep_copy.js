/* COPIE PROFONDE D'UN OBJET

ÉNONCÉ

Dans cet exercice vous devez copier en profondeur un objet dans un nouvel objet. Attention ! Ne réalisez pas de copie superficielle : si l'objet contient des propriétés non primitives, elles seraient copiées par référence.

CODE DE DÉPART

export function testFn(objet) {
  // Déclarez en dessous :
  const copie = ...

  // Ne touchez pas au return :
  return copie;
}

LISTE DES OBJECTIFS À REMPLIR
Copiez en profondeur l'objet reçu dans une variable copie
Attention à réaliser une copie profonde et non une copie superficielle (copie des références et non des valeurs)

EXEMPLE D'ENTRÉE
({ a: 1, b: 42 })

EXEMPLE DE SORTIE
{ a: 1, b: 42 } */

/* -------------------------------------------------------------------------- */

function testFn(objet) {
    // Utilisation de JSON.stringify pour convertir l'objet en une chaîne JSON
    // Utilisation de JSON.parse pour convertir la chaîne JSON en un nouvel objet
    // Cela crée une copie profonde de l'objet
    const copie = JSON.parse(JSON.stringify(objet));

    /* Utilisation et solution plus moderne avec `structuredClone() */
    // const copie = structuredClone(objet);
    return copie;
}

console.log(testFn({ a: 1, b: 2 }));
