/* SPREAD ET OBJECT.ASSIGN()
ÉNONCÉ

Dans cet exercice il faut utiliser l'opérateur spread ou la méthode Object.assign() pour fusionner 3 objets.

CODE DE DÉPART

export function testFn(objet1, objet2, objet3) {
  // Déclarez en dessous :
  const objet =

  // Ne touchez pas au return :
  return objet;
}

LISTE DES OBJECTIFS À REMPLIR
Utilisez l'opérateur spread ou la méthode Object.assign pour fusionner les trois objets dans un nouvel objet
Attention ! Si des objets ont une propriété en commun, l'ordre de fusion importe

EXEMPLE D'ENTRÉE
({ a: -2 }, { a: 2 }, { b: 42 })
({ a: 1 }, { b: 2 }, { c: 3 })

EXEMPLE DE SORTIE
{ a: 2, b: 42 }
{ a: 1, b: 2, c: 3 } */

/* -------------------------------------------------------------------------- */

function testFn(objet1, objet2, objet3) {
    /* Ce code utilise la syntaxe de la copie des objets (...) ou spread operator pour fusionner les propriétés de plusieurs objets en un seul objet. Cela permet de combiner les propriétés des trois objets objet1, objet2 et objet3 en un seul objet objet. Les propriétés ayant le même nom sont écrasées par les dernières valeurs rencontrées. */
    const objet = { ...objet1, ...objet2, ...objet3 };
    return objet;
}

console.log(testFn({ a: -2 }, { a: 2 }, { b: 42 }));
console.log(testFn({ a: 1 }, { b: 2 }, { c: 3 }));
