/* AFFECTATION PAR DÉCOMPOSITION

ÉNONCÉ
Vous recevrez des objets de la forme indiquée plus bas. L'objectif est d'utiliser l'affectation par décomposition pour obtenir un objet littéral contenant {prenom, nom}. Il faut donc renommer les propriétés lors de la décomposition.

CODE DE DÉPART

export function testFn(personne) {
  // Déclarez en dessous :

  // Ne touchez pas au return :
  return { prenom, nom };
}

LISTE DES OBJECTIFS À REMPLIR
Utilisez l'affectation par décomposition sur l'objet personne pour obtenir un nouvel objet littéral { prenom, nom }
N'oubliez pas de renommer les propriétés firstname et lastname lors de la décomposition

EXEMPLE D'ENTRÉE
({
  familly: {
    father: {
      firstname: 'Jean',
      lastname: 'Valjean',
    },
  },
},)

EXEMPLE DE SORTIE
{ prenom: 'Jean', nom: 'Valjean' } */

/* -------------------------------------------------------------------------- */

function testFn(personne) {
    console.log(personne);
    // La déstructuration est utilisée pour extraire les propriétés firstname et lastname de l'objet personne.familly.father. Les propriétés sont renommées en prenom et nom.
    const { firstname: prenom, lastname: nom } = personne.familly.father;
    // Un nouvel objet est créé avec les propriétés renommées prenom et nom.
    return { prenom, nom };
}

const test = testFn({
    familly: {
        father: {
            firstname: "John",
            lastname: "Bishop",
        },
    },
});
console.log("🚀 ~ file: affectation_decomposition.js:50 ~ test:", test);
