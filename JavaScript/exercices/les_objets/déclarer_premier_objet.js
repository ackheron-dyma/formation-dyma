export function testFn() {
    // Déclarez en dessous :
    const personne = {
        prenom: "Jean",
        nom: "Dupont",
        adresse: "12 rue du château - Montpellier",
        age: 30,
    };

    // Ne touchez pas au return :
    return personne;
}
