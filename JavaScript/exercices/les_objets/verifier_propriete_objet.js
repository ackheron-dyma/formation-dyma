/* VÉRIFIER QU'UNE PROPRIÉTÉ EXISTE SUR UN OBJET

ÉNONCÉ
Utilisez les opérateurs in et typeof pour vérifier l'existence et le type de propriétés d'un objet

CODE DE DÉPART

export function testFn(objet) {
  // Déclarez en dessous :
  const resultat = ...

  // Ne touchez pas au return :
  return resultat;
}

LISTE DES OBJECTIFS À REMPLIR
Utilisez l'opérateur in pour vérifier que l'objet a bien les propriétés prenom et nom
Utilisez l'opérateur typeof pour vérifier que les propriétés prenom et nom sont des chaînes de caractères

EXEMPLE D'ENTRÉE
{ prenom: 42, nom: 'Dupont' }
{ prenom: 'Jean', nom: 'Bob' }
{ prenom: 'Jean', age: 72 }

EXEMPLE DE SORTIE
false
true
false */

/* -------------------------------------------------------------------------- */

function testFn(objet) {
    /* L'expression vérifie les conditions suivantes :
    Si la propriété "prenom" est présente dans l'objet ("prenom" in objet).
    Si la propriété "nom" est présente dans l'objet ("nom" in objet).
    Si la valeur de la propriété "prenom" est de type "string" (typeof objet.prenom === "string").
    Si la valeur de la propriété "nom" est de type "string" (typeof objet.nom === "string").
    Le résultat de cette expression est un booléen qui indique si toutes les conditions sont satisfaites. */
    const resultat = "prenom" in objet && "nom" in objet && typeof objet.prenom === "string" && typeof objet.nom === "string";
    // Le résultat de cette expression est un booléen qui indique si toutes les conditions sont satisfaites.
    return resultat;
}

console.log(testFn({ prenom: 42, nom: "Dupont" }));
console.log(testFn({ prenom: "Jean", nom: "Bob" }));
console.log(testFn({ prenom: "Jean", nom: 72 }));
