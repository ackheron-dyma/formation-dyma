fn main() {
    let mut x: i32 = 25;
    let z: f64 = 3495.46;
    let t: &str = "Une ligne de texte";

    println!("Ma variable x contient: {}", x);
    println!("Ma variable z contient: {}", z);
    println!("Ma variable t contient: {}", t);

    x = 50;
    println!("Ma variable x contient: {}", x);
}
