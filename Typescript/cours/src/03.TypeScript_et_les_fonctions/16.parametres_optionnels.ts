/*=============================================
=            Les paramètres obligatoires par défaut            =
=============================================*/

const ajouter = (x: number, y: number): number => {
    return x + y;
};

// ajouter(2) // FAUX: Expected 2 arguments but got 2

// ajouter(1, 2, 3) // FAUX: Expected 2 arguments, but got 3

ajouter(8, 34); // JUSTE: Le nombre d'arguments de l'appel de la fonction correspond aux nombres de paramètres de la fonction

/* -------------------------------------------------------------------------- */

/*=============================================
=            Définir des paramètres optionnels            =
=============================================*/

// La fonction 'ajouter2' est définie avec deux paramètres obligatoires et un troisième paramètre optionnel.
// Il suffit d'ajouter un ? après chaque paramètre optionnel, ATTENTION les paramètres optionnels doivent être toujours ajoutés après les paramètres obligatoires

const ajouter2 = (x: number, y: number, z?: number): number => {
    // Si 'z' est fourni (c'est-à-dire qu'il n'est pas 'undefined'), on renvoie la somme de x, y et z.
    // Sinon, on renvoie simplement la somme de x et y.
    return z ? x + y + z : x + y;
};

ajouter2(27, 47); // JUSTE: Car le troisième paramètre de la fonction est optionnel "z?";
