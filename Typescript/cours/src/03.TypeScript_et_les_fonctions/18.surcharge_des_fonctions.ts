/*=============================================
=            La surcharge des fonctions            =
=============================================*/

// Ces lignes définissent les signatures pour la surcharge.
// Elles déclarent que 'maFonction' peut accepter soit une chaîne de caractères, soit un nombre, soit une valeur booléenne.
function maFonction(param: string): string;
function maFonction(param: number): number;
function maFonction(param: boolean): boolean;

// Voici l'implémentation réelle de 'maFonction'.
// La fonction utilise le type 'any' pour 'param' pour être compatible avec toutes les signatures ci-dessus.
function maFonction(param: any): any {
    return param;
}

// Utilisation de 'maFonction' avec différentes sortes d'arguments :

// Appelle 'maFonction' avec une chaîne de caractères et utilise ensuite 'toLocaleLowerCase' (une méthode des chaînes de caractères).
maFonction("toto tutu").toLocaleLowerCase();

// Appelle 'maFonction' avec un nombre et utilise ensuite 'toFixed' (une méthode des nombres).
maFonction(46).toFixed();

// Appelle 'maFonction' avec une valeur booléenne et utilise ensuite 'valueOf' (une méthode des valeurs booléennes).
maFonction(false).valueOf();

/* -------------------------------------------------------------------------- */

/*=============================================
=            Exemple avancé de surcharge            =
=============================================*/

/* DANS CE CODE, L'UTILISATION DE SURCHARGE PERMET DE DÉFINIR UNE SEULE FONCTION GETUSER QUI PEUT GÉRER DIFFÉRENTS SCÉNARIOS EN FONCTION DES ARGUMENTS FOURNIS. LE CORPS DE LA FONCTION UTILISE DES CONTRÔLES DE TYPE (TYPEOF) POUR DÉTERMINER LE SCÉNARIO APPROPRIÉ EN FONCTION DES ARGUMENTS REÇUS. */

// Définition d'une interface vide 'User'. Elle pourrait être enrichie avec des propriétés spécifiques à un utilisateur.
interface User {}

// Première signature de surcharge : permet d'obtenir un utilisateur en fournissant un 'id' numérique.
function getUser(id: number): User;

// Deuxième signature de surcharge : permet d'obtenir un utilisateur en fournissant un 'email' sous forme de chaîne de caractères.
function getUser(email: string): User;

// Troisième signature de surcharge : permet d'obtenir un utilisateur en fournissant un 'prenom' et un 'nom' (deux chaînes de caractères).
function getUser(prenom: string, nom: string): User;

// Implémentation de la fonction 'getUser' qui gère toutes les surcharges ci-dessus.
// 'param1' peut être soit un nombre (id) soit une chaîne de caractères (email ou prenom).
// 'param2' est optionnel et serait utilisé pour le 'nom' si 'param1' est le 'prenom'.
function getUser(param1: string | number, param2?: string): User {
    let user: User;

    if (typeof param1 === "number") {
        // Si 'param1' est un nombre, il est traité comme un 'id'.
        // Le code pour récupérer l'utilisateur en fonction de cet 'id' serait ici.
    } else if (typeof param2 != "undefined") {
        // Si 'param2' est défini, 'param1' est traité comme un 'prenom' et 'param2' comme un 'nom'.
        // Le code pour récupérer l'utilisateur en fonction du 'prenom' et du 'nom' serait ici.
    } else {
        // Si seul 'param1' est fourni et que c'est une chaîne de caractères, elle est traitée comme un 'email'.
        // Le code pour récupérer l'utilisateur en fonction de cet 'email' serait ici.
    }

    // La fonction renvoie l'objet 'user' (qui devrait être défini à partir des blocs de code ci-dessus).
    return user;
}
