/*=============================================
=            Les paramètres par défaut            =
=============================================*/

// Déclaration de la fonction 'ajouter3' avec deux paramètres ayant des valeurs par défaut.
// Si 'x' ou 'y' ne sont pas fournis lors de l'appel de la fonction, ils prendront respectivement les valeurs 0.
const ajouter3 = (x = 0, y = 0): number => {
    return x + y;
};

/*=============================================
=            Utilisation du rest operator            =
=============================================*/

/* EN UTILISANT LA FONCTION AJOUTER4, LE PREMIER ARGUMENT EST ASSIGNÉ À X, ET TOUS LES ARGUMENTS SUIVANTS SONT REGROUPÉS DANS LE TABLEAU REST. LA FONCTION UTILISE ENSUITE LA MÉTHODE REDUCE POUR SOMMER TOUS CES NOMBRES, EN COMMENÇANT PAR LA VALEUR DE X. */

// Déclaration de la fonction 'ajouter4'. Elle prend un argument 'x' et un nombre variable d'autres arguments
// regroupés dans le tableau 'rest'.
const ajouter4 = (x: number, ...rest: number[]): number => {
    // La méthode 'reduce' est utilisée pour sommer tous les éléments de 'rest', en commençant par la valeur de 'x'.
    return rest.reduce((previousValue, currentValue) => {
        return (previousValue += currentValue);
    }, x);
};

// Appel de la fonction 'ajouter4' avec plusieurs arguments.
let result = ajouter4(36, 37, 737, 72, 67, 1999); // // 'result' contiendra la somme de tous ces nombres.
