/*=============================================
=            Typer avant la déclaration            =
=============================================*/

// TYPAGE DE LA FONCTION, il est possible de typer une fonction avant de la déclarer, par exemple:
//Attention, lors du typage d'une fonction de cette manière il est obligatoire de préciser le type de la valeur de retour après =>

let multiplication: (nombre1: number, nombre2: number) => number;

// DÉCLARATION DE LA FONCTION, TypeScrypt utilise l'inférence pour les arguments et la valeur de retour
multiplication = (premier, second) => {
    return premier * second;
};

/*=============================================
=            Usage            =
=============================================*/

// Dans la première partie, nous définissons un type MonTypeObjet qui décrit la structure de l'objet que nous souhaitons.
type MonTypeObjet = {
    data: number[];
    // On utilise par exemple le type fonction notamment pour les objets afin de typer leurs méthodes:
    multiplier: (nombre1: number, nombre2: number) => number;
};

// Dans la deuxième partie, nous assignons une instance à la variable `obj` en respectant le type que nous avons défini.
let obj: MonTypeObjet = {
    data: [2, 3, 44],
    multiplier(n1, n2) {
        return n1 * n1;
    },
};
