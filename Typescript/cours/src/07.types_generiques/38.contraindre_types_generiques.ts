/*=============================================
=            Contraindre les types génériques            =
=============================================*/

/* Le code TypeScript fourni illustre le concept de contraintes sur les types génériques. Les types génériques permettent de définir des fonctions ou des classes qui peuvent opérer sur différents types de données, sans les spécifier à l’avance. Par exemple, la fonction maFn<T> prend un paramètre de type T, qui peut être n’importe quel type. Cependant, cela implique aussi que l’on ne peut pas utiliser les propriétés ou les méthodes spécifiques à un type sur ce paramètre, car TypeScript ne peut pas garantir qu’elles existent. C’est pourquoi les lignes param.length et param.reverse() provoquent des erreurs de compilation.

Pour résoudre ce problème, on peut utiliser des contraintes sur les types génériques, qui sont des expressions qui limitent les types possibles pour un type générique. Par exemple, la fonction maBonneFn<T extends MaInter> prend un paramètre de type T, qui doit étendre l’interface MaInter. Cela signifie que T doit avoir les propriétés et les méthodes définies dans MaInter, comme length et reverser. Ainsi, on peut utiliser ces propriétés et ces méthodes sur le paramètre sans erreur. De même, la fonction maBonneFn2<T extends Array<any>> prend un paramètre de type T, qui doit être un tableau. On peut alors utiliser la méthode reverse() sur le paramètre et renvoyer le tableau inversé. */

// Définition d'une fonction générique avec un paramètre de type T
function maFn<T>(param: T) {
    // Tentative d'accès à la propriété length du paramètre
    // ERREUR: Property 'length' does not exist on type 'T'.
    // Le compilateur TypeScript ne sait pas si le type T possède cette propriété
    // param.length;
}

// Définition d'une autre fonction générique avec un paramètre de type T
function manFn2<T>(param: T) {
    // Tentative d'appel à la méthode reverse du paramètre
    // ERREUR: Property 'reverse' does not exist on type 'T'.
    // Le compilateur TypeScript ne sait pas si le type T possède cette méthode
    // param.reverse();
}

/* -------------------------------------------------------------------------- */

// Définition d'une interface avec deux membres: une propriété length et une méthode reverser
interface MaInter {
    length: number;
    reverser(param: number): void;
}

// Définition d'une fonction générique avec un paramètre de type T
// Le type T est contraint à étendre l'interface MaInter
// Cela signifie que T doit avoir les membres définis dans MaInter
function maBonneFn<T extends MaInter>(param: T) {
    // Accès à la propriété length du paramètre
    // Pas d'erreur car le compilateur sait que T possède cette propriété
    param.length;
    // Appel à la méthode reverser du paramètre avec un argument numérique
    // Pas d'erreur car le compilateur sait que T possède cette méthode
    param.reverser(12);
}

// Définition d'une autre fonction générique avec un paramètre de type T
// Le type T est contraint à être un tableau (Array<any>)
// Cela signifie que T doit avoir les méthodes d'un tableau
function maBonneFn2<T extends Array<any>>(param: T): T {
    // Appel à la méthode reverse du paramètre pour inverser l'ordre des éléments du tableau
    // Pas d'erreur car le compilateur sait que T possède cette méthode
    param.reverse();
    // Retourne le paramètre inversé
    return param;
}

/*=============================================
=            Contraindre un type générique à partir des propriétés d'un objet            =
=============================================*/

/* Une autre façon de contraindre un type générique est d’utiliser le mot-clé keyof, qui représente l’ensemble des noms de propriétés d’un type. Par exemple, la fonction getProperty<T, U extends keyof T> prend deux paramètres: un objet de type T et une clé de type U. Le type U doit être une sous-ensemble des noms de propriétés de T, ce qui garantit que l’on peut accéder à la valeur correspondante à la clé dans l’objet. Par exemple, on peut appeler la fonction avec l’objet newObj et la clé "prop3" et obtenir la valeur 3. */

// Définition d'une fonction générique avec deux paramètres: un objet de type T et une clé de type U
// Le type U est contraint à être un sous-ensemble des noms de propriétés de T (keyof T)
// Cela signifie que U doit être une chaîne de caractères correspondant à une propriété de T
function getProperty<T, U extends keyof T>(obj: T, cle: U) {
    // Retourne la valeur de la propriété correspondant à la clé dans l'objet
    // Pas d'erreur car le compilateur sait que l'objet possède cette propriété
    return obj[cle];
}

// Définition d'un nouvel objet avec quatre propriétés numériques
const newObj = {
    prop1: 1,
    prop2: 2,
    prop3: 3,
    prop4: 4,
};

// Appel à la fonction getProperty avec l'objet newObj et la clé "prop3"
// Pas d'erreur car "prop3" est bien une propriété de newObj
getProperty(newObj, "prop3");
