/*=============================================
=            Le type Partial<T>            =
=============================================*/

// Définition d'une interface Todo avec trois propriétés: id, texte et auteur
interface Todo {
    id: number;
    texte: string;
    auteur: string;
}

// Définition d'une fonction qui prend deux paramètres: un objet de type Todo et un objet de type Partial<Todo>
// Le type Partial<Todo> est un type utilitaire qui rend toutes les propriétés de Todo optionnelles
// Cela permet de passer un objet qui ne contient qu'une partie des propriétés de Todo
function majTodo(todo: Todo, update: Partial<Todo>) {
    // Retourne un nouvel objet qui combine les propriétés de todo et de update
    // Si une propriété est présente dans update, elle écrase celle de todo
    return { ...todo, ...update };
}

// Déclaration d'une variable todo1 de type Todo et initialisation avec un objet conforme à l'interface Todo
let todo1: Todo = {
    id: 22,
    texte: "Apprendre TypeScript",
    auteur: "Paul",
};

// Appel à la fonction majTodo avec todo1 et un objet partiel qui ne contient que la propriété id
// La fonction retourne un nouvel objet qui a la même structure que todo1 mais avec l'id modifié
todo1 = majTodo(todo1, { id: 12 });

/*=============================================
=            Le type Readonly<T>            =
=============================================*/

// Déclaration d'une constante todoObj de type Readonly<Todo>
// Le type Readonly<Todo> est un type utilitaire qui rend toutes les propriétés de Todo en lecture seule
// Cela empêche de modifier les valeurs des propriétés après l'initialisation
const todoObj: Readonly<Todo> = {
    id: 2,
    texte: "Le seigneur des anneaux",
    auteur: "Tolkien",
};

// Tentative de modification de la propriété id de todoObj
// ERREUR - Cannot assign to 'id' because it is a read-only property.
// Le compilateur TypeScript interdit d'assigner une nouvelle valeur à une propriété en lecture seule
// todoObj.id = 4

/*=============================================
=            Le type Record<T>            =
=============================================*/

// Définition d'une interface PageInfo avec une propriété title
interface PageInfo {
    title: string;
}

// Définition d'un type Page qui est une union de trois littéraux de chaîne: "home", "about" et "contact"
type Page = "home" | "about" | "contact";

// Déclaration d'une constante livre de type Record<Page, PageInfo>
// Le type Record<Page, PageInfo> est un type utilitaire qui crée un type d'objet dont les clés sont les valeurs du type Page et les valeurs sont du type PageInfo
const livre: Record<Page, PageInfo> = {
    about: { title: "A propos" }, // La clé about correspond à une valeur du type Page et la valeur associée est du type PageInfo
    contact: { title: "Nous contacter," }, // Idem pour la clé contact
    home: { title: "Accueil" }, // Idem pour la clé home
};

/*=============================================
=            Le type Pick<T>            =
=============================================*/

// Définition d'un type TodoFront qui est le résultat du type utilitaire Pick<Todo, "texte" | "auteur">
// Le type Pick<Todo, "texte" | "auteur"> est un type utilitaire qui crée un sous-type de Todo en ne gardant que les propriétés texte et auteur
type TodoFront = Pick<Todo, "texte" | "auteur">;

// Déclaration d'une constante todoWithPick de type TodoFront et initialisation avec un objet conforme au sous-type de Todo
const todoWithPick: TodoFront = {
    texte: "Apprendre la programmation", // La propriété texte fait partie du sous-type de Todo
    auteur: "Michel", // La propriété auteur fait partie du sous-type de Todo
};

/*=============================================
=            le type Omit<T>            =
=============================================*/

// Définition d'un type TodoFront2 qui est le résultat du type utilitaire Omit<Todo, "id">
// Le type Omit<Todo, "id"> est un type utilitaire qui crée un sous-type de Todo en omettant la propriété id
type TodoFront2 = Omit<Todo, "id">;

// Déclaration d'une constante todoWhithOmit de type TodoFront2 et initialisation avec un objet conforme au sous-type de Todo
const todoWhithOmit: TodoFront2 = {
    texte: "Apprendre la programmation", // La propriété texte fait partie du sous-type de Todo
    auteur: "Michel", // La propriété auteur fait partie du sous-type de Todo
};
