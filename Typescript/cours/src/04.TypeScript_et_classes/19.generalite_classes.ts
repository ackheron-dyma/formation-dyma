/*=============================================
=            TypeScript et les classes            =
=============================================*/

class Personne {
    // Première spécificité, avec TypeScript, il est obligatoire de déclarer des propriétés à l'extérieur du constructor
    nom: string;
    prenom: string;
    age: number;
    constructor(nom: string, age: number, prenom: string) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }

    sePresenter() {
        return `Bonjour je m'appelle ${this.nom}`;
    }
}

const pierre = new Personne("Romand", 28, "Pierre");

console.log("🚀 ~ file: 19.generalite_classes.ts:21 ~ pierre:", pierre);

console.log(pierre.sePresenter());

/*=============================================
=            L'héritage            =
=============================================*/

// Définition de la classe Voiture
class Voiture {
    // Propriété sièges de la classe
    sieges: number;
    constructor(sieges: number) {
        this.sieges = sieges;
    }

    roule() {
        return "Je roule.";
    }
}

// Classe Sportive qui hérite de Voiture
class Sportive extends Voiture {
    // Propriété supplémentaire : chevaux
    chevaux: number;
    constructor(chevaux: number, sieges: number) {
        // Appel au constructeur de la classe parent avec super()
        super(sieges);
        this.chevaux = chevaux;
    }

    faireLaCourse() {
        console.log(`${super.roule()} Et je fais la course`);
    }
}

const monBolide = new Sportive(300, 2);

console.log("🚀 ~ file: 19.generalite_classes.ts:54 ~ monBolide:", monBolide);

console.log(monBolide.faireLaCourse());
