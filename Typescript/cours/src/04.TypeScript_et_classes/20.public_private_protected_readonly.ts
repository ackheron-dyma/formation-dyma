/*=============================================
=            Les propriétés publique par défaut            =
=============================================*/

/* LES PROPRIÉTÉS PUBLIQUES PAR DÉFAUT
Par défaut, toutes les propriétés et méthodes déclarés sur une classe sont publiques.
Lorsqu'une propriété d'une classe est public, nous pouvons y accéder à l'extérieur de la classe.  */

class Humain {
    // La propriété nom est publique et de type string
    public nom: string;

    constructor(nom: string) {
        // Le constructeur prend un paramètre nom de type string et l'assigne à la propriété nom
        this.nom = nom;
    }

    // La méthode sePresenter est publique et renvoie une chaîne de caractères
    public sePresenter() {
        return `Bonjour, je m'appelle ${this.nom}`;
    }
}

// On crée une instance de la classe Humain avec le nom "Paulo"
const unePersonne = new Humain("Paulo");

// On peut modifier la propriété nom depuis l'extérieur de la classe
unePersonne.nom = "Bishop";

// On peut appeler la méthode sePresenter depuis l'extérieur de la classe
console.log(unePersonne.sePresenter());

/*=============================================
=            Le modificateur private            =
=============================================*/

//Lorsqu'une propriété d'une classe est marquée comme private, il n'est pas possible d'y accéder à l'extérieur de sa classe.

class Humain2 {
    // La propriété nom est privée et de type string
    private nom: string;

    constructor(nom: string) {
        // Le constructeur prend un paramètre nom de type string et l'assigne à la propriété nom
        this.nom = nom;
    }

    // La méthode sePresenter est privée et renvoie une chaîne de caractères
    private sePresenter() {
        return `Bonjour, je m'appelle ${this.nom}`;
    }
}

// On crée une instance de la classe Humain2 avec le nom "Batiste"
const uneAutrePersonne = new Humain2("Batiste");

// On ne peut pas accéder à la propriété nom depuis l'extérieur de la classe
// uneAutrePersonne.nom; // ERREUR, Property 'nom' is private and only accessible within class 'Humain2'.

// On ne peut pas appeler la méthode sePresenter depuis l'extérieur de la classe
// uneAutrePersonne.sePresenter(); // ERREUR, Property 'sePresenter' is private and only accessible within class 'Humain2'.

/*=============================================
=            Le modificateur protected            =
=============================================*/

// Lorsqu'une propriété d'une classe est marquée comme protected elle peut uniquement y être accédée depuis sa classe ou depuis les classes héritant de celle-ci.

class Humain3 {
    // La propriété nom est protégée et de type string
    protected nom: string;

    constructor(nom: string) {
        // Le constructeur prend un paramètre nom de type string et l'assigne à la propriété nom
        this.nom = nom;
    }
}

// La classe Employe hérite de la classe Humain3
class Employe extends Humain3 {
    // La propriété service est privée et de type string
    private service: string;

    constructor(nom: string, service: string) {
        // Je peux récupérer la propriété "nom" de la classe "Humain3" car le modificateur protected me permet d'y accéder car la class "Employe" est une sous classe de "Humain3"
        super(nom);
        // On assigne le paramètre service à la propriété service
        this.service = service;
    }

    // La méthode presentation est publique et renvoie une chaîne de caractères
    public presentation() {
        // On peut accéder à la propriété nom depuis la sous-classe Employe
        return `Bonjour, je m'appelle ${this.nom} et je travaille au service ${this.service}.`;
    }
}

// On crée une instance de la classe Employe avec le nom "José" et le service "informatique"
const unGugus = new Employe("José", "informatique");

// On ne peut pas accéder à la propriété nom depuis l'extérieur de la classe
// unGugus.nom // ERREUR, Property 'nom' is protected and only accessible within class 'Humain3' and its subclasses.

// On ne peut pas accéder à la propriété service depuis l'extérieur de la classe
// unGugus.service // ERREUR, Property 'service' is private and only accessible within class 'Employe'.

unGugus.presentation();

/*=============================================
=            Le modificateur readonly            =
=============================================*/

/* CommentCertaines propriétés ne doivent être modifiables que lors de l'initialisation.
Vous pouvez spécifier qu'une propriété est en lecture seule en mettant readonly devant le nom de la propriété.
Cette propriété ne pourra être modifiée que lors de sa déclaration ou son initialisation dans le constructeur de la classe.  */

class UneClasse {
    // La propriété nom est en lecture seule et de type string
    readonly nom: string;

    constructor(nom: string) {
        // Le constructeur prend un paramètre nom de type string et l'assigne à la propriété nom
        this.nom = nom;
    }

    renimmer(nom: string) {
        // On ne peut pas modifier la propriété nom après son initialisation
        // this.nom = "Arnold" // ERREUR, Cannot assign to 'nom' because it is a read-only property.
        console.log(this.nom);
    }
}

// On crée une instance de la classe UneClasse avec le nom "Franco"
const uneMec = new UneClasse("Franco");

// On ne peut pas modifier la propriété nom après son initialisation
// uneMec.nom = "Karl"; // ERREUR, Cannot assign to 'nom' because it is a read-only property.
