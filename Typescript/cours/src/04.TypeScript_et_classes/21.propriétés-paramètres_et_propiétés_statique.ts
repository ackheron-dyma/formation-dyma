/*=============================================
=            Notation raccourcis (propriétés paramètres)            =
=============================================*/

/* Les propriétés paramètres sont une façon de déclarer et d’initialiser les propriétés d’une classe dans le constructeur, en utilisant des modificateurs d’accès comme public, private, protected ou readonly. Cela permet d’éviter de répéter le nom de la propriété deux fois, une fois dans la déclaration et une fois dans l’assignation. Par exemple, dans votre classe ClasseRaccourcis, vous avez défini la propriété name comme public dans le constructeur, ce qui équivaut à la déclarer en dehors du constructeur et à l’assigner dans le constructeur comme dans votre classe ClasseClassique. Les propriétés paramètres sont une fonctionnalité de TypeScript qui n’existe pas en JavaScript pur1. */

class ClasseClassique {
    // déclaration d'une propriété définit classiquement en dehors du constructeur
    name: string;

    // constructeur avec un paramètre définit classiquement
    constructor(name: string) {
        this.name = name; // assignation de la propriété name avec le paramètre name
    }
}

class ClasseRaccourcis {
    // constructeur avec "propriété paramètre" qui grace au modificateur de propriétés (public, private, protected, readonly) nous permet d'éviter de définir les propriétés en dehors du constructeur.
    constructor(public name: string) {} // déclaration et assignation de la propriété name avec le modificateur public cela nous évite l'assignation "this.name = name"
}

// ATTENTION, POUR UTILISER LA NOTATION RACCOURCIS DANS LE CONSTRUCTEUR IL FAUT OBLIGATOIREMENT PRÉCISER UN MODIFICATEUR (PUBLIC, PRIVATE, PROTECTED, READONLY)

/*=============================================
=            Propriétés et méthodes statique            =
=============================================*/

// Les méthodes statiques sont des méthodes qui appartiennent à la classe elle-même et non à ses instances. Elles sont utiles pour définir des fonctionnalités communes à toutes les instances de la classe ou qui n’ont pas besoin d’accéder aux propriétés de l’instance. Les méthodes statiques sont déclarées avec le mot-clé static.
class Personnel {
    constructor(public prenom: string, public nom: string) {}

    static decrire() {
        console.log("Cette classe Personnel est utilisée pour créer les personnes qui composent l'entreprise");
    }
}

const dylan = new Personnel("Dylan", "Hammet");
const bob = new Personnel("Bob", "Rock");

Personnel.decrire(); // Cette classe Personnel est utilisée pour créer les personnes qui composent l'entreprise

// dylan.decrire(); // ERREUR, dylan.decrire is a static member of type 'Personnel'
