// Les chaines de caractères : string

let prenom: string = "Jean";
prenom = "Paul";
prenom = `Tom`;

// Les nombres : number

let decimal: number = 42;
let flottant: number = 42.23;
let hexa: number = 0x2a;
let bin: number = 0b10011;
let octal: number = 0o52;

// Les booléens : boolean

let actif: boolean;
actif = true;
actif = false;
