/*=============================================
=            Le type array            =
=============================================*/
// La première section illustre l'utilisation du type array. Les arrays en TypeScript peuvent être typés, ce qui signifie que vous pouvez spécifier le type des éléments qu'ils contiennent.

let liste: number[] = [1, 46, 27, 53.2];

let liste2: string[] = ["bonjour", "coucou"];

let liste3: any[] = [1, "test", 46.9];

// Attention le type suivant ne peut être assigné que à un tableau vide

let liste4: [] = [];

/*=============================================
=            Le type tuple            =
=============================================*/

// La deuxième section montre l'utilisation de tuples en TypeScript. Un tuple est comme un tableau, mais sa taille est fixe et chaque élément peut avoir un type différent.

let tuple: [number, string] = [3, "Avion"];

let tuple2: [number, number] = [3, 5788];

let tuple3: [string, string, string] = ["toto", "coucou", "alfa"];

let tuple4: [string, number, object] = ["titi", 99, {}];
