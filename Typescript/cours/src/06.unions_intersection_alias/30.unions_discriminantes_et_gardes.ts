/*=============================================
=            UNE PROBLÉMATIQUE FRÉQUENTE            =

Lorsqu'on travaille avec des unions de types, nous rencontrons parfois le problème où TypeScript ne sait pas quel membre spécifique d'une union est utilisé à un moment donné. Dans l'exemple fourni, uneFonction accepte un User ou un Moderator. Lorsque nous essayons d'appeler user.editMessage("Un message"), TypeScript se plaint car il ne sait pas si user a la méthode editMessage, car cette méthode n'existe pas pour le type User.
=============================================*/

// On déclare une interface User qui représente un utilisateur avec un nom et un mail
interface User {
    name: string;
    mail: string;
}

// On déclare une interface Moderator qui étend l'interface User et qui ajoute un rôle et une méthode pour éditer un message
interface Moderator extends User {
    role: "moderator";
    editMessage: (msg: string) => string;
}

// On déclare une fonction qui prend un paramètre de type User ou Moderator
function uneFonction(user: User | Moderator) {
    // user.editMessage("Un message") // Property 'editMessage' does not exist on type 'User | Moderator'. Property 'editMessage' does not exist on type 'User'
    // Autrement dit ici TypeScript ne peut pas savoir si la fonction va recevoir un utilisateur ou un modérateur, il ne peut donc pas savoir si la méthode editMessage() existera sur l'argument passé lors de l'exécution.
}

/*=============================================
=            LES UNIONS DISCRIMINATES            =

Pour résoudre le problème ci-dessus, nous pouvons utiliser des "unions discriminates". L'idée est d'avoir un champ en commun entre les types que nous voulons discriminer (distinguer). Dans cet exemple, role est le champ discriminant. Ainsi, lorsqu'on vérifie user.role === "moderator", TypeScript sait que user est de type IntModerator dans ce bloc de code, et nous permet donc d'appeler user.editMessage.
=============================================*/

// On déclare une interface IntUser qui représente un utilisateur avec un rôle, un nom et un mail
interface IntUser {
    role: "user";
    name: string;
    mail: string;
}

// On déclare une interface IntModerator qui représente un modérateur avec un rôle, un nom, un mail et une méthode pour éditer un message
interface IntModerator {
    role: "moderator";
    name: string;
    email: string;
    editMessage: (msg: string) => string;
}

// On déclare une fonction qui prend un paramètre de type IntUser ou IntModerator
function nouvelleFonction(user: IntUser | IntModerator) {
    // On peut maintenant utiliser la propriété role pour discriminer le type du paramètre et accéder aux propriétés ou méthodes spécifiques
    if (user.role === "moderator") {
        // Si le rôle est "moderator", on sait que le paramètre est de type IntModerator et on peut appeler la méthode editMessage sans erreur
        user.editMessage("Mon message");
    }
}

/* -------------------------------------------------------------------------- */

/* Les formes géométriques (Carre, Rectangle et Cercle) sont un autre exemple. Ici, le champ discriminant est forme. La fonction calcAire utilise un switch pour déterminer le type exact de la forme et calculer l'aire en conséquence. */

// On déclare une interface Carre qui représente un carré avec une propriété forme et une propriété cote
interface Carre {
    forme: "carre";
    cote: number;
}

// On déclare une interface Rectangle qui représente un rectangle avec une propriété forme, une propriété largeur et une propriété hauteur
interface Rectangle {
    forme: "rectangle";
    largeur: number;
    hauteur: number;
}

// On déclare une interface Cercle qui représente un cercle avec une propriété forme et une propriété rayon
interface Cercle {
    forme: "cercle";
    rayon: number;
}

// On déclare un alias type Forme qui est l'union des trois interfaces Carre, Rectangle et Cercle
type Forme = Carre | Rectangle | Cercle;

// On déclare une fonction qui calcule l'aire d'une forme géométrique en fonction de son type
function calcAire(figure: Forme) {
    // On utilise la propriété forme pour discriminer le type de la figure et appliquer la formule appropriée
    switch (figure.forme) {
        case "carre":
            // Si la forme est "carre", on sait que la figure est de type Carre et on peut accéder à la propriété cote
            return figure.cote * figure.cote;
        case "rectangle":
            // Si la forme est "rectangle", on sait que la figure est de type Rectangle et on peut accéder aux propriétés largeur et hauteur
            return figure.hauteur * figure.largeur;
        case "cercle":
            // Si la forme est "cercle", on sait que la figure est de type Cercle et on peut accéder à la propriété rayon
            return Math.PI * figure.rayon ** 2;
    }
}

// Pour appeler la fonction calcAire, il faut lui passer en argument une variable de type Forme, c’est-à-dire un objet qui respecte l’une des interfaces Carre, Rectangle ou Cercle. Par exemple, on peut faire :

// On crée une variable de type Carre
let monCarre: Carre = {
    forme: "carre",
    cote: 10,
};

// On appelle la fonction calcAire avec la variable monCarre
let aireCarre = calcAire(monCarre);

// On affiche le résultat
console.log(aireCarre); // 100
