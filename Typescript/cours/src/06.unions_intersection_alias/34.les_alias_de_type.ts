/*=============================================
=            Les alias de type            =
=============================================*/

// On définit un alias de type nommé Animal, qui représente un type union de trois chaînes de caractères littérales : "Chien", "Chat" ou "Cheval"
type Animal = "Chien" | "Chat" | "Cheval";

// On définit une fonction qui prend en paramètre un animal de type Animal
function sortit(animal: Animal) {
    // On affiche l'animal dans la console
    console.log(animal);
}

// On appelle la fonction sortit en lui passant une chaîne de caractères qui correspond à l'un des types de l'alias Animal
sortit("Cheval");

/* RECOMMANDATION

Notre recommandation est de toujours utiliser les interfaces dans votre application pour tous vos modèles.

Nous vous conseillons de réserver les alias pour les unions de types littéraux vus précédemment. C'est un excellent cas d'utilisation.  */
