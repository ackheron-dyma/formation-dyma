/*=============================================
=            Les intersections            =
=============================================*/

// On définit trois interfaces qui représentent trois types d'utilisateurs possibles
interface RegularUser {
    name: string; // Le nom de l'utilisateur est une chaîne de caractères
    email: string; // L'email de l'utilisateur est une chaîne de caractères
}

interface ModeratorUser {
    role: "Modérateur"; // Le rôle du modérateur est une chaîne de caractères fixe
    editMessage: (msg: string) => void; // Le modérateur a une méthode pour éditer un message, qui prend en paramètre une chaîne de caractères et ne renvoie rien
    deleteMessage: (msg: string) => void; // Le modérateur a une méthode pour supprimer un message, qui prend en paramètre une chaîne de caractères et ne renvoie rien
}

interface AddContent {
    addMessage: (msg: string) => void; // Ce type a une méthode pour ajouter un message, qui prend en paramètre une chaîne de caractères et ne renvoie rien
}

// On définit une objet qui est de type RegularUser & AddContent, c'est-à-dire qu'elle doit avoir toutes les propriétés et méthodes des deux interfaces
let unUtilsateurNormal: RegularUser & AddContent = {
    name: "Jean", // On lui assigne un nom
    email: "jean@mail.com", // On lui assigne un email

    addMessage(msg) {}, // On lui assigne une méthode pour ajouter un message
};

// On définit une objet qui est de type RegularUser & ModeratorUser & AddContent, c'est-à-dire qu'elle doit avoir toutes les propriétés et méthodes des trois interfaces
let unAdmin: RegularUser & ModeratorUser & AddContent = {
    role: "Modérateur",
    name: "Jean", // On lui assigne un nom
    email: "jean@mail.com", // On lui assigne un email

    addMessage(msg) {}, // On lui assigne une méthode pour ajouter un message
    editMessage(msg) {}, // On lui assigne une méthode pour éditer un message
    deleteMessage(msg) {}, // On lui assigne une méthode pour supprimer un message
};
