/* Le code définit une interface nommée User qui représente le type d’un utilisateur. Une interface est un moyen de décrire la structure et les contraintes d’un type personnalisé. Une interface peut avoir des propriétés, des méthodes, des indexeurs et des types imbriqués.
L’interface User a une seule propriété nommée prenom de type string. Cela signifie que tout objet qui implémente l’interface User doit avoir une propriété prenom qui est une chaîne de caractères. */

interface UserInt {
    prenom: string;
}

function printUser(user: UserInt) {
    console.log(user.prenom);
}

let paul: UserInt = { prenom: "Paul" };

printUser(paul);

/* -------------------------------------------------------------------------- */

/* Le code répète le même processus avec une nouvelle interface nommée NewUser, une nouvelle fonction nommée newPrintUser, et un nouvel objet nommé pascal. L’objet pascal a deux propriétés : prenom et nom. L’appel de la fonction newPrintUser avec l’objet pascal affiche "Pascal" dans la console. */

interface NewUser {
    prenom: string;
}

function newPrintUser(user: NewUser) {
    console.log(user.prenom);
}

const pascal = {
    prenom: "Pascal",
    nom: "Dupont",
};

// Malgré le fait que 'pascal' ait une propriété supplémentaire ('nom') qui n'est pas définie dans 'NewUser',
// il peut toujours être utilisé comme argument pour 'newPrintUser' car il a au moins toutes les propriétés requises par 'NewUser'.
newPrintUser(pascal); // Cela fonctionne car paul a au moins une propriété nom

/* Le code crée enfin un autre objet nommé jacques qui a également deux propriétés : prenom et nom. Cependant, le code utilise la notation de type (:) pour indiquer que l’objet jacques doit être de type User. Cela provoque une erreur de compilation, car l’interface User n’a pas de propriété nom. Le message d’erreur indique que l’objet littéral ne peut spécifier que les propriétés connues, et que la propriété nom n’existe pas dans le type User. */

// Lors de la définition explicite du type d'un objet avec une interface, seules les propriétés présentes dans cette interface sont autorisées.
const jacques: UserInt = {
    prenom: "Jacques",
    // nom: "Duprey", // ERREUR
    /* Type '{ prenom: string; nom: string; }' is not assignable to type 'User'.
  Object literal may only specify known properties, and 'nom' does not exist in type 'User'. */
};
