/*=============================================
=            Les propriétés optionnelles avec ?            =
=============================================*/

/* Le code définit une interface nommée User qui a deux propriétés : prenom et nom. La propriété nom est marquée avec un point d’interrogation (?) après son nom, ce qui signifie qu’elle est optionnelle. Une propriété optionnelle peut être présente ou absente dans un objet qui implémente l’interface. Si elle est présente, elle doit respecter le type indiqué par l’interface. Si elle est absente, elle n’entraîne pas d’erreur de compilation. */

interface UserInterface {
    prenom: string;
    nom?: string;
}

function printUserNew(user: UserInterface) {
    console.log(user.prenom);
}

// Mon objet "bishop" respecte bien l'interface User car la propriété "nom" est optionnelle
const bishop = {
    prenom: "Bishop",
};

printUserNew(bishop);

/*=============================================
=            Propriété en lecture seule avec readonly            =
=============================================*/

interface AnotherUserModel {
    readonly prenom: string;
}

const ripley: AnotherUserModel = { prenom: "Ripley" };

// Toute tentative de modification du prénom de 'ripley' entraînera une erreur.

// ripley.prenom = " pamela"; // ERREUR
// Cannot assign to 'prenom' because it is a read-only property.

/* -------------------------------------------------------------------------- */
//  La lecture seule fonctionne également pour les tableaux, par exemple :

interface User2 {
    prenom: string;
    allergies: readonly string[]; // Tableau en lecture seule
}

/* Le code crée ensuite un objet nommé jean de type User2 qui a la propriété prenom avec la valeur "Jean" et la propriété allergies avec un tableau contenant "pollen" et "poussière". Le code essaie ensuite d’ajouter un élément au tableau des allergies en utilisant la méthode push. Cela provoque une erreur de compilation, car la méthode push n’existe pas sur un tableau en lecture seule. */

const jean: User2 = { prenom: "Jean", allergies: ["pollen", "poussière"] };

// Toute tentative de modification du tableau 'allergies' entraînera une erreur.

// jean.allergies.push("poils de chat"); // Erreur
// Property 'push' does not exist on type 'readonly string[]'.
