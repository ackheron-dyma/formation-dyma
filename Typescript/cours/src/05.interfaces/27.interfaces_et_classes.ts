/*=============================================
=            Les classes peuvent implémenter des interfaces            =
=============================================*/

/* Le code utilise le concept de les classes peuvent implémenter des interfaces, qui permettent de définir des types d’objets qui respectent un contrat ou une spécification donnée par une interface. Une classe qui implémente une interface doit définir toutes les propriétés et les méthodes que l’interface exige, et peut également ajouter des propriétés ou des méthodes supplémentaires. Par exemple, la classe NamedUser implémente l’interface User89, et doit donc avoir les propriétés prenom, nom et la méthode direbonjour.

Le code utilise également le concept de extension d’interface, qui permettent de créer une nouvelle interface qui hérite des propriétés et des méthodes d’une interface existante, et qui peut également ajouter des propriétés ou des méthodes supplémentaires. Par exemple, l’interface UtilisateurEnregistre étend l’interface Utilisateur, et ajoute les propriétés nom, adresse, mail et age.

Voici le code commenté : */

// Création d'une interface User89 qui définit la signature d'un type d'utilisateur
interface User89 {
    prenom: string;
    nom: string;
    direbonjour(): void;
}

// Création d'une classe NamedUser qui implémente l'interface User89
class NamedUser implements User89 {
    // Le constructeur prend deux paramètres publics prenom et nom, qui sont automatiquement assignés aux propriétés de la classe
    // Notation raccourcis (propriétés paramètres)
    constructor(public prenom: string, public nom: string) {
        // pas besoin grace au propriété paramètres de :
        // this.prenom = prenom
        // this.nom = nom
    }

    // Définition de la méthode direbonjour, qui respecte la signature de l'interface User89
    direbonjour() {
        // La méthode affiche un message dans la console en utilisant les propriétés prenom et nom
        console.log(`Bonjour je m'apelle ${this.prenom} ${this.nom}`);
    }
}

/*=============================================
=            Extension d'interface            =
=============================================*/

// Création d'une interface Utilisateur qui définit la signature d'un type d'utilisateur simple
interface Utilisateur {
    prenom: string;
    direbonjour(): void;
}

// Création d'une interface UtilisateurEnregistre qui étend l'interface Utilisateur
// Elle hérite donc des propriétés et des méthodes de l'interface Utilisateur, et ajoute des propriétés supplémentaires
interface UtilisateurEnregistre extends Utilisateur {
    nom: string;
    adresse: string;
    mail: string;
    age: number;
}

const Arnold: UtilisateurEnregistre = {
    prenom: "Arnold", // On respecte le type de la propriété prenom héritée de l'interface Utilisateur
    nom: "Swarz,", // On respecte le type de la propriété nom ajoutée par l'interface UtilisateurEnregistre
    adresse: "56 rue d'Autriche,", // On respecte le type de la propriété adresse ajoutée par l'interface UtilisateurEnregistre
    mail: "terminator@predator.com,", // On respecte le type de la propriété mail ajoutée par l'interface UtilisateurEnregistre
    age: 39, // On respecte le type de la propriété mail ajoutée par l'interface UtilisateurEnregistre

    // Définition de la méthode direbonjour, qui respecte la signature de l'interface Utilisateur héritée par l'interface UtilisateurEnregistre
    direbonjour() {
        // La méthode affiche un message dans la console en utilisant les propriétés prenom et nom
        console.log(`Bonjour je suis ${this.prenom} ${this.nom}`);
    },
};

// Appel de la méthode direbonjour sur l'objet Arnold
let bonjour = Arnold.direbonjour();
