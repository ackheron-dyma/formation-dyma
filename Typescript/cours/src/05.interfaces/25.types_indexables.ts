/*=============================================
=            Les types indexables            =
=============================================*/

// On définit une interface User qui a une propriété prenon de type string
// et qui peut avoir d'autres propriétés de n'importe quel type, en utilisant le type indexable [propName: string]: any
interface User {
    prenom: string; // Nom donné à l'utilisateur.
    [propName: string]: any; // Permet d'ajouter n'importe quelle autre propriété avec une clé de type string.
}

//  une variable jean de type User, qui a une propriété prenon avec la valeur "Jean"
export const jean: User = {
    prenom: "Jean",
};

// On ajoute une propriété nom à l'objet jean, avec la valeur "Dupont"
// Cela est possible grâce au type indexable [propName: string]: any, qui permet d'avoir des propriétés supplémentaires de n'importe quel type
jean.nom = "Dupont";

/*=============================================
=            Autre exemple            =
=============================================*/

// On définit une interface TableauString qui représente un tableau d'éléments de type string
// Le tableau peut être accédé par des indices de type number, en utilisant le type indexable [index: number]: string
interface TableauString {
    [index: number]: string; // Spécifie que chaque index de type number pointe vers une valeur de type string.
}

let myArray: TableauString = ["Pierre", "Paul", "Bob"];

/*=============================================
=            Recommandations            =
=============================================*/

// On définit une interface UserInterface qui a un type indexable [index: string]: number
// Cela signifie que toutes les propriétés de l'objet doivent être de type number
interface UserInterface {
    [index: string]: number;
    age: number; // OK, age est de type number
    // prenom: string // ERREUR, Property 'prenom' of type 'string' is not assignable to 'string' index type 'number'.
}
