/*=============================================
=            Exemple de type avancé:
Obtenir les noms de propriétés d'un type qui ne sont pas des fonctions            =
=============================================*/

/* Le code TypeScript fourni utilise un type avancé qui permet d’obtenir les noms de propriétés d’un type qui ne sont pas des fonctions. Ce type avancé utilise les concepts suivants :

    Les types génériques, qui sont des types paramétrés par d’autres types. Par exemple, le type ProprietesPasFonctions<T> est un type générique qui prend un type T en paramètre.
    Les types indexés, qui sont des types qui représentent les noms de propriétés d’un autre type. Par exemple, le type keyof T est un type indexé qui contient tous les noms de propriétés du type T.
    Les types mappés, qui sont des types qui créent de nouveaux types à partir d’autres types en appliquant une transformation sur leurs propriétés. Par exemple, le type { [K in keyof T]: T[K] extends Function ? never : K } est un type mappé qui prend un type T et pour chaque propriété K du type T, crée une nouvelle propriété K dont le type dépend de la condition T[K] extends Function ? never : K. Cette condition signifie que si le type de la propriété K du type T est une fonction, alors le type de la nouvelle propriété K est never, sinon il est K.
    Les types conditionnels, qui sont des types qui dépendent d’une condition logique. Par exemple, le type T[K] extends Function ? never : K est un type conditionnel qui signifie que si le type T[K] est une sous-classe de Function, alors le type est never, sinon il est K.
    Les types d’accès aux propriétés, qui sont des types qui accèdent au type d’une propriété d’un autre type. Par exemple, le type T[K] est un type d’accès aux propriétés qui accède au type de la propriété K du type T.
    Le type never, qui est un type qui représente l’absence de valeur. Il est utilisé pour indiquer qu’une expression ne peut jamais se produire ou renvoyer une valeur.

Le type avancé ProprietesPasFonctions<T> utilise ces concepts pour créer un nouveau type qui contient les noms de propriétés du type T qui ne sont pas des fonctions. Il fonctionne de la manière suivante :

    Il prend un type générique T en paramètre.
    Il crée un nouveau type mappé { [K in keyof T]: T[K] extends Function ? never : K } qui pour chaque propriété K du type T, crée une nouvelle propriété K dont le type dépend de la condition T[K] extends Function ? never : K.
    Il accède au type indexé [keyof T] de ce nouveau type mappé, ce qui revient à prendre l’union de tous les types des propriétés du nouveau type mappé.
    Le résultat est donc un nouveau type qui contient les noms de propriétés du type T qui ne sont pas des fonctions, car les propriétés dont le type est une fonction ont été éliminées par le type never.

Par exemple, le code fourni définit une interface User007 avec quatre propriétés : id, name, adresses et updateName. La propriété updateName est une fonction, tandis que les autres sont des types simples. Le code définit ensuite un alias de type proprietes qui est égal à ProprietesPasFonctions<User007>. Ce type avancé va donc créer un nouveau type qui contient les noms des propriétés de l’interface User007 qui ne sont pas des fonctions, c’est-à-dire : id, name et adresses. */

// Définition d'une interface avec quatre propriétés
interface User007 {
    id: number;
    name: string;
    adresses: string[];
    updateName(newname: string): void;
}

// Définition d'un alias de type générique qui prend un paramètre T
type ProprietesPasFonctions<T> =
    // Création d'un nouveau type mappé qui pour chaque propriété K du type T
    {
        [K in keyof T]: // Crée une nouvelle propriété K dont le type dépend de la condition suivante
        T[K] extends Function
            ? // Si le type de la propriété K du type T est une fonction, alors le type est never
              never
            : // Sinon, le type est K
              K;
    }[keyof T]; // Accès au type indexé de ce nouveau type mappé, ce qui revient à prendre l'union de tous les types des propriétés

// Définition d'un alias de type qui est égal à ProprietesPasFonctions<User007>
type proprietes = ProprietesPasFonctions<User007>;
