/*=============================================
=            Utilisation des types mappés            =
=============================================*/
/* Le code TypeScript fourni utilise les types mappés, qui sont une fonctionnalité introduite dans la version 2.1 du langage.

Les types mappés permettent de créer de nouveaux types à partir d’autres types existants en appliquant une transformation sur leurs propriétés. 

Par exemple, le type TempData<T> est un type mappé qui prend un type générique T et lui ajoute une propriété isValid de type boolean, tout en rendant optionnelles toutes les propriétés de T (en utilisant le symbole ?). 

Ainsi, le type TempData<InterfaceTodo> est un type qui contient les propriétés id, texte et auteur de type number, string et string respectivement, mais qui sont toutes optionnelles, ainsi qu’une propriété isValid de type boolean qui est obligatoire. La variable tempTodo est donc un objet qui respecte ce type, car elle a la propriété isValid à true et la propriété texte à "Texte en cours", mais pas les autres propriétés de InterfaceTodo. */

// Définition d'une interface avec trois propriétés
interface InterfaceTodo {
    id: number;
    texte: string;
    auteur: string;
}

// Définition d'un type mappé qui prend un type générique T et lui ajoute une propriété isValid
type TempData<T> = {
    // Pour chaque propriété P du type T, on crée une nouvelle propriété P du même type, mais optionnelle
    [P in keyof T]?: T[P];
} & {
    // On ajoute une nouvelle propriété isValid de type boolean, qui est obligatoire
    isValid: boolean;
};

// Création d'une variable de type TempData<InterfaceTodo>
const tempTodo: TempData<InterfaceTodo> = {
    // On assigne une valeur à la propriété texte, qui est optionnelle
    texte: "Texte en cours",
    // On assigne une valeur à la propriété isValid, qui est obligatoire
    isValid: true,
};

/* -------------------------------------------------------------------------- */

/* Le type TempData2<T> est un autre type mappé qui prend un type générique T et lui ajoute une propriété prop20 de type boolean et une propriété prop1 de type string, tout en rendant optionnelles toutes les autres propriétés de T. 

Ainsi, le type TempData2<EncoreUnUser> est un type qui contient les propriétés id, prop1, prop2 et prop20 de type number, string, string et boolean respectivement, mais dont seule la propriété id est optionnelle, car les autres sont redéfinies par le type mappé. La variable tempTodo2 est donc un objet qui respecte ce type, car elle a les propriétés prop20 à true et la propriété prop1 à "Texte", mais pas la propriété id. */

// Définition d'une autre interface avec quatre propriétés
interface EncoreUnUser {
    id: number;
    prop1: string;
    prop2: string;
    prop20: boolean;
}

// Définition d'un autre type mappé qui prend un type générique T et lui ajoute deux propriétés prop20 et prop1
type TempData2<T> = {
    // Pour chaque propriété P du type T, on crée une nouvelle propriété P du même type, mais optionnelle
    [P in keyof T]?: T[P];
} & {
    // On ajoute deux nouvelles propriétés prop20 et prop1 de type boolean et string respectivement, qui sont obligatoires
    prop20: boolean;
    prop1: string;
};

// Création d'une variable de type TempData2<EncoreUnUser>
const tempTodo2: TempData2<EncoreUnUser> = {
    // On assigne une valeur à la propriété prop20, qui est obligatoire
    prop20: true,
    // On assigne une valeur à la propriété prop1, qui est obligatoire
    prop1: "Texte",
};
