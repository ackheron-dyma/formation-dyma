/*=============================================
=            Utilisation des décorateurs sur les propriétés            =
=============================================*/

/* Le code utilise des décorateurs, qui sont des fonctions spéciales qui peuvent modifier le comportement ou l’apparence d’une classe, d’une méthode, d’une propriété ou d’un paramètre. Les décorateurs sont une fonctionnalité expérimentale de TypeScript qui suit la proposition de décorateurs ECMAScript.

Le code définit deux décorateurs : @Prop et @Param. Ces décorateurs sont appliqués à différents éléments du code et affichent des informations sur ces éléments dans la console.

Le décorateur @Prop est appliqué aux propriétés name des classes AnotherClass et ItsAClass. Ce décorateur prend en paramètres la cible (la classe à laquelle appartient la propriété) et le nom de la propriété. Il affiche ces paramètres dans la console.

Le décorateur @Param est appliqué au paramètre fancy de la méthode greeting de la classe ItsAClass. Ce décorateur prend en paramètres la cible (la classe à laquelle appartient la méthode), le nom de la méthode et l’index du paramètre. Il affiche ces paramètres dans la console. */

/* -------------------------------------------------------------------------- */

/* Définition du décorateur @Prop */
function Prop(cible: any, nomProp: string) {
    // Afficher la cible et le nom de la propriété dans la console
    console.log(cible, nomProp);
}

class AnotherClass {
    // Appliquer le décorateur @Prop à la propriété name
    @Prop
    public name: string;

    constructor(name: string) {
        this.name = name;
    }

    public greeting(fancy?: boolean) {}
}

/*=============================================
=            Utilisation des décorateurs sur les paramètres des méthodes            =
=============================================*/

/* Définition du décorateur @Param */
function Param(cible: any, nomMethode: string, indexParam: number) {
    // Afficher la cible, le nom de la méthode et l'index du paramètre dans la console
    console.log(cible, nomMethode, indexParam);
}

class ItsAClass {
    // Appliquer le décorateur @Prop à la propriété name
    @Prop
    public name: string;

    constructor(name: string) {
        this.name = name;
    }

    // Appliquer le décorateur @Param au paramètre fancy
    public greeting(@Param fancy?: boolean) {
        // Afficher un message selon la valeur de fancy
        console.log(`${fancy ? "Hello" : "Hi"} ${this.name}`);
    }
}
