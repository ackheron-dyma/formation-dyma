/*=============================================
=            Les factories de décorateur            =
=============================================*/

/*      Un factory de décorateur nommé "ComponentFactory". Le factory de décorateur prend un objet en tant qu'argument destructuré avec deux propriétés : "template" (une chaîne de caractères) et "selector" (une chaîne de caractères). Ce factory renvoie une fonction qui prend un paramètre "target" de type "MyComponent" (la classe sur laquelle le décorateur sera appliqué).

À l'intérieur de la fonction générée par le factory de décorateur, vous récupérez un élément du DOM en utilisant le sélecteur spécifié dans "selector" (par exemple, "app"). Si l'élément est trouvé, le contenu de l'élément est remplacé par le modèle HTML spécifié dans "template" (par exemple, "<h1>Hello<h1>").

Vous utilisez le factory de décorateur "@CompenentFactory" (qui devrait être "@ComponentFactory") pour décorer la classe "MyComponent". Lorsque le décorateur est appliqué à la classe, la fonction générée par le factory est exécutée, ce qui peut modifier le comportement de la classe en fonction des paramètres spécifiés. */

// Définition de la factory de décorateur
function ComponentFactory({ template, selector }: { template: string; selector: string }) {
    // Renvoi d'une fonction décorateur
    return (target: MyComponent) => {
        // Sélection de l'élément HTML correspondant au sélecteur
        const elem = document.querySelector(selector);
        // Si l'élément existe, modification de son contenu HTML avec le template
        if (elem) {
            elem.innerHTML = template;
        }
    };
}

// Application du décorateur à la classe MyComponent avec les paramètres template et selector
@ComponentFactory({
    template: "<h1>Hello<h1>",
    selector: "app",
})
class MyComponent {
    // Constructeur de la classe avec un paramètre name
    constructor(public name: string) {}
}
