/*=============================================
=            La syntaxe des décorateurs            =

Le code TypeScript utilise des décorateurs, une fonctionnalité introduite dans ECMAScript 2016 (ES6) qui est également prise en charge en TypeScript. Les décorateurs sont des fonctions qui sont utilisées pour annoter des classes et des membres de classes, leur permettant de modifier ou d'étendre leur comportement.
=============================================*/

/*     Un décorateur nommé "decorateur" est défini en tant que fonction. Il prend un paramètre "target" de type "any". Le paramètre "target" représente la cible sur laquelle le décorateur est appliqué, dans ce cas, la classe "TheClasse".

    La classe "TheClasse" est définie avec un constructeur qui prend un argument "name" de type "string" et une méthode nommée "uneMet".

    Le décorateur "@decorateur" est appliqué à la classe "TheClasse" en le plaçant juste au-dessus de la déclaration de la classe. Cela signifie que le décorateur "decorateur" sera appelé avec la classe "TheClasse" en tant que cible.

    Le décorateur "decorateur" dans cet exemple se contente d'afficher la cible (la classe "TheClasse") dans la console. Il n'effectue aucune action particulière, mais il pourrait être utilisé pour effectuer des opérations spécifiques sur la classe, comme la modification de son comportement ou la collecte de méta-informations. */

// Définition du décorateur "decorateur"
function decorateur(target: any) {
    console.log(target);
}

// Utilisation du décorateur sur la classe "TheClasse"
@decorateur
class TheClasse {
    constructor(public name: string) {}

    uneMet() {}
}
