/*=============================================
=            Les décorateurs sur les méthodes et les accessors            =
=============================================*/

// Premier Exemple

/* Dans le premier exemple, il y a un décorateur nommé enumerable qui prend en argument une valeur booléenne et qui renvoie une fonction décorateur. La fonction décorateur modifie le descripteur de propriété de la méthode methode de la classe UneAutreClasse pour lui assigner la valeur booléenne passée en argument à la propriété enumerable. La propriété enumerable détermine si la méthode apparaît dans une boucle for...in ou dans les clés renvoyées par Object.keys. En appliquant le décorateur avec la valeur true, on rend la méthode énumérable. */

// Définition du décorateur sur les méthodes

function enumerable(valeur: boolean) {
    // Renvoi d'une fonction décorateur
    return function (cible: any, nomMethode: string, descripteur: PropertyDescriptor) {
        // Modification du descripteur de propriété pour assigner la valeur à la propriété enumerable
        descripteur.enumerable = valeur;
    };
}

class UneAutreClasse {
    // Application du décorateur à la méthode statique methode avec la valeur true
    @enumerable(true)
    static methode() {
        return "Coucou !";
    }
}
for (const prop in UneClasse) {
    console.log(prop); // methode
}
/* -------------------------------------------------------------------------- */

// Second exemple

/* Dans le deuxième exemple, il y a un décorateur nommé enumerable2 qui prend en argument une valeur booléenne et qui renvoie une fonction décorateur. La fonction décorateur modifie le descripteur de propriété des accessors x et y de la classe UneAutreClasseBis pour leur assigner la valeur booléenne passée en argument à la propriété enumerable. Les accessors sont des fonctions qui permettent d’accéder ou de modifier les valeurs des propriétés privées d’une classe. En appliquant le décorateur avec la valeur true, on rend les accessors énumérables. */

// Définition du décorateur sur les accessors

function enumerable2(valeur: boolean) {
    // Renvoi d'une fonction décorateur
    return function (target: any, nomProp: string, descripteur: PropertyDescriptor) {
        // Modification du descripteur de propriété pour assigner la valeur à la propriété enumerable
        descripteur.enumerable = valeur;
    };
}

class UneAutreClasseBis {
    // Constructeur de la classe avec deux paramètres privés _x et _y
    constructor(private _x: number, private _y: number) {}

    // Application du décorateur à l'accessor x avec la valeur true
    @enumerable2(true)
    get x() {
        return this._x;
    }

    // Application du décorateur à l'accessor y avec la valeur true
    @enumerable2(true)
    get y() {
        return this._y;
    }
}

const instance = new UneAutreClasseBis(1, 2);

for (const prop in instance) {
    console.log(prop); // _x _y x y
}
