/*=============================================
=            Utilisation de plusieurs décorateurs            =
=============================================*/

/*      Une fonction "MyComponentFactory" qui prend un objet destructuré en tant qu'argument avec deux propriétés : "template" (une chaîne de caractères) et "selector" (une chaîne de caractères). Cette fonction renvoie une fonction qui prend une cible "cible" de type "MyComponent". La fonction générée par le décorateur est destinée à personnaliser le contenu d'un élément HTML spécifié par le sélecteur "selector" en utilisant le modèle HTML spécifié dans "template".

Vous avez également défini une fonction "geler" qui prend un constructeur (de type "Function") en tant qu'argument. Cette fonction utilise les méthodes "Object.freeze" pour geler à la fois le constructeur lui-même et son prototype. Cela signifie que la classe et son prototype ne peuvent pas être modifiés après avoir été gelés.

Vous avez appliqué les décorateurs "@geler" et "@MyComponentFactory" à la classe "AnotherMyComponent". Cela signifie que la classe "AnotherMyComponent" sera gelée, et le décorateur "MyComponentFactory" sera utilisé pour personnaliser le contenu de l'élément HTML avec le modèle spécifié. */

// Définition de la factory de décorateur MyComponentFactory
function MyComponentFactory({ template, selector }: { template: string; selector: string }) {
    // Renvoi d'une fonction décorateur
    return (cible: MyComponent) => {
        // Sélection de l'élément HTML correspondant au sélecteur
        const elem = document.querySelector(selector);
        // Si l'élément existe, modification de son contenu HTML avec le template
        if (elem) elem.innerHTML = template;
    };
}

// Définition de la factory de décorateur geler
function geler(constructor: Function) {
    // Gel du constructeur et de son prototype
    Object.freeze(constructor);
    Object.freeze(constructor.prototype);
}

// Application des deux décorateurs à la classe AnotherMyComponent avec les paramètres template et selector
@geler
@MyComponentFactory({
    template: "<h1>Hello</h1>",
    selector: "app",
})
class AnotherMyComponent {
    // Constructeur de la classe avec un paramètre name
    constructor(public name: string) {}
}
