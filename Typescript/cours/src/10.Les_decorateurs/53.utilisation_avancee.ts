/* Le code utilise un décorateur de classe, qui est une fonction qui prend en paramètre le constructeur d’une classe et renvoie une nouvelle classe modifiée. Les décorateurs de classe sont une fonctionnalité expérimentale de TypeScript qui suit la proposition de décorateurs ECMAScript.

Le code définit un décorateur de classe nommé @ComponentFactory, qui prend en paramètre un objet avec deux propriétés : template et selector. Ces propriétés représentent respectivement le contenu HTML à afficher et le sélecteur CSS du conteneur où l’afficher. Le décorateur renvoie une fonction générique qui prend en paramètre le constructeur d’une classe et renvoie une nouvelle classe qui hérite de la classe originale et qui ajoute les propriétés selector et template, ainsi qu’une méthode render qui affiche le contenu HTML dans le conteneur.

Le code utilise le décorateur @ComponentFactory pour créer une nouvelle classe nommée MyComponentElement, qui hérite de la classe ClassComponent. La classe ClassComponent implémente l’interface Component, qui définit une méthode render. La classe ClassComponent a aussi un constructeur qui prend en paramètre un nom et une méthode render vide.

Le code crée ensuite une instance de la classe MyComponentElement avec le nom “Jean” et l’assigne à la variable foo. */

/* Définition du décorateur de classe @ComponentFactory */
function ComponentFactory({ template, selector }: { template: string; selector: string }) {
    // Renvoyer une fonction générique qui prend en paramètre le constructeur d'une classe
    return <T extends { new (...args: any[]): {} }>(constructor: T): T => {
        // Renvoyer une nouvelle classe qui hérite de la classe originale
        return class MyComponentElement extends constructor {
            // Déclarer les propriétés selector et template avec les valeurs passées au décorateur
            private selector: string = selector;
            private template: string = template;

            // Définir le constructeur de la nouvelle classe
            constructor(...args: any[]) {
                // Appeler le constructeur de la classe originale avec les arguments passés
                super(args);
            }

            // Définir la méthode render de la nouvelle classe
            render() {
                // Sélectionner l'élément du DOM correspondant au sélecteur
                const elem = document.querySelector(selector);
                // Si l'élément existe, lui assigner le contenu HTML du template
                if (elem) elem.innerHTML = template;
            }
        };
    };
}

// Définir une interface Component qui définit une méthode render
interface Component {
    render: () => void;
}

// Appliquer le décorateur @ComponentFactory à la classe ClassComponent avec les paramètres template et selector
@ComponentFactory({
    template: "<h1>Hello</h1>",
    selector: "app",
})
class ClassComponent implements Component {
    // Définir le constructeur de la classe ClassComponent qui prend en paramètre un nom
    constructor(public name: string) {}

    // Définir la méthode render de la classe ClassComponent comme vide
    render() {}
}

// Créer une instance de la classe MyComponentElement avec le nom "Jean" et l'assigner à la variable foo
const foo = new MyComponent("Jean");
