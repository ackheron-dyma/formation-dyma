/*=============================================
=            Les instruction if            =
=============================================*/
// On définit une fonction qui prend en paramètre un utilisateur qui peut être soit un simple utilisateur, soit un modérateur
function unefonctionCommune(user) {
    // On utilise une instruction if pour vérifier si le rôle de l'utilisateur est égal à "Modérateur"
    if (user.role === "Modérateur") {
        // Si c'est le cas, on appelle la méthode editMessage sur l'utilisateur, en lui passant une chaîne de caractères en paramètre
        user.editMessage("Mon nouveau message");
    }
}
/* -------------------------------------------------------------------------- */
// Utilisation des prédicats
// On définit une fonction qui prend en paramètre un utilisateur qui peut être soit un simple utilisateur, soit un modérateur
// Cette fonction renvoie un booléen qui indique si l'utilisateur est un modérateur ou non
// On utilise le mot-clé is suivi du type Moderateur pour indiquer que cette fonction est un prédicat, c'est-à-dire qu'elle permet de vérifier le type d'une variable
function isMod(user) {
    // On utilise l'opérateur as pour convertir le type de l'utilisateur en Moderateur
    // On vérifie ensuite si la propriété editMessage existe sur l'utilisateur converti
    // Si c'est le cas, on renvoie true, sinon on renvoie false
    return user.editMessage !== undefined;
}
// On définit une fonction qui prend en paramètre un utilisateur qui peut être soit un simple utilisateur, soit un modérateur
function uneFonctionQuiUtiliseLePredicat(user) {
    // On utilise une instruction if pour appeler la fonction isMod sur l'utilisateur
    if (isMod(user)) {
        // Si la fonction renvoie true, cela signifie que l'utilisateur est un modérateur
        // On peut donc appeler la méthode editMessage sur l'utilisateur, en lui passant une chaîne de caractères en paramètre
        user.editMessage("Couou je suis un modo");
    }
}
/*=============================================
=            Utiliser l'opérateur in            =
=============================================*/
// On définit une fonction qui prend en paramètre un utilisateur qui peut être soit un simple utilisateur, soit un modérateur
function uneFonctionAvecOperateurIn(user) {
    // On utilise une instruction if avec l'opérateur in pour vérifier si la propriété editMessage existe sur l'utilisateur
    if ("editMessage" in user) {
        // Si c'est le cas, cela signifie que l'utilisateur est un modérateur
        // On peut donc appeler la méthode editMessage sur l'utilisateur, en lui passant une chaîne de caractères en paramètre
        user.editMessage("Salut es choums");
    }
}
/*=============================================
=            Utiliser l’opérateur instanceof            =
=============================================*/
// On définit deux classes qui représentent deux types d'utilisateurs possibles
class ClassUser {
    constructor(role, name, email) {
        this.role = role;
        this.name = name;
        this.email = email;
    } // Le constructeur de la classe prend en paramètre le rôle, le nom et l'email de l'utilisateur et les assigne comme propriétés publiques de l'instance
}
class ClassModerateur {
    constructor(role, name, email) {
        this.role = role;
        this.name = name;
        this.email = email;
    } // Le constructeur de la classe prend en paramètre le rôle, le nom et l'email du modérateur et les assigne comme propriétés publiques de l'instance
    editMessage(msg) { } // La classe modérateur a une méthode pour éditer un message, qui prend en paramètre une chaîne de caractères
}
// On définit une fonction qui prend en paramètre un utilisateur qui peut être soit une instance de la classe ClassUser, soit une instance de la classe ClassModerateur
function uneFonctionAvecTypeClasse(user) {
    // On utilise une instruction if avec l'opérateur instanceof pour vérifier si l'utilisateur est une instance de la classe ClassModerateur
    if (user instanceof ClassModerateur) {
        // Si c'est le cas, cela signifie que l'utilisateur est un modérateur
        // On peut donc appeler la méthode editMessage sur l'utilisateur, en lui passant une chaîne de caractères en paramètre
        user.editMessage("Coucou je suis le grand Modo");
    }
}
