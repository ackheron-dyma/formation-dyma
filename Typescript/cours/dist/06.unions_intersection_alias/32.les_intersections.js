/*=============================================
=            Les intersections            =
=============================================*/
// On définit une objet qui est de type RegularUser & AddContent, c'est-à-dire qu'elle doit avoir toutes les propriétés et méthodes des deux interfaces
let unUtilsateurNormal = {
    name: "Jean",
    email: "jean@mail.com",
    addMessage(msg) { }, // On lui assigne une méthode pour ajouter un message
};
// On définit une objet qui est de type RegularUser & ModeratorUser & AddContent, c'est-à-dire qu'elle doit avoir toutes les propriétés et méthodes des trois interfaces
let unAdmin = {
    role: "Modérateur",
    name: "Jean",
    email: "jean@mail.com",
    addMessage(msg) { },
    editMessage(msg) { },
    deleteMessage(msg) { }, // On lui assigne une méthode pour supprimer un message
};
