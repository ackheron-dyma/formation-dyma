/*=============================================
=            UNE PROBLÉMATIQUE FRÉQUENTE            =

Lorsqu'on travaille avec des unions de types, nous rencontrons parfois le problème où TypeScript ne sait pas quel membre spécifique d'une union est utilisé à un moment donné. Dans l'exemple fourni, uneFonction accepte un User ou un Moderator. Lorsque nous essayons d'appeler user.editMessage("Un message"), TypeScript se plaint car il ne sait pas si user a la méthode editMessage, car cette méthode n'existe pas pour le type User.
=============================================*/
// On déclare une fonction qui prend un paramètre de type User ou Moderator
function uneFonction(user) {
    // user.editMessage("Un message") // Property 'editMessage' does not exist on type 'User | Moderator'. Property 'editMessage' does not exist on type 'User'
    // Autrement dit ici TypeScript ne peut pas savoir si la fonction va recevoir un utilisateur ou un modérateur, il ne peut donc pas savoir si la méthode editMessage() existera sur l'argument passé lors de l'exécution.
}
// On déclare une fonction qui prend un paramètre de type IntUser ou IntModerator
function nouvelleFonction(user) {
    // On peut maintenant utiliser la propriété role pour discriminer le type du paramètre et accéder aux propriétés ou méthodes spécifiques
    if (user.role === "moderator") {
        // Si le rôle est "moderator", on sait que le paramètre est de type IntModerator et on peut appeler la méthode editMessage sans erreur
        user.editMessage("Mon message");
    }
}
// On déclare une fonction qui calcule l'aire d'une forme géométrique en fonction de son type
function calcAire(figure) {
    // On utilise la propriété forme pour discriminer le type de la figure et appliquer la formule appropriée
    switch (figure.forme) {
        case "carre":
            // Si la forme est "carre", on sait que la figure est de type Carre et on peut accéder à la propriété cote
            return figure.cote * figure.cote;
        case "rectangle":
            // Si la forme est "rectangle", on sait que la figure est de type Rectangle et on peut accéder aux propriétés largeur et hauteur
            return figure.hauteur * figure.largeur;
        case "cercle":
            // Si la forme est "cercle", on sait que la figure est de type Cercle et on peut accéder à la propriété rayon
            return Math.PI * figure.rayon ** 2;
    }
}
// Pour appeler la fonction calcAire, il faut lui passer en argument une variable de type Forme, c’est-à-dire un objet qui respecte l’une des interfaces Carre, Rectangle ou Cercle. Par exemple, on peut faire :
// On crée une variable de type Carre
let monCarre = {
    forme: "carre",
    cote: 10,
};
// On appelle la fonction calcAire avec la variable monCarre
let aireCarre = calcAire(monCarre);
// On affiche le résultat
console.log(aireCarre); // 100
