/*=============================================
=            LES UNIONS DES TYPES            =

L'une des fonctionnalités intéressantes de TypeScript est la capacité de définir une variable qui peut avoir des valeurs de plus d'un type. C'est ce qu'on appelle une "union de types". Dans l'exemple, la variable uneVar peut être soit de type string soit de type number.
=============================================*/
// On déclare une variable qui peut être de type string ou number
let uneVar;
// On peut assigner une chaîne de caractère à la variable
uneVar = "Une chaîne de caractère";
// On peut aussi assigner un nombre à la variable
uneVar = 27;
/*=============================================
=            UNIONS DE TYPE DE FONCTION            =

De même que pour les variables, les fonctions peuvent également retourner des valeurs de différents types. Dans l'exemple, la fonction add retourne soit une string soit void (rien), en fonction de la valeur de l'argument isAdmin.
=============================================*/
// On déclare une fonction qui prend un paramètre booléen et qui renvoie soit une chaîne de caractère, soit rien (void)
function add(isAdmin) {
    // Si le paramètre est vrai, on renvoie la chaîne "Secret"
    if (isAdmin) {
        return "Secret";
    }
    else {
        // Sinon, on ne renvoie rien
        return;
    }
}
// On déclare une variable qui peut être de type User ou Moderator
let monUtilisateur;
// On déclare une autre variable qui peut être de type User ou null (si aucun utilisateur n'est connecté par exemple)
let unAutreUtilisateur;
