/*=============================================
=            TypeScript et les classes            =
=============================================*/
class Personne {
    constructor(nom, age, prenom) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }
    sePresenter() {
        return `Bonjour je m'appelle ${this.nom}`;
    }
}
const pierre = new Personne("Romand", 28, "Pierre");
console.log("🚀 ~ file: 19.generalite_classes.ts:21 ~ pierre:", pierre);
console.log(pierre.sePresenter());
/*=============================================
=            L'héritage            =
=============================================*/
// Définition de la classe Voiture
class Voiture {
    constructor(sieges) {
        this.sieges = sieges;
    }
    roule() {
        return "Je roule.";
    }
}
// Classe Sportive qui hérite de Voiture
class Sportive extends Voiture {
    constructor(chevaux, sieges) {
        // Appel au constructeur de la classe parent avec super()
        super(sieges);
        this.chevaux = chevaux;
    }
    faireLaCourse() {
        console.log(`${super.roule()} Et je fais la course`);
    }
}
const monBolide = new Sportive(300, 2);
console.log("🚀 ~ file: 19.generalite_classes.ts:54 ~ monBolide:", monBolide);
console.log(monBolide.faireLaCourse());
