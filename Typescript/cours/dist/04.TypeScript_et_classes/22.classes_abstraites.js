/*=============================================
=            Les classes abstraites            =
=============================================*/
// Une classe abstraite est une classe qui ne peut pas être instanciée directement, mais qui sert de modèle pour les classes qui en héritent.
class Perso {
    // Le constructeur de la classe abstraite prend en paramètre le genre du personnage.
    constructor(genre) {
        this.genre = genre;
    }
}
// const unPerso = new Perso //Cannot create an instance of an abstract class.
// On ne peut pas créer une instance d'une classe abstraite avec le mot-clé new.
class Homme extends Perso {
    // La classe Homme hérite de la classe abstraite Perso et doit appeler le constructeur de la classe parente avec super().
    constructor() {
        super("Un homme");
    }
}
// On peut créer une instance d'une classe qui hérite d'une classe abstraite.
const homme = new Homme();
/*=============================================
=            Les méthodes abstraites            =
=============================================*/
// Une méthode abstraite est une méthode qui n'a pas d'implémentation dans la classe abstraite, mais qui doit être définie dans les classes qui en héritent.
class UnAutrePerso {
    constructor(gene) {
        this.gene = gene;
    }
    // La méthode direAuRevoir() n'est pas abstraite et a une implémentation par défaut dans la classe abstraite.
    direAuRevoir() {
        console.log("Au revoir !");
    }
}
class Femme extends UnAutrePerso {
    constructor() {
        super("Une femme");
    }
    // La classe Femme doit définir la méthode direBonjour() qui est abstraite dans la classe parente.
    direBonjour() {
        console.log("Bonjour !");
    }
}
const femme = new Femme();
femme.direBonjour(); // Affiche "Bonjour !"
femme.direAuRevoir(); // Affiche "Au revoir !"
/*=============================================
=            Typer les instances avec la classe comme type            =
=============================================*/
// On déclare une variable utilisateur de type Femme, qui est la classe définie précédemment.
let utilisateur;
// On peut affecter à cette variable une instance de la classe Femme avec le mot-clé new.
utilisateur = new Femme();
// On peut appeler les méthodes de la classe Femme sur la variable utilisateur, comme direAuRevoir().
utilisateur.direAuRevoir();
