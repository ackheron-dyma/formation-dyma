/*PREMIER EXEMPLE
Le code utilise le concept de classes génériques, qui permet de créer des classes qui peuvent définir et utiliser des attributs et des méthodes de différents types de données sans perdre la vérification de type.

La classe Stack<T> représente une structure de données de type pile, qui suit le principe dernier entré, premier sorti (LIFO). La classe utilise un paramètre de type générique T pour définir le type des éléments de la pile. La classe a une propriété items qui est un tableau de type T[] et qui stocke les éléments de la pile. La classe a aussi trois méthodes: push, pop et display. La méthode push prend un élément de type T en paramètre et l’ajoute à la fin du tableau items. La méthode pop renvoie et supprime le dernier élément du tableau items, ou undefined si le tableau est vide. La méthode display affiche le contenu du tableau items dans la console.
*/
// Définition d'une classe générique qui représente une pile
class Stack {
    // Déclaration d'un attribut items qui est un tableau de type T[]
    constructor(items = []) {
        this.items = items;
    }
    // Définition d'une méthode push qui prend un élément de type T en paramètre et l'ajoute à la fin du tableau items
    push(item) {
        this.items.push(item);
    }
    // Définition d'une méthode pop qui renvoie et supprime le dernier élément du tableau items, ou undefined si le tableau est vide
    pop() {
        return this.items.length ? this.items.pop() : undefined;
    }
    // Définition d'une méthode display qui affiche le contenu du tableau items dans la console
    display() {
        console.log(this.items);
    }
}
// Création d'une instance de la classe Stack avec le type string
let stack1 = new Stack();
// Appel de la méthode push avec deux arguments de type string
stack1.push("test");
stack1.push("test2");
// Appel de la méthode display pour afficher le contenu de la pile
stack1.display(); // ["test", "test2"]
// Appel de la méthode pop pour retirer le dernier élément de la pile
stack1.pop(); // "test2"
// Appel de la méthode display pour afficher le contenu de la pile
stack1.display(); // ["test"]
/* -------------------------------------------------------------------------- */
/* SECOND EXEMPLE

La classe Store<T> représente une structure de données de type magasin, qui permet d’ajouter et de supprimer des éléments d’un tableau. La classe utilise un paramètre de type générique T pour définir le type des éléments du magasin.
 La classe a une propriété coffre qui est un tableau de type T[] et qui stocke les éléments du magasin. La classe a aussi trois méthodes: addItems, removeItems et display. La méthode addItems prend un élément ou un tableau d’éléments de type T en paramètre et les ajoute au tableau items. La méthode renvoie le tableau items mis à jour.
La méthode removeItems prend un élément ou un tableau d’éléments de type T en paramètre et les supprime du tableau items. La méthode renvoie le tableau items mis à jour. La méthode display affiche le contenu du tableau items dans la console.

*/
// Définition d'une classe générique qui représente un magasin
class Store {
    // Déclaration d'un attribut coffre qui est un tableau de type T[]
    constructor(coffre = []) {
        this.coffre = coffre;
    }
    // Définition d'une méthode addItems qui prend un élément ou un tableau d'éléments de type T en paramètre et les ajoute au tableau items
    addItems(items) {
        // Utilisation d'une condition ternaire pour vérifier si items est un tableau ou non
        Array.isArray(items) ? this.coffre.push(...items) : this.coffre.push(items);
        // Renvoi du tableau items mis à jour
        return this.coffre;
    }
    // Définition d'une méthode removeItems qui prend un élément ou un tableau d'éléments de type T en paramètre et les supprime du tableau items
    removeItems(items) {
        // Utilisation d'une condition ternaire pour vérifier si items est un tableau ou non
        // Utilisation de la méthode filter pour créer un nouveau tableau sans les éléments à supprimer
        this.coffre = Array.isArray(items) ? this.coffre.filter((item) => !items.includes(item)) : this.coffre.filter((item) => item !== items);
        // Renvoi du tableau items mis à jour
        return this.coffre;
    }
    // Définition d'une méthode display qui affiche le contenu du tableau items dans la console
    display() {
        console.log(this.coffre);
    }
}
// Création d'une instance de la classe Store avec le type number
const storage = new Store();
// Appel de la méthode addItems avec un argument de type number
storage.addItems(1);
// Appel de la méthode addItems avec un argument de type number[]
storage.addItems([2, 3, 4]);
// Appel de la méthode display pour afficher le contenu du magasin
storage.display(); // [1, 2, 3, 4]
// Appel de la méthode removeItems avec un argument de type number
storage.removeItems(2);
// Appel de la méthode removeItems avec un argument de type number[]
storage.removeItems([1, 3]);
// Appel de la méthode display pour afficher le contenu du magasin
storage.display(); // [4]
