/*=============================================
=            Le type Partial<T>            =
=============================================*/
// Définition d'une fonction qui prend deux paramètres: un objet de type Todo et un objet de type Partial<Todo>
// Le type Partial<Todo> est un type utilitaire qui rend toutes les propriétés de Todo optionnelles
// Cela permet de passer un objet qui ne contient qu'une partie des propriétés de Todo
function majTodo(todo, update) {
    // Retourne un nouvel objet qui combine les propriétés de todo et de update
    // Si une propriété est présente dans update, elle écrase celle de todo
    return Object.assign(Object.assign({}, todo), update);
}
// Déclaration d'une variable todo1 de type Todo et initialisation avec un objet conforme à l'interface Todo
let todo1 = {
    id: 22,
    texte: "Apprendre TypeScript",
    auteur: "Paul",
};
// Appel à la fonction majTodo avec todo1 et un objet partiel qui ne contient que la propriété id
// La fonction retourne un nouvel objet qui a la même structure que todo1 mais avec l'id modifié
todo1 = majTodo(todo1, { id: 12 });
/*=============================================
=            Le type Readonly<T>            =
=============================================*/
// Déclaration d'une constante todoObj de type Readonly<Todo>
// Le type Readonly<Todo> est un type utilitaire qui rend toutes les propriétés de Todo en lecture seule
// Cela empêche de modifier les valeurs des propriétés après l'initialisation
const todoObj = {
    id: 2,
    texte: "Le seigneur des anneaux",
    auteur: "Tolkien",
};
// Déclaration d'une constante livre de type Record<Page, PageInfo>
// Le type Record<Page, PageInfo> est un type utilitaire qui crée un type d'objet dont les clés sont les valeurs du type Page et les valeurs sont du type PageInfo
const livre = {
    about: { title: "A propos" },
    contact: { title: "Nous contacter," },
    home: { title: "Accueil" }, // Idem pour la clé home
};
// Déclaration d'une constante todoWithPick de type TodoFront et initialisation avec un objet conforme au sous-type de Todo
const todoWithPick = {
    texte: "Apprendre la programmation",
    auteur: "Michel", // La propriété auteur fait partie du sous-type de Todo
};
// Déclaration d'une constante todoWhithOmit de type TodoFront2 et initialisation avec un objet conforme au sous-type de Todo
const todoWhithOmit = {
    texte: "Apprendre la programmation",
    auteur: "Michel", // La propriété auteur fait partie du sous-type de Todo
};
