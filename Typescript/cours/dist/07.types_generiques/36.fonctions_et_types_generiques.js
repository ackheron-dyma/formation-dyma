/*=============================================
=            Utilisation des génériques avec les fonctions            =
=============================================*/
//Le code commence par définir une fonction générique identique qui prend un paramètre param1 de type générique T et retourne une valeur de ce même type T. L'utilisation des génériques permet d'assurer que la valeur retournée par cette fonction est du même type que la valeur passée en paramètre.
function identique(param1) {
    return param1;
}
// La ligne suivante appelle cette fonction en lui passant un nombre, ce qui nous donne une indication sur l'utilisation des génériques : dans ce cas, le type T est implicitement inféré comme étant number.
let number = identique(2);
/* -------------------------------------------------------------------------- */
// Un autre exemple
//Ensuite, une autre fonction appelée fusionner est définie, qui prend deux objets et retourne leur fusion à l'aide de la méthode Object.assign. Cependant, cette fonction a un problème : elle utilise le type object pour ses paramètres, qui est un type très basique en TypeScript et n'a pas de propriétés spécifiques.
function fusionner(a, b) {
    return Object.assign(a, b);
}
// Cette limitation est illustrée dans l'exemple suivant, où une erreur est signalée parce que le type object n'a pas de propriété name.
const obj01 = fusionner({ name: "Jean" }, { name: "Paul" });
// if (obj01.name) { // ERREUR - Property 'name' does not exist on type 'object'.
// Pour résoudre ce problème, une autre version de la fonction fusionner est fournie, appelée fusionnerBonneMethode. Cette version utilise des génériques pour ses paramètres, ce qui permet de conserver plus d'informations sur les types des objets passés en paramètre.
function fusionnerBonneMethode(a, b) {
    return Object.assign(a, b);
}
// Avec cette nouvelle version, il n'y a plus d'erreur lorsque l'on accède à la propriété name de l'objet résultant.
let obj2 = fusionnerBonneMethode({ name: "Jean" }, { name: "Paul" });
if (obj2.name) {
}
