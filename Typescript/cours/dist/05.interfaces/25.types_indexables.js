/*=============================================
=            Les types indexables            =
=============================================*/
//  une variable jean de type User, qui a une propriété prenon avec la valeur "Jean"
export const jean = {
    prenom: "Jean",
};
// On ajoute une propriété nom à l'objet jean, avec la valeur "Dupont"
// Cela est possible grâce au type indexable [propName: string]: any, qui permet d'avoir des propriétés supplémentaires de n'importe quel type
jean.nom = "Dupont";
let myArray = ["Pierre", "Paul", "Bob"];
