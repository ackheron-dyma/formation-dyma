/*=============================================
=            Les propriétés optionnelles avec ?            =
=============================================*/
function printUserNew(user) {
    console.log(user.prenom);
}
// Mon objet "bishop" respecte bien l'interface User car la propriété "nom" est optionnelle
const bishop = {
    prenom: "Bishop",
};
printUserNew(bishop);
const ripley = { prenom: "Ripley" };
/* Le code crée ensuite un objet nommé jean de type User2 qui a la propriété prenom avec la valeur "Jean" et la propriété allergies avec un tableau contenant "pollen" et "poussière". Le code essaie ensuite d’ajouter un élément au tableau des allergies en utilisant la méthode push. Cela provoque une erreur de compilation, car la méthode push n’existe pas sur un tableau en lecture seule. */
const jean = { prenom: "Jean", allergies: ["pollen", "poussière"] };
// Toute tentative de modification du tableau 'allergies' entraînera une erreur.
// jean.allergies.push("poils de chat"); // Erreur
// Property 'push' does not exist on type 'readonly string[]'.
