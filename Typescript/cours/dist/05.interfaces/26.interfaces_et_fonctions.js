/*=============================================
=            Typer les fonctions avec les interfaces            =
=============================================*/
// Déclaration d'une fonction rechercher de type FonctionInterface
let rechercher = (crit1, crit2) => {
    // le corp de la fonction renvoie toujours true
    return true;
};
// Déclaration d'une fonction toto de type FonctionInterface
let toto = (crit1, crit2) => {
    // le corp de la fonction renvoie toujours false
    return false;
};
// Appel de la fonction rechercher avec les arguments 27 et "Basil"
rechercher(27, "Basil");
// Appel de la fonction toto avec les arguments 8383 et "Franck"
toto(8383, "Franck");
// Création d'un objet user1 de type MyUserInterface
const user1 = {
    prenom: "Jean",
    // La définition de la méthode direBonjour
    direBonjour(nom) {
        // La méthode affiche un message dans la console en utilisant la propriété prenom et le paramètre nom
        console.log(`Bonjour je m'appelle ${this.prenom} ${nom}`);
    },
};
let dudu = user1.direBonjour("Dujardin");
console.log("🚀 ~ file: 26.interfaces_et_fonctions.ts:40 ~ dudu:", dudu); // Bonjour je m'appelle Jean Dujardin
