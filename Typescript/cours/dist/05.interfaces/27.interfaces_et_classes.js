/*=============================================
=            Les classes peuvent implémenter des interfaces            =
=============================================*/
// Création d'une classe NamedUser qui implémente l'interface User89
class NamedUser {
    // Le constructeur prend deux paramètres publics prenom et nom, qui sont automatiquement assignés aux propriétés de la classe
    // Notation raccourcis (propriétés paramètres)
    constructor(prenom, nom) {
        this.prenom = prenom;
        this.nom = nom;
        // pas besoin grace au propriété paramètres de :
        // this.prenom = prenom
        // this.nom = nom
    }
    // Définition de la méthode direbonjour, qui respecte la signature de l'interface User89
    direbonjour() {
        // La méthode affiche un message dans la console en utilisant les propriétés prenom et nom
        console.log(`Bonjour je m'apelle ${this.prenom} ${this.nom}`);
    }
}
const Arnold = {
    prenom: "Arnold",
    nom: "Swarz,",
    adresse: "56 rue d'Autriche,",
    mail: "terminator@predator.com,",
    age: 39,
    // Définition de la méthode direbonjour, qui respecte la signature de l'interface Utilisateur héritée par l'interface UtilisateurEnregistre
    direbonjour() {
        // La méthode affiche un message dans la console en utilisant les propriétés prenom et nom
        console.log(`Bonjour je suis ${this.prenom} ${this.nom}`);
    },
};
// Appel de la méthode direbonjour sur l'objet Arnold
let bonjour = Arnold.direbonjour();
