/*=============================================
=            La surcharge des fonctions            =
=============================================*/
// Voici l'implémentation réelle de 'maFonction'.
// La fonction utilise le type 'any' pour 'param' pour être compatible avec toutes les signatures ci-dessus.
function maFonction(param) {
    return param;
}
// Utilisation de 'maFonction' avec différentes sortes d'arguments :
// Appelle 'maFonction' avec une chaîne de caractères et utilise ensuite 'toLocaleLowerCase' (une méthode des chaînes de caractères).
maFonction("toto tutu").toLocaleLowerCase();
// Appelle 'maFonction' avec un nombre et utilise ensuite 'toFixed' (une méthode des nombres).
maFonction(46).toFixed();
// Appelle 'maFonction' avec une valeur booléenne et utilise ensuite 'valueOf' (une méthode des valeurs booléennes).
maFonction(false).valueOf();
// Implémentation de la fonction 'getUser' qui gère toutes les surcharges ci-dessus.
// 'param1' peut être soit un nombre (id) soit une chaîne de caractères (email ou prenom).
// 'param2' est optionnel et serait utilisé pour le 'nom' si 'param1' est le 'prenom'.
function getUser(param1, param2) {
    let user;
    if (typeof param1 === "number") {
        // Si 'param1' est un nombre, il est traité comme un 'id'.
        // Le code pour récupérer l'utilisateur en fonction de cet 'id' serait ici.
    }
    else if (typeof param2 != "undefined") {
        // Si 'param2' est défini, 'param1' est traité comme un 'prenom' et 'param2' comme un 'nom'.
        // Le code pour récupérer l'utilisateur en fonction du 'prenom' et du 'nom' serait ici.
    }
    else {
        // Si seul 'param1' est fourni et que c'est une chaîne de caractères, elle est traitée comme un 'email'.
        // Le code pour récupérer l'utilisateur en fonction de cet 'email' serait ici.
    }
    // La fonction renvoie l'objet 'user' (qui devrait être défini à partir des blocs de code ci-dessus).
    return user;
}
