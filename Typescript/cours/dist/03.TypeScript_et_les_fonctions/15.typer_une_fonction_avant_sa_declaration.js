/*=============================================
=            Typer avant la déclaration            =
=============================================*/
// TYPAGE DE LA FONCTION, il est possible de typer une fonction avant de la déclarer, par exemple:
//Attention, lors du typage d'une fonction de cette manière il est obligatoire de préciser le type de la valeur de retour après =>
let multiplication;
// DÉCLARATION DE LA FONCTION, TypeScrypt utilise l'inférence pour les arguments et la valeur de retour
multiplication = (premier, second) => {
    return premier * second;
};
// Dans la deuxième partie, nous assignons une instance à la variable `obj` en respectant le type que nous avons défini.
let obj = {
    data: [2, 3, 44],
    multiplier(n1, n2) {
        return n1 * n1;
    },
};
