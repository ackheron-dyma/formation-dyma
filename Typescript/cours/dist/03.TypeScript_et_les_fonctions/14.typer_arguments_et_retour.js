// Le typage dans cette fonction est assez simple. Chaque argument (x et y) est du type number, et la valeur de retour de la fonction est également déclarée comme étant de type number. Cela signifie que si vous essayez d'appeler cette fonction avec des valeurs qui ne sont pas des nombres, ou si vous essayez de renvoyer une valeur qui n'est pas un nombre, TypeScript vous signalera une erreur.
function multiplier(x, y) {
    let somme = x * y;
    return somme;
}
// Dans ce cas précis comme la fonctions est très simple l'inférence de TypeScript fonctionne si on ne type pas la valeur de retour de la fonction.
