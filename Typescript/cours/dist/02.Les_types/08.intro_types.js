// Les chaines de caractères : string
let prenom = "Jean";
prenom = "Paul";
prenom = `Tom`;
// Les nombres : number
let decimal = 42;
let flottant = 42.23;
let hexa = 0x2a;
let bin = 0b10011;
let octal = 0o52;
// Les booléens : boolean
let actif;
actif = true;
actif = false;
