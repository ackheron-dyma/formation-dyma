/*=============================================
=            Le type enum            =
=============================================*/
/* Les énumérations (ou enum) permettent de définir un ensemble de valeurs nommées, rendant le code plus lisible et moins sujet aux erreurs que l'utilisation de simples valeurs comme des chaînes de caractères ou des nombres. */
// Exemple d'utilisation d'un type number
// Par défaut, les énumérations commencent à numéroter leurs membres à partir de 0 et incrémente de 1 pour chaque membre suivant.
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
    Role[Role["READ_WRITE"] = 2] = "READ_WRITE";
})(Role || (Role = {}));
let user;
if (user === Role.ADMIN) {
    // user = 0
    // l'utilisateur a un accès administrateur sur la machine
}
if (user === Role.READ_ONLY) {
    // user = 1
    // l'utilisateur ne peut que lire les fichiers sur la machine
}
if (user === Role.READ_WRITE) {
    // user = 2
    // l'utilisateur peut lire et écrire sur la machine
}
// ON peut changer la numérotation de départ "0" en définissant manuellement le valeur d'un de ses membres
var chemin;
(function (chemin) {
    chemin[chemin["d\u00E9part"] = 10] = "d\u00E9part";
    chemin[chemin["suite"] = 11] = "suite";
    chemin[chemin["plusLoin"] = 12] = "plusLoin";
    chemin[chemin["arriv\u00E9"] = 13] = "arriv\u00E9";
})(chemin || (chemin = {}));
