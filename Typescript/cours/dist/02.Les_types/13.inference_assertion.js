/*=============================================
=            L’inférence de type            =
=============================================*/
//L'inférence est l'une des raisons principales pour utiliser TypeScript : sans aucune ligne de code supplémentaire vous bénéficiez d'un contrôle déjà très puissant de l'IDE et du compilateur sur tous les types natifs et toutes vos assignations.
// Par l'inférence de type, TypeScript déclare implicitement que la variable et de type "number"
let x = 41; // let x: number
/*=============================================
=            L'inférence contextuelle due type            =
=============================================*/
//L'inférence contextuelle est l'inférence de type effectuée par TypeScript en fonction de la localisation du code :Ici TypeScript détecte le type de la fonction onmousedown(), en effet tous les objets et fonctions natifs, notamment du DOM, sont typés par TypeScript !
window.onmousedown = (e) => {
    console.log(e.button); // fonctionne
    // console.log(e.random); erreur pas de propriété random sur sur MouseEvent
};
window.onscroll = (e) => {
    // console.log(e.button); erreur pas de propriété button sur un UIevent
};
/*=============================================
=            L'assertion            =
=============================================*/
/*      L'assertion de type est utilisée lorsque vous, en tant que développeur, avez une connaissance supérieure à celle de TypeScript sur le type d'une variable.
        Dans l'exemple, la variable myVar est de type any, mais lorsqu'on calcule la longueur, on sait qu'elle est une chaîne. L'assertion (myVar as string) dit à TypeScript de traiter myVar comme une chaîne dans ce contexte. */
let myVar = "Une chaîne";
let longueur = myVar.length;
