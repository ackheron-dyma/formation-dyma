/*=============================================
=            Le type void            =
=============================================*/
/*     Le type void est généralement utilisé pour indiquer qu'une fonction ne renvoie rien.
        Dans le cas de direbonjour, cette fonction affiche un message dans la console mais ne renvoie aucune valeur. */
const direbonjour = () => {
    // La fonction ne retourne rien , la valeur de retour est donc "void"
    console.log("bonjour");
};
/*=============================================
=            Les types null et undefined            =
=============================================*/
let n = null;
let u = undefined;
// par défaut "null" et "undefined" sont des sous types de tous le autres types
let nombre = null;
/*=============================================
=            Le type number            =
=============================================*/
/*  Le type never est utilisé pour représenter des valeurs qui ne se produisent jamais.
    Les fonctions error, echec et boucleInfinie sont toutes de type never.
      *  error lance une erreur et ne retourne donc jamais.
      *  echec appelle error, ce qui signifie qu'elle ne retourne également jamais.
      *  boucleInfinie a une boucle while qui tourne indéfiniment, donc elle ne retourne jamais non plus. */
const error = () => {
    throw new Error("Something bad happened.");
};
const echec = () => {
    return error();
};
const boucleInfinie = () => {
    while (true) { }
};
