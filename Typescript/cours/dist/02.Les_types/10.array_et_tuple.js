/*=============================================
=            Le type array            =
=============================================*/
// La première section illustre l'utilisation du type array. Les arrays en TypeScript peuvent être typés, ce qui signifie que vous pouvez spécifier le type des éléments qu'ils contiennent.
let liste = [1, 46, 27, 53.2];
let liste2 = ["bonjour", "coucou"];
let liste3 = [1, "test", 46.9];
// Attention le type suivant ne peut être assigné que à un tableau vide
let liste4 = [];
/*=============================================
=            Le type tuple            =
=============================================*/
// La deuxième section montre l'utilisation de tuples en TypeScript. Un tuple est comme un tableau, mais sa taille est fixe et chaque élément peut avoir un type différent.
let tuple = [3, "Avion"];
let tuple2 = [3, 5788];
let tuple3 = ["toto", "coucou", "alfa"];
let tuple4 = ["titi", 99, {}];
