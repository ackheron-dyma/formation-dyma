/*=============================================
=            Utilisation des types mappés            =
=============================================*/
/* Le code TypeScript fourni utilise les types mappés, qui sont une fonctionnalité introduite dans la version 2.1 du langage.

Les types mappés permettent de créer de nouveaux types à partir d’autres types existants en appliquant une transformation sur leurs propriétés.

Par exemple, le type TempData<T> est un type mappé qui prend un type générique T et lui ajoute une propriété isValid de type boolean, tout en rendant optionnelles toutes les propriétés de T (en utilisant le symbole ?).

Ainsi, le type TempData<InterfaceTodo> est un type qui contient les propriétés id, texte et auteur de type number, string et string respectivement, mais qui sont toutes optionnelles, ainsi qu’une propriété isValid de type boolean qui est obligatoire. La variable tempTodo est donc un objet qui respecte ce type, car elle a la propriété isValid à true et la propriété texte à "Texte en cours", mais pas les autres propriétés de InterfaceTodo. */
// Création d'une variable de type TempData<InterfaceTodo>
const tempTodo = {
    // On assigne une valeur à la propriété texte, qui est optionnelle
    texte: "Texte en cours",
    // On assigne une valeur à la propriété isValid, qui est obligatoire
    isValid: true,
};
// Création d'une variable de type TempData2<EncoreUnUser>
const tempTodo2 = {
    // On assigne une valeur à la propriété prop20, qui est obligatoire
    prop20: true,
    // On assigne une valeur à la propriété prop1, qui est obligatoire
    prop1: "Texte",
};
